﻿namespace SensorManufSY2
{
    partial class Banner
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Banner));
            this.textFirstFailCode = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.mac = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pfText = new System.Windows.Forms.Label();
            this.okButton = new System.Windows.Forms.Button();
            this.sn = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lStepNumber = new System.Windows.Forms.Label();
            this.lErrorMessage = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textFirstFailCode
            // 
            resources.ApplyResources(this.textFirstFailCode, "textFirstFailCode");
            this.textFirstFailCode.Name = "textFirstFailCode";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // mac
            // 
            resources.ApplyResources(this.mac, "mac");
            this.mac.Name = "mac";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // pfText
            // 
            resources.ApplyResources(this.pfText, "pfText");
            this.pfText.Name = "pfText";
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            resources.ApplyResources(this.okButton, "okButton");
            this.okButton.Name = "okButton";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // sn
            // 
            resources.ApplyResources(this.sn, "sn");
            this.sn.Name = "sn";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.checkBoxButton);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // lStepNumber
            // 
            resources.ApplyResources(this.lStepNumber, "lStepNumber");
            this.lStepNumber.Name = "lStepNumber";
            // 
            // lErrorMessage
            // 
            resources.ApplyResources(this.lErrorMessage, "lErrorMessage");
            this.lErrorMessage.Name = "lErrorMessage";
            // 
            // Banner
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ControlBox = false;
            this.Controls.Add(this.lErrorMessage);
            this.Controls.Add(this.lStepNumber);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textFirstFailCode);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.mac);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pfText);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.sn);
            this.Controls.Add(this.label1);
            this.Name = "Banner";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label textFirstFailCode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label mac;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label pfText;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Label sn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lStepNumber;
        private System.Windows.Forms.Label lErrorMessage;
    }
}