﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SensorManufSY2
{
    public partial class Banner : Form
    {
        StationConfigure stationConfig;
        FixtureFunctions fixture;

        public Banner(StationConfigure config, FixtureFunctions fix)
        {
            InitializeComponent();
            stationConfig = config;
            fixture = fix;
            timer1.Enabled = true;
        }

        public string SN
        {
            set { sn.Text = value.ToString(); }
        }

        public string MAC
        {
            set { mac.Text = value.ToString(); }
        }

        public string PFText
        {
            set { pfText.Text = value.ToString(); }
        }

        public string StepNumber
        {
            set { lStepNumber.Text = value.ToString(); }
        }

        public string FirstFailCode
        {
            set { textFirstFailCode.Text = value.ToString(); }
        }

        public string ErrorMessage
        {
            set { lErrorMessage.Text = value; }
        }

        private void checkBoxButton(object sender, EventArgs e)
        {
            if (fixture.DoesBoxButtonExist())
            { 
                if (fixture.IsBoxButtonPushed())
                {
                    DialogResult = System.Windows.Forms.DialogResult.OK;
                    timer1.Enabled = false;
                    return;
                }
            }

        }

    }
}
