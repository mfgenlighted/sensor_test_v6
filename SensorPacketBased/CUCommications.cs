﻿// CUCommications.cs
//
// Define a set of routines that will commicate with the CU.
//
// defined functions:
//      CUComOpen
//      CUComClose
//      CUSendCommand
//      CU4ManufTestFUN.CUCommications.get_cu_firmware_build_number(out string, out bool, out bool)     
//  8/1/13  LN  Initial code.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Threading;
using System.Diagnostics;

using TestStationErrorCodes;

namespace SensorManufSY2
{
    public  class CUCommications : IDisposable
    {
        /************************************/
        /*      constants                   */
        /************************************/
        private const int CMD_RCV_DELAY = 50;


        /************************************/
        /*          defines              */
        /************************************/
        public static bool TEST_PASSED = true;
        public static bool TEST_FAILED = false;


        public static int NUMBER_OF_BYTES_IN_CALIIBRATION_BUFFER = 48;  // 4 * NUMBER_OF_CAL_VALUES
        public static int NUMBER_OF_CAL_VALUES = 12;                     // the number of cal values in the buffer
        // Data is is position depended data(start 0xff80)
        // Index   # bytes type    field                       description
        // 0       8       char    last 8 char of sn           unique id
        // 8       1       chara                               always 0x5E
        // 9       2       char    parameter.ESIVersion        ESI spec. ASCII. ie 48 -> 0x34,0x38
        // 11       16      char    passed serialNumber         ASCII serial number, trailing 0x00
        // 27      2       char    parameter.ManufactureCode   ASCII manufacture code
        // 29      16      char    passed model                ASCII model number, trailing 0x00
        // 45      4       char    parameter.HardwareRevision  ASCII hardware revision, trailing 0x00
        // 49      1       u8      parameter.NumOfChannels     number dimmable channels
        // 50      1       u8      parameter.DimMethod         supported dimming comm

        //        static int CU_NUM_BYTES_COMMAND = 6;
        static int CU_NUM_BYTES_RESPONSE_BUFFER = 6;


        /************************************/
        /*         CU Commands variables         */
        /************************************/
        byte[] CU_responseBuffer = new byte[CU_NUM_BYTES_RESPONSE_BUFFER];

        byte[] rd_status =              { 0x00, 0x00, 0x00, 0x00, 0x00, 0 };    // 0x0000   read
        byte[] rd_SU_watchdog_control = { 0x00, 0x00, 0x01, 0xFE, 0xED, 0 };    // 0x0001   read
        byte[] wr_SU_watchdog_control = { 0x20, 0x00, 0x01, 0xBE, 0xEF, 0 };    // 0x0001   write
        byte[] wr_relay_control =       { 0x20, 0x00, 0x13, 0x00, 0x00, 0 };    // 0x0013   write, data 0=off 1=on
        byte[] wr_SU_reboot =           { 0x20, 0x00, 0x17, 0x01, 0x01, 0 };    // 0x0017   write
        byte[] rd_milliWattHoursLower = { 0x00, 0x00, 0x19, 0x00, 0x00, 0 };    // 0x0019   read
        byte[] rd_milliWattHoursUpper = { 0x00, 0x00, 0x1A, 0x00, 0x00, 0 };    // 0x001A   read
        byte[] rd_WattHoursLower =      { 0x00, 0x00, 0x1B, 0x00, 0x00, 0 };    // 0x001B   read
        byte[] rd_WattHoursUpper =      { 0x00, 0x00, 0x1C, 0x00, 0x00, 0 };    // 0x001C   read
        byte[] rd_RMS_Voltage =         { 0x00, 0x00, 0x1D, 0x00, 0x00, 0 };    // 0x001D   read
        byte[] rd_RMS_Current =         { 0x00, 0x00, 0x1E, 0x00, 0x00, 0 };    // 0x001E   read
        byte[] rd_PowerFactor =         { 0x00, 0x00, 0x1F, 0x00, 0x00, 0 };    // 0x001F   read
        byte[] rd_RMS_Power =           { 0x00, 0x00, 0x20, 0x00, 0x00, 0 };    // 0x0020   read
        byte[] rd_temperature =         { 0x00, 0x00, 0x21, 0x00, 0x00, 0 };    // 0x0021   read
        byte[] rd_line_frequency =      { 0x00, 0x00, 0x22, 0x00, 0x00, 0 };    // 0x0022   read
        byte[] rd_phaseangle =          { 0x00, 0x00, 0x23, 0x00, 0x00, 0 };    // 0x0023   read
        byte[] rd_fw_version_register = { 0x00, 0xFF, 0x02, 0x00, 0x00, 0 };    // 0xFF02   read
        byte[] wr_cu_reboot =           { 0x00, 0xFF, 0x04, 0x00, 0x00, 0 };    // 0xFF04   write
        byte[] wr_hard_rst =            { 0x20, 0xFF, 0x0C, 0x00, 0x00, 0 };    // 0XFF0C   write
        byte[] rd_mfg_data =            { 0x00, 0xFF, 0x00, 0x00, 0x00, 0 };    // 0xFF80 - 0xFFFF read
        byte[] wr_mfg_data =            { 0x20, 0xFF, 0x00, 0x00, 0x00, 0 };    // 0xFF80 - 0xFFFF write



        //----------------------------------------
        // serial port values
        //----------------------------------------
        private readonly SerialPort CUSerialPort;            // dut serial port
        //private string CUSerialBuffer = string.Empty;      // buffer of incoming serial data
        //private string ConfigCUComPort = null;             // com port to use. in the form of COMx
        private bool disposed = false;
        
        public bool IsCUSerialPortInitialized { get { return (CUSerialPort != null); } }
        public bool IsCUSerialPortOpen { get { return CUSerialPort.IsOpen; } }

        byte[] inv = new byte[6];
        byte[] outv = new byte[6];

        byte[] mfgdata = new byte[6];
        byte[] mfgctrl = new byte[6];

        byte[] CalibrationDataBuffer = new byte[NUMBER_OF_BYTES_IN_CALIIBRATION_BUFFER];


        public CUCommications()
        {
            CUSerialPort = new SerialPort();
        }

        //-------------------
        // exception PortNotFound 
        // Will cause a exception when the configured port is not found in the
        // list of avaialibe ports
        //[Serializable]
        //public class PortNotFound : ApplicationException
        //{
        //    public string[] ComputerPorts { get; set; }                 // list of ports availible ports
        //    public string RequestedPort { get; set; }

        //    public PortNotFound() { }
        //    public PortNotFound(string message, string[] comPorts, string rqsPort)
        //        : base(message)
        //    {
        //        ComputerPorts = comPorts;
        //        RequestedPort = rqsPort;
        //    }
        //    public PortNotFound(string message, Exception inner) : base(message, inner) { }
        //    protected PortNotFound(
        //      System.Runtime.Serialization.SerializationInfo info,
        //      System.Runtime.Serialization.StreamingContext context)
        //        : base(info, context) { }
        //}


        //---------------------------------------------
        //  private functions
        //--------------------------------------------
        private byte CrcCalc(byte[] Msg)
        {
            int len, k;
            int crc = 0;
            const byte poly = 0x07;
            for (len = 0; len < 5; len++)
            {
                crc = crc ^ Msg[len];
                for (k = 0; k < 8; k++)
                {
                    if ((crc & 0x80) != 0)
                    {
                        crc <<= 1;
                        crc ^= poly;
                    }
                    else
                    {
                        crc <<= 1;
                    }
                }
            }
            return ((byte)crc);
        }



        //--------------------------------------
        //  public functions
        //--------------------------------------

        /*********************************************************************************************************/
        /***********************************    CU  COMM   PORT   ************************************************/
        /*********************************************************************************************************/

        public string Open(string comport)
        {
            if (this.disposed)
            {
                throw new ObjectDisposedException(this.GetType().Name, "Cannot use a disposed object.");
            }

            string[] ports = SerialPort.GetPortNames();         // get list of serial ports

            // see if this port is in the list of ports on the computer
            bool found = false;
            foreach (string i in ports)
            {
                if (i.Equals(comport))         // if found a match
                {
                    found = true;
                    break;
                }
            }
            if (!found)
            {       // port not found
                string msg = string.Format("Configured port({0}) not valid on this system", comport);
                throw new Exception(msg);
            }

            // port valid on this system, so try to open
            try
            {
                if (!CUSerialPort.IsOpen)
                {
                    CUSerialPort.PortName = comport;
                    CUSerialPort.BaudRate = 19200;
                    CUSerialPort.Parity = Parity.None;
                    CUSerialPort.StopBits = StopBits.One;
                    CUSerialPort.DataBits = 8;
                    CUSerialPort.Handshake = Handshake.None;
                    CUSerialPort.ReadTimeout = 500;
                    CUSerialPort.WriteTimeout = 500;
                    disposed = false;
                    CUSerialPort.Open();           // open the serial port

                    CUSerialPort.ErrorReceived += new SerialErrorReceivedEventHandler(ErrorReceived);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error opening {0}", comport), ex);
            }

            // Test the serialPort connection to ensure
            return "Success";
        }

        private void ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            Debug.Print(DateTime.Now.ToString() + " CU port error: " + e.EventType.ToString());
        }

        /************************************/
        /*         Close COM Port           */
        /************************************/
        public bool Close()
        {
            if (this.disposed)
            {
                return true;
            }
            try
            {
                CUSerialPort.Close();
                Thread.Sleep(200);          // let the port close
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public void DiscardAllBuffers()
        {
            if (this.disposed)
            {
                throw new ObjectDisposedException(this.GetType().Name, "Cannot use a disposed object.");
            }
            CUSerialPort.DiscardOutBuffer();
            CUSerialPort.DiscardInBuffer();
            Thread.Sleep(1000);
        }

        public void Dispose()
        {
            Close();
            CUSerialPort.Dispose();
            this.disposed = true;
        }


        /**************************************/
        /* send command to CU com port 
        // Sends the command and waits for the response.
        //
        // throw the following expections:
        //      Sending Command error
        //      Not enough characters received in time
        //      Did not get a ACK
        /***************************************/
        /// <summary>
        /// Sends the command array passed and waits for the response. 
        /// Will throw exceptions for the following:
        ///     Sending Command error
        ///     Not enough characters received in time
        ///     Did not get a ACK
        /// </summary>
        /// <param name="CU_command_array">byte[6] - data to send. The CRC will be calcuated and put in the last byte</param>
        /// <param name="timeoutms">int - max time in ms for the received to complete</param>
        /// <param name="rcvDelayms">int - delay in ms to wait after the send to receive</param>
        /// <param name="CU_response_array">byte[6] - data received</param>
        public void send_CU_com_port(byte[] CU_command_array, int timeoutms, int rcvDelayms, ref byte[] CU_response_array)
        {
            bool rtnbad = false;
            int timesSent = 0;
            int x;
            byte rtncrc;

            if (this.disposed)
            {
                throw new ObjectDisposedException(this.GetType().Name, "Cannot use a disposed object.");
            }
            if (!CUSerialPort.IsOpen)
                throw new Exception("Serial port is not open");

            CU_command_array[5] = CrcCalc(CU_command_array);    // add the CRC to the command
            do
            {
                rtnbad = false;
                if (CUSerialPort.BytesToRead != 0)      // if there is stuff in the input buffer
                {                                   
                    CUSerialPort.DiscardInBuffer();     // clear out any old stuff
                    CUSerialPort.DiscardOutBuffer();
                    Thread.Sleep(1000);
                }
                try
                {
                    CUSerialPort.Write(CU_command_array, 0, 6);
                    while (CUSerialPort.BytesToWrite > 0)
                    {
                        Thread.Sleep(5);
                    }
                    Thread.Sleep(5);
                }
                catch (Exception ex)    // send error
                {
                    throw new Exception("Sending command error. " + ex.Message);
                }

                Thread.Sleep(rcvDelayms);
                x = CUReadResponse(ref CU_response_array, timeoutms);   // get the response

                // check the CRC of the response
                rtncrc = CrcCalc(CU_response_array);
                if (x < 6)             // if the return dose not have enough returned chars
                {
                    rtnbad = true;
                    Trace.WriteLine(string.Format("{13}:CU:Not enough characters received in time. number of recived characters={12}  " +
                                                    "snd={0:X2}:{1:X2}:{2:X2}:{3:X2}:{4:X2}:{5:X2}  " +
                                                    "rcv={6:X2}:{7:X2}:{8:X2}:{9:X2}:{10:X2}:{11:X2}",
                                                        CU_command_array[0], CU_command_array[1], CU_command_array[2],
                                                        CU_command_array[3], CU_command_array[4], CU_command_array[5],
                                                        CU_response_array[0], CU_response_array[1], CU_response_array[2],
                                                        CU_response_array[3], CU_response_array[4], CU_response_array[5],
                                                        x, DateTime.Now.ToString()));
                    ////CUSerialPort.DiscardInBuffer();
                    ////CUSerialPort.DiscardOutBuffer();
                    //CUSerialPort.Close();
                    //Thread.Sleep(1000);
                    //CUSerialPort.Open();
                    //Thread.Sleep(1000);
                    Trace.WriteLine(string.Format("{0}:Reopened DUT com port", DateTime.Now.ToString()));
                }
                else if (rtncrc != CU_response_array[5])
                {           // if it is bad, mark it
                    rtnbad = true;
                    Trace.WriteLine(string.Format("{12}:CU:CRCs do not match  " +
                                                    "snd={0:X2}:{1:X2}:{2:X2}:{3:X2}:{4:X2}:{5:X2}  " +
                                                    "rcv={6:X2}:{7:X2}:{8:X2}:{9:X2}:{10:X2}:{11:X2}",
                                                        CU_command_array[0], CU_command_array[1], CU_command_array[2],
                                                        CU_command_array[3], CU_command_array[4], CU_command_array[5],
                                                        CU_response_array[0], CU_response_array[1], CU_response_array[2],
                                                        CU_response_array[3], CU_response_array[4], CU_response_array[5],
                                                        DateTime.Now.ToString()));
                }

                else        // got the right number of returned characters
                {
                    if ((CU_response_array[0] & 0x87) != 0x80)      // only care about the reply and error bits
                    {           // if there is a error, mark as bad
                        rtnbad = true;
                        Trace.WriteLine(string.Format("{12}:CU:Did not get a ACK. snd={0:X2}:{1:X2}:{2:X2}:{3:X2}:{4:X2}:{5:X2}  " +
                                                        "rcv={6:X2}:{7:X2}:{8:X2}:{9:X2}:{10:X2}:{11:X2}",
                                                            CU_command_array[0], CU_command_array[1], CU_command_array[2],
                                                            CU_command_array[3], CU_command_array[4], CU_command_array[5],
                                                            CU_response_array[0], CU_response_array[1], CU_response_array[2],
                                                            CU_response_array[3], CU_response_array[4], CU_response_array[5],
                                                            DateTime.Now.ToString()));
                    }
                }
            } while (rtnbad && (timesSent++ <= 2)); // retry 2 times
            Trace.Flush();

            if (x != 6)
                throw new Exception(string.Format("Not enough characters received in time. number of recived characters={12}\n" +
                                                    "snd={0:X2}:{1:X2}:{2:X2}:{3:X2}:{4:X2}:{5:X2}\n" +
                                                    "rcv={6:X2}:{7:X2}:{8:X2}:{9:X2}:{10:X2}:{11:X2}",
                                                        CU_command_array[0], CU_command_array[1], CU_command_array[2],
                                                        CU_command_array[3], CU_command_array[4], CU_command_array[5],
                                                        CU_response_array[0], CU_response_array[1], CU_response_array[2],
                                                        CU_response_array[3], CU_response_array[4], CU_response_array[5], x));
            if (rtncrc != CU_response_array[5])
            {
                throw new Exception(string.Format("CRCs do not match  " +
                                                "snd={0:X2}:{1:X2}:{2:X2}:{3:X2}:{4:X2}:{5:X2}  " +
                                                "rcv={6:X2}:{7:X2}:{8:X2}:{9:X2}:{10:X2}:{11:X2}",
                                                    CU_command_array[0], CU_command_array[1], CU_command_array[2],
                                                    CU_command_array[3], CU_command_array[4], CU_command_array[5],
                                                    CU_response_array[0], CU_response_array[1], CU_response_array[2],
                                                    CU_response_array[3], CU_response_array[4], CU_response_array[5]));
            }


            if ((CU_response_array[0] & 0x87) != 0x80)      // only care about the reply and error bits
                throw new Exception(string.Format("Did not get a ACK. snd={0:X2}:{1:X2}:{2:X2}:{3:X2}:{4:X2}:{5:X2}\n" +
                                                    "rcv={6:X2}:{7:X2}:{8:X2}:{9:X2}:{10:X2}:{11:X2}",
                                                        CU_command_array[0], CU_command_array[1], CU_command_array[2],
                                                        CU_command_array[3], CU_command_array[4], CU_command_array[5],
                                                        CU_response_array[0], CU_response_array[1], CU_response_array[2],
                                                        CU_response_array[3], CU_response_array[4], CU_response_array[5]));
        }

        //************************************************************
        // Method: CUReadResponse
        //  Reads the serial input buffer for a command response.
        // 
        // params:  ref byte[] returnedLine - the 6 retured bytes.
        //          int timeoutms - max number of milliseconds to wait for the line
        // return:
        //      number of bytes found.
        // Exceptions:
        //      Exception
        //*******************************************************
        public int CUReadResponse(ref byte[] returnedLine, int timeoutms)
        {
            DateTime endTime;
            int status = 0;

            endTime = DateTime.Now.AddMilliseconds(timeoutms);
            while ((CUSerialPort.BytesToRead < 6) && (endTime > DateTime.Now))
            {
                //Thread.Sleep(10);
            }

            if (CUSerialPort.BytesToRead > 6)
                status = 6;
            else
                status = CUSerialPort.BytesToRead;
            returnedLine[0] = 0;
            returnedLine[1] = 0;
            returnedLine[2] = 0;
            returnedLine[3] = 0;
            returnedLine[4] = 0;
            returnedLine[5] = 0;
            for (int i = 0; i < status; i++)
            {
                returnedLine[i] = (byte)CUSerialPort.ReadByte();
            }
            return status;
        }


        //------------------------------------------------
        //  CU commands
        //---------------------------------------



        /************************************/
        /*   get cu_firmware_version_register    /
        /************************************/

        public int get_cu_firmware_version_register(ref string fwVersion, ref string reportMsg)
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };

            fwVersion = "";
            reportMsg = string.Empty;

            try
            {
                send_CU_com_port(rd_fw_version_register, 2000, CMD_RCV_DELAY, ref cmdresponse);
                fwVersion = Convert.ToString(((cmdresponse[3] * 256) + cmdresponse[4]), 16);
                return 0;
            }
            catch (Exception ex)    // there was an issue with the send or receive
            {
                reportMsg = "Error doing 'rd_fw_version_register' function. Error message: " + ex.Message;
                return (int)ErrorCodes.DUT.FailedToPerformCUFunction;
            }
        }


        /************************************/
        /*   get cu_AC_RMS_Voltage           /
        /************************************/

        public int get_cu_AC_RMS_Voltage(ref double voltage, ref string reportMsg)
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };
            voltage = 0;
            reportMsg = string.Empty;

            try
            {
                send_CU_com_port(rd_RMS_Voltage, 1000, CMD_RCV_DELAY, ref cmdresponse);
                voltage = ((cmdresponse[3] * 256) + cmdresponse[4]) / 10.0;
                return 0;
	        }
	        catch (Exception ex)    // there was an issue with the send or receive
	        {
		        reportMsg = "Error doing 'get_cu_AC_RMS_Voltage' functions. Error message: " + ex.Message;
                return (int)ErrorCodes.DUT.FailedToPerformCUFunction;
	        }
        }

        /************************************/
        /*   get cu_AC_RMS_Current  ma       /
        /************************************/

        public int get_cu_AC_RMS_Current(ref double voltage, ref string reportMsg)
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };

            voltage = 0;
            reportMsg = string.Empty;

            try
            {
                send_CU_com_port(rd_RMS_Current, 1000, CMD_RCV_DELAY, ref cmdresponse);
                voltage = (cmdresponse[3] * 256) + cmdresponse[4];
                return 0;
	        }
	        catch (Exception ex)    // there was an issue with the send or receive
	        {
                reportMsg = "Error doing 'get_cu_AC_RMS_Current' functions. Error message: " + ex.Message;
                return (int)ErrorCodes.DUT.FailedToPerformCUFunction;
            }
        }


        /************************************/
        /*  get cu_AC_RMS_Watts              /
        /************************************/
        public int get_cu_AC_RMS_Watts(ref double voltage, ref string reportMsg)
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };

            voltage = 0;
            reportMsg = string.Empty;

            try
            {
                send_CU_com_port(rd_RMS_Power, 1000, CMD_RCV_DELAY, ref cmdresponse);
                voltage = ((cmdresponse[3] * 256) + cmdresponse[4]) / 10.0;
                return 0;
	        }
	        catch (Exception ex)    // there was an issue with the send or receive
	        {
                reportMsg = "Error doing 'get_cu_AC_RMS_Power' functions. Error message: " + ex.Message;
                return (int)ErrorCodes.DUT.FailedToPerformCUFunction;
            }
        }

        /************************************/
        /*  get cu_AC_PowerFactor            /
        /************************************/
        public int get_cu_AC_PowerFactor(ref double voltage, ref string reportMsg)
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };

            voltage = 0;
            reportMsg = string.Empty;

            try
            {
                send_CU_com_port(rd_PowerFactor, 1000, CMD_RCV_DELAY, ref cmdresponse);
                voltage = ((cmdresponse[3] * 256) + cmdresponse[4]) / 1000.0;
                return 0;
            }
            catch (Exception ex)    // there was an issue with the send or receive
            {
                reportMsg = "Error doing 'get_PowerFactor' functions. Error message: " + ex.Message;
                return (int)ErrorCodes.DUT.FailedToPerformCUFunction;
            }
        }

        /************************************/
        /*  get cu_Temperature               /
        /************************************/
        public int get_cu_Temperature(ref double voltage, ref string reportMsg)
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };

            voltage = 0;
            reportMsg = string.Empty;

            try
            {
                send_CU_com_port(rd_temperature, 1000, CMD_RCV_DELAY, ref cmdresponse);
                voltage = ((cmdresponse[3] * 256) + cmdresponse[4]) / 10.0;
                return 0;
            }
            catch (Exception ex)    // there was an issue with the send or receive
            {
                reportMsg = "Error doing 'get_cu_Temperature' functions. Error message: " + ex.Message;
                return (int)ErrorCodes.DUT.FailedToPerformCUFunction;
            }
        }




        /************************************/
        /*   Perform CU_S_RELAY Control      /
        /************************************/

        public int perform_cu_s_relay_control(int state, ref string reportMsg)
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };
            byte[] cmd = wr_relay_control;

            try
            {
                cmd[4] = (byte)state;
                send_CU_com_port(cmd, 1000, CMD_RCV_DELAY, ref cmdresponse);
                return 0;
	        }
	        catch (Exception ex)    // there was an issue with the send or receive
	        {
                reportMsg = "Error doing 'perform_cu_s_relay_contorl' functions. Error message: " + ex.Message;
                return (int)ErrorCodes.DUT.FailedToPerformCUFunction;
            }
        }


        /************************************/
        /*   Perform wr_SU_reboot            /
        /************************************/

        public int perform_wr_SU_reboot(ref string reportMsg)
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };

            try
            {
                send_CU_com_port(wr_SU_reboot, 1000, CMD_RCV_DELAY, ref cmdresponse);
                return 0;
            }
            catch (Exception ex)    // there was an issue with the send or receive
            {
                reportMsg = "Error doing 'perform_wr_SU_reboot' functions. Error message: " + ex.Message;
                return (int)ErrorCodes.DUT.FailedToPerformCUFunction;
            }
        }



        /*****************************************/
        /*   read_manufacturing_data_string     /
        /*****************************************/
        /// <summary>
        /// Will read the manufacting data from ram to the manufacturingDataBuffer. 
        /// </summary>
        /// <returns>byte[NUMBER_OF_BYTES_IN_MANUFACTURING_BUFFER] - bytes read from cu ram.</returns>
        public byte[] read_manufacturing_data_string_from_ram(int numOfBytes)
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };
            byte[] ManufacturingDataBuffer = new byte[numOfBytes];


            int loop2;
            rd_mfg_data[2] = 0x80;  // starting address

            for (loop2 = 0; (loop2 < numOfBytes); loop2++)
            {
                try
                {
                    send_CU_com_port(rd_mfg_data, 1000, CMD_RCV_DELAY, ref cmdresponse);
                }
                catch (Exception ex)    // there was an issue with the send or receive
                {
                    throw new Exception(string.Format("Error doing 'read_manufacturing_data_string_from_ram' function, loop {0}. Error message: {1}",
                        loop2, ex.Message));
                }
                ManufacturingDataBuffer[loop2] = cmdresponse[4];
                rd_mfg_data[2] += 1;
            }

            return ManufacturingDataBuffer;
        }



        /// <summary>
        /// Write the ManufacturingDataBuffer to ram. will only write the full bufffer.
        /// </summary>
        /// <param name="ManufacturingDataBuffer"></param>
        /// <param name="reportStatus"></param>
        /// <returns></returns>
        public int  write_manufacturing_data_string_to_ram(byte[] ManufacturingDataBuffer, out string reportStatus)
        {
            int loop2;
            int retrycnt = 0;
            bool sendByteStatus = true;
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };
            int status = 0;
            reportStatus = string.Empty;

            // first need to enable the manufacture area
            if ((status = ManufactureDataEnableControl(true, ref reportStatus)) != 0)
            {
                return (int)ErrorCodes.DUT.FailedToPerformCUFunction;
            }

            wr_mfg_data[2] = 0x80;   // set the starting address in the ram flash buffer
            reportStatus = "";

            for (loop2 = 0; loop2 < ManufacturingDataBuffer.Length; loop2++)
            {
                wr_mfg_data[4] = ManufacturingDataBuffer[loop2];  // get the data to write
                sendByteStatus = false;
                retrycnt = 0;
                while (!sendByteStatus)
                {
                    try
                    {
                        send_CU_com_port(wr_mfg_data, 1000, CMD_RCV_DELAY, ref cmdresponse);        // write the data to ram flash buffer
                        sendByteStatus = true;                                                      // no exception throw, good send/rcv
                    }
                    catch (Exception ex)    // there was an issue with the send or receive
                    {
                        if (retrycnt++ == 1)    // only try to resend once
                        {
                            reportStatus = string.Format("Error doing 'write_manufacturing_data_string_to_ram' function, loop {0}. Error message: {1}", loop2, ex.Message);
                            return (int)ErrorCodes.DUT.FailedToPerformCUFunction;
                        }
                    }
                }
                wr_mfg_data[2] += 1;// increment the address
            }

            return 0;
        }

        /// <summary>
        /// Enable or disable the manufacturing data area ability to change.
        /// </summary>
        /// <param name="enableToWrite"></param>
        /// <returns></returns>
        public int ManufactureDataEnableControl(bool enableToWrite, ref string reportMsg)
        {
            byte[] cmdresponse = { 0, 0, 0, 0, 0, 0 };
            byte[] sndData1 = { 0x20, 0xF0, 0x01, 0xA5, 0x5A, 0 };  // magic word
            byte[] sndData2 = { 0x20, 0xF0, 0x02, 0x00, 0xA5, 0 };
            byte[] sndData3 = { 0x20, 0xF0, 0x00, 0x00, 0x69, 0 };    // 0x0000   write, execute the command

            try
            {
                send_CU_com_port(sndData1, 1000, CMD_RCV_DELAY, ref cmdresponse);
                if (enableToWrite)
                    send_CU_com_port(sndData2, 1000, CMD_RCV_DELAY, ref cmdresponse);
                else        // to disable, send the rest of the magic word
                    send_CU_com_port(sndData1, 1000, CMD_RCV_DELAY, ref cmdresponse);
                send_CU_com_port(sndData3, 1000, CMD_RCV_DELAY, ref cmdresponse);
            }
            catch (Exception ex)
            {
                reportMsg = "Error unlocking manufacture data area. Error message: " + ex.Message;
                return (int)ErrorCodes.DUT.FailedToPerformCUFunction;
            }
            return 0;
        }



    }
}