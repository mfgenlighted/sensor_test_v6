﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SensorManufSY2
{
    public partial class ConfigFixturesEditStation : Form
    {
        public string stationName;

        public ConfigFixturesEditStation(string sn)
        {
            InitializeComponent();
            tbStation.Text = sn;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            stationName = tbStation.Text;
            this.DialogResult = DialogResult.OK;
        }
    }
}
