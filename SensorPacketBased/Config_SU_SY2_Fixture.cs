﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.IO.Ports;
using System.Drawing.Printing;
using System.Management;
using System.Diagnostics;
using System.Threading;

using TestStationErrorCodes;

namespace SensorManufSY2
{
    public partial class Config_SU_SY2_Fixture : Form
    {
        string fixtureInUse;
        StationConfigure sconfig;
        const string  REF_RADIO_MODEL = "CK-1U-01";

        public Config_SU_SY2_Fixture(StationConfigure config, string fixture)
        {
            InitializeComponent();
            fixtureInUse = fixture;
            sconfig = config;
        }

        private void SetupForm(object sender, EventArgs e)
        {
            string outmsg = string.Empty;

            outmsg = LoadFormData(outmsg);
        }

        private string LoadFormData(string outmsg)
        {

            if (sconfig.ReadStationConfigFile(this.fixtureInUse, ref outmsg) != 0)
            {
                MessageBox.Show(outmsg);
                return outmsg;
            }

            // these are not changable by the user
            lbFixtureType.Text = sconfig.currentFixDef.fixtureType;    // hardware fixture type
            lbStationDB.Text = sconfig.ConfigStationToUse;     // database station to use
            lbStationSection.Text = sconfig.ConfigFixtureName; // Fixture name

            // used by all stations
            textBoxTSStationTitle.Text = sconfig.StationTitle;
            textBoxTSStationOperation.Text = sconfig.ConfigStationOperation;

            // dut console port
            if (sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.DUTConsole) != null)
            {
                comboBoxTSDUTComPort.Text = sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.DUTConsole).connection;
                comboBoxTSDUTComPort.Visible = true;
                if (sconfig.currentFixDef.fixtureType == FixtureDefintions.FT_GW_2_1_FCT)   // if gateway fixture, console autofind not avaialible
                    vAutoFindDUTComPort.Visible = false;
            }
            else
            {
                comboBoxTSDUTComPort.Visible = false;
            }

            // test radio
            if (sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.TestRadio) != null)
            {
                numericUpDownTSTestChannel.Value = Convert.ToInt32(sconfig.ConfigTestRadioChannel);
                comboBoxTSTestRadioComPort.Text = sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.TestRadio).connection;
                numericUpDownTSTestChannel.Visible = true;
                comboBoxTSTestRadioComPort.Visible = true;
                bAutoFindTestRadioComPort.Visible = true;
            }
            else
            {
                numericUpDownTSTestChannel.Visible = false;
                comboBoxTSTestRadioComPort.Visible = false;
                bAutoFindTestRadioComPort.Visible = false;
            }

            // Flasher
            if (sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.Flasher) != null)
            {
                cbFlasherComPort.Text = sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.Flasher).connection;
                cbFlasherComPort.Visible = true;
            }
            else
            {
                cbFlasherComPort.Visible = false;
                bAutoFindFlasher.Visible = false;
            }


            // Switch-Mate
            cbSwitchMateCom.Visible = false;
            bAutoFindSMComPort.Visible = false;
            if (sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.SwitchMate) != null)
            {
                cbSwitchMateCom.Text = sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.SwitchMate).connection;
                cbSwitchMateCom.Visible = true;
                bAutoFindSMComPort.Visible = true;
            }

            // printers
            if (sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.PCBAPrinter) != null)
            {
                comboBoxTSPCBAPrinter.Text = sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.PCBAPrinter).connection;
                comboBoxTSPCBAPrinter.Visible = true;
            }
            else
            {
                comboBoxTSPCBAPrinter.Visible = false;
            }

            if (sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.HLAPrinter) != null)
            {
                comboBoxTSProductPrinter.Text = sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.HLAPrinter).connection;
                comboBoxTSProductPrinter.Visible = true;
            }
            else
            {
                comboBoxTSProductPrinter.Visible = false;
            }

            // LED detector
            comboBoxTSLEDDetectorComPort.Visible = false;
            bAutofindLEDComPort.Visible = false;
            textBoxTSRedRange.Visible = false;
            textBoxTSGreenRange.Visible = false;
            textBoxTSBlueRange.Visible = false;
            if (sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.LEDDetector) != null)
            {
                comboBoxTSLEDDetectorComPort.Text = sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.LEDDetector).connection;
                comboBoxTSLEDDetectorComPort.Visible = true;
                bAutofindLEDComPort.Visible = true;
                textBoxTSRedRange.Text = sconfig.ConfigLedDetector.ColorRED;
                textBoxTSGreenRange.Text = sconfig.ConfigLedDetector.ColorGREEN;
                textBoxTSBlueRange.Text = sconfig.ConfigLedDetector.ColorBLUE;
                textBoxTSRedRange.Visible = true;
                textBoxTSGreenRange.Visible = true;
                textBoxTSBlueRange.Visible = true;
            }

            lbDAQVisaName.Visible = false;
            tbDAQVisaName.Visible = false;
            if (sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.A34970A) != null)
            {
                tbDAQVisaName.Text = sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.A34970A).connection;
                lbDAQVisaName.Visible = true;
                tbDAQVisaName.Visible = true;
            }

            // used by Sensor type stations
            comboBoxTSTempProbe.Visible = false;
            if (sconfig.GetChannelInstrument(FixtureDefintions.CH_TEMPERTURE_PROBE) != null)
            {
                comboBoxTSTempProbe.Text = sconfig.ConfigTempSensorOutput;
                comboBoxTSTempProbe.Visible = true;
            }


            // Sensors with ambient sensor
            numericUpDownTSAmbDLSLEDV.Visible = false;
            numericUpDownTSAmbDLSOnDelay.Visible = false;
            numericUpDownTSAmbDLSOffDelay.Visible = false;
            if (sconfig.GetChannelInstrument(FixtureDefintions.CH_AMBIENT_DETECTOR_LED) != null)
            {
                numericUpDownTSAmbDLSLEDV.Text = sconfig.ConfigAmbientDetectorLightSource.Voltage.ToString("N3");
                numericUpDownTSAmbDLSLEDV.Visible = true;
                numericUpDownTSAmbDLSOnDelay.Value = sconfig.ConfigAmbientDetectorLightSource.OnDelayMs;
                numericUpDownTSAmbDLSOffDelay.Value = sconfig.ConfigAmbientDetectorLightSource.OffDelayMs;
                numericUpDownTSAmbDLSOnDelay.Visible = true;
                numericUpDownTSAmbDLSOffDelay.Visible = true;
            }

            // sensors with PIR sensor
            numericUpDownTSPIRDLSOnDelay.Visible = false;
            numericUpDownTSPIRDLSOffDelay.Visible = false;
            if (sconfig.GetChannelInstrument(FixtureDefintions.CH_PIR_SOURCE_CNTL) != null)
            {
                numericUpDownTSPIRDLSOnDelay.Value = sconfig.ConfigPIRDetectorLightSource.OnDelayMs;
                numericUpDownTSPIRDLSOffDelay.Value = sconfig.ConfigPIRDetectorLightSource.OffDelayMs;
                numericUpDownTSPIRDLSOnDelay.Visible = true;
                numericUpDownTSPIRDLSOffDelay.Visible = true;
            }

            return outmsg;
        }

        private void FixtureToConfig_SelectedIndexChanged(object sender, EventArgs e)
        {
            string outmsg = string.Empty;

            LoadFormData(outmsg);
        }

        // save button
        private void button1_Click(object sender, EventArgs e)
        {

            sconfig.StationTitle = textBoxTSStationTitle.Text;
            sconfig.ConfigStationOperation = textBoxTSStationOperation.Text;
            sconfig.ConfigTempSensorOutput = comboBoxTSTempProbe.Text;

            if (sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.DUTConsole) != null)
                sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.DUTConsole).connection = comboBoxTSDUTComPort.Text;

            if (sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.TestRadio) != null)
            {
                sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.TestRadio).connection = comboBoxTSTestRadioComPort.Text;
                sconfig.ConfigTestRadioChannel = numericUpDownTSTestChannel.Value.ToString();
            }

            if (sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.SwitchMate) != null)
                sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.SwitchMate).connection = cbSwitchMateCom.Text;
            if (sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.Flasher) != null)
                sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.Flasher).connection = cbFlasherComPort.Text;
            if (sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.A34970A) != null)
                sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.A34970A).connection = tbDAQVisaName.Text;


            if (sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.PCBAPrinter) != null)
                sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.PCBAPrinter).connection = comboBoxTSPCBAPrinter.Text;
            if (sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.HLAPrinter) != null)
                sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.HLAPrinter).connection = comboBoxTSProductPrinter.Text;

            if (sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.LEDDetector) != null)
            {
                sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.LEDDetector).connection = comboBoxTSLEDDetectorComPort.Text;
                sconfig.ConfigLedDetector.ColorRED = textBoxTSRedRange.Text;
                sconfig.ConfigLedDetector.ColorGREEN = textBoxTSGreenRange.Text;
                sconfig.ConfigLedDetector.ColorBLUE = textBoxTSBlueRange.Text;
            }

            sconfig.ConfigAmbientDetectorLightSource.Voltage = Convert.ToDouble(numericUpDownTSAmbDLSLEDV.Value);
            sconfig.ConfigAmbientDetectorLightSource.OnDelayMs = Convert.ToInt32(numericUpDownTSAmbDLSOnDelay.Value);
            sconfig.ConfigAmbientDetectorLightSource.OffDelayMs = Convert.ToInt32(numericUpDownTSAmbDLSOffDelay.Value);

            sconfig.ConfigPIRDetectorLightSource.OnDelayMs = Convert.ToInt32(numericUpDownTSPIRDLSOnDelay.Value);
            sconfig.ConfigPIRDetectorLightSource.OffDelayMs = Convert.ToInt32(numericUpDownTSPIRDLSOffDelay.Value);

            sconfig.UpdateFixtureData(fixtureInUse);
        }


        private void DutComPort_DropDown(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();         // get list of serial ports
            ComboBox mybutton = sender as ComboBox;

            mybutton.Items.Clear();
            //foreach (var item in ports)
            //{
            //    mybutton.Items.Add(item.ToString());
            //}

            try
            {
                ManagementObjectSearcher searcher =
                    new ManagementObjectSearcher("root\\CIMV2",
                    "SELECT * FROM Win32_PnPEntity");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    if (queryObj["Caption"] != null)
                    { 
                        if (queryObj["Caption"].ToString().Contains("(COM"))
                        {
                            mybutton.Items.Add(queryObj["Caption"]);
                        }
                    }

                }
            }
            catch (ManagementException exx)
            {
                MessageBox.Show(exx.Message);
            }
        }

        private void buttonFindBarcodeFile_Click(object sender, EventArgs e)
        {

            PrintDocument prtdoc = new PrintDocument();
            ComboBox myprinter = sender as ComboBox;

            //prt.PrinterSettings.PrinterName returns the name of the Default Printer
            string strDefaultPrinter = prtdoc.PrinterSettings.PrinterName;

            //this will loop through all the Installed printers and add the Printer Names to a ComboBox.
            foreach (String strPrinter in PrinterSettings.InstalledPrinters)
            {
                myprinter.Items.Add(strPrinter);

                //This will set the ComboBox Index where the Default Printer Name matches with the current Printer Name returned by for loop
                if (strPrinter.CompareTo(strDefaultPrinter) == 0)
                {
                    myprinter.SelectedIndex = myprinter.Items.IndexOf(strPrinter);
                }
            }

        }

        private void comboBoxTSDUTComPort_SelectionChangeCommitted(object sender, EventArgs e)
        {
            string temp = string.Empty;
            string pn = string.Empty;
            string sn = string.Empty;
            string model = string.Empty;
            int start = 0;
            ComboBox mybutton = sender as ComboBox;
            List<FixtureDefintions.Instrument> initList = new List<FixtureDefintions.Instrument>();

            start = mybutton.SelectedItem.ToString().IndexOf("(COM") + 1;
            temp = mybutton.SelectedItem.ToString().Substring(start, mybutton.SelectedItem.ToString().Length - start - 1);
            mybutton.Items.Clear();
            mybutton.Items.Add(temp);
            mybutton.SelectedIndex = 0;

            if (sconfig.currentFixDef.fixtureType == FixtureDefintions.FT_GW_2_1_FCT)
            {
                return;
            }

            if (sconfig.GetChannelData(FixtureDefintions.CH_DUT_POWER_CNTL) == null)
            {
                MessageBox.Show("Make sure DUT is powered up with DVTMAN and unconfigured.");
            }
            else
            {
                if (sconfig.GetInstrumentData(sconfig.GetChannelInstrument(FixtureDefintions.CH_DUT_POWER_CNTL).instrumentID).connection == string.Empty)      // if the power control has not been config
                {
                    MessageBox.Show("DUT Power Control must be configured first");
                    comboBoxTSDUTComPort.BackColor = Color.White;
                    mybutton.Text = "Auto Find";
                    return;
                }
                // get the instrument that has the DUT power control. Add to list so that it is the only instrument in the list
                initList.Add(sconfig.GetInstrumentData(sconfig.GetChannelInstrument(FixtureDefintions.CH_DUT_POWER_CNTL).instrumentID));
            }


            using (FixtureFunctions fixture = new FixtureFunctions(sconfig))
            {
                initList.Add(sconfig.GetInstrumentData(sconfig.GetChannelInstrument(FixtureDefintions.CH_DUT_CONSOLE).instrumentID));
                initList[0].connection = temp;
                if (!fixture.InitFixture(initList))
                {
                    MessageBox.Show("Error initializing the fixture. " + fixture.LastFunctionErrorMessage);
                }
                else
                {
                    fixture.ControlDUTPower(FixtureDefintions.PowerControl.ON, 1000);

                    fixture.PacketDutConsole.ClearBuffers();
                    if (fixture.PacketDutConsole.GetMACData(ref pn, ref sn))
                    {
                        if (pn.Contains("FFFFFFFF"))       // make sure not the ref radio
                        {
                            MessageBox.Show("Looks like this is the correct port.");
                            mybutton.BackColor = Color.Green;
                        }
                        else
                        {
                            MessageBox.Show("Looks like this is not the correct port.");
                            mybutton.BackColor = Color.Red;
                        }
                    }
                    else
                    { 
                        MessageBox.Show("Looks like this is not the correct port. No answer to query");
                        mybutton.BackColor = Color.Red;
                    }

                    fixture.SetOutput("DUT_POWER_CNTL", (double)FixtureDefintions.PowerControl.OFF);
                }
            }
        }

        private void comboBoxTSSwitchMateComPort_SelectionChangeCommitted(object sender, EventArgs e)
        {
            string temp = string.Empty;
            int start = 0;
            SerialPort smport = new SerialPort();

            ComboBox mybutton = sender as ComboBox;
            start = mybutton.SelectedItem.ToString().IndexOf("(COM") + 1;
            temp = mybutton.SelectedItem.ToString().Substring(start, mybutton.SelectedItem.ToString().Length - start - 1);
            mybutton.Items.Clear();
            mybutton.Items.Add(temp);
            mybutton.SelectedIndex = 0;

            smport.BaudRate = 19200;
            smport.PortName = temp;
            smport.NewLine = "\r";
            smport.Open();
            smport.WriteLine("SM_ID?");
            smport.ReadTimeout = 1000;
            try
            {
                if (!(temp = smport.ReadLine()).Contains("SWITCH-MATE"))
                {
                    MessageBox.Show("Not the Switch-Mate port. Found: " + temp + "\n");
                    mybutton.BackColor = Color.Red;
                }
                else
                { 
                    MessageBox.Show("Correct port");
                    mybutton.BackColor = Color.Green;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Not the Switch-Mate port. Error: " + ex.Message);
                mybutton.BackColor = Color.Red;
            }
            smport.Close();
        }

        private void cbRefRadioComPort_SelectionChangeCommitted(object sender, EventArgs e)
        {
            string temp = string.Empty;
            int start = 0;
            SerialPort smport = new SerialPort();

            ComboBox mybutton = sender as ComboBox;
            start = mybutton.SelectedItem.ToString().IndexOf("(COM") + 1;
            temp = mybutton.SelectedItem.ToString().Substring(start, mybutton.SelectedItem.ToString().Length - start - 1);
            mybutton.Items.Clear();
            mybutton.Items.Add(temp);
            mybutton.SelectedIndex = 0;

            smport.BaudRate = 115200;
            smport.PortName = temp;
            smport.ReadTimeout = 1000;
            smport.Open();
            try
            {
                smport.WriteLine("d v");
                smport.ReadLine();          // get the echo
            }
            catch (Exception ex)
            {
                MessageBox.Show("Not the Ref Radio port. Error: " + ex.Message);
                mybutton.BackColor = Color.Red;
                return;
            }
            try
            {
                if (!(temp = smport.ReadLine()).Contains("Running"))
                {
                    MessageBox.Show("Not the Ref Radio port. Found: " + temp + "\n");
                    mybutton.BackColor = Color.Red;
                }
                else
                {
                    MessageBox.Show("Correct port");
                    mybutton.BackColor = Color.Green;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Not the Ref Radio port. Error: " + ex.Message);
                mybutton.BackColor = Color.Red;
            }
            smport.Close();
        }

        private void cbFlasherComPort_SelectionChangeCommitted(object sender, EventArgs e)
        {
            string temp = string.Empty;
            int start = 0;

            ComboBox mybutton = sender as ComboBox;
            start = mybutton.SelectedItem.ToString().IndexOf("(COM") + 1;
            temp = mybutton.SelectedItem.ToString().Substring(start, mybutton.SelectedItem.ToString().Length - start - 1);
            mybutton.Items.Clear();
            mybutton.Items.Add(temp);
            mybutton.SelectedIndex = 0;

            Flasher flasher = new Flasher(temp);
            flasher.Open();
            temp = flasher.GetStatus();
            if (!flasher.LastErrorStatus)        // if got some valid responses
            {
                MessageBox.Show("Correct port");
                mybutton.BackColor = Color.Green;
            }
            else
            {
                MessageBox.Show("Not the Flasher port. Error");
                mybutton.BackColor = Color.Red;
            }
            flasher.Close();
        }


        private void cbLEDSensorComPort_SelectionChangeCommitted(object sender, EventArgs e)
        {
            string temp = string.Empty;
            int start = 0;
            SerialPort ledport = new SerialPort();

            ComboBox mybutton = sender as ComboBox;
            start = mybutton.SelectedItem.ToString().IndexOf("(COM") + 1;
            temp = mybutton.SelectedItem.ToString().Substring(start, mybutton.SelectedItem.ToString().Length - start - 1);
            mybutton.Items.Clear();
            mybutton.Items.Add(temp);
            mybutton.SelectedIndex = 0;

            ledport.BaudRate = 19200;
            ledport.PortName = temp;
            ledport.ReadTimeout = 1000;
            ledport.Open();
            try
            {
                ledport.ReadLine();     // get a couple of lines
                ledport.ReadLine();     // get a couple of lines
            }
            catch (Exception)
            {
                MessageBox.Show("Not the LED Detector port.");
                ledport.Close();
                mybutton.BackColor = Color.Red;
                return;
            }

            try
            {
                if (!(temp = ledport.ReadLine()).Contains("w="))
                {
                    MessageBox.Show("Not the LED Detector port. Found: " + temp + "\n");
                    mybutton.BackColor = Color.Red;
                }
                else
                { 
                    MessageBox.Show("Correct port");
                    mybutton.BackColor = Color.Green;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Not the LED Detector port. Error: " + ex.Message);
                mybutton.BackColor = Color.Red;
            }
            ledport.Close();
        }




        private void comboBoxTSLEDDetectorType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox mybutton = sender as ComboBox;
            if (mybutton.Text == "USB")
            {
                comboBoxTSLEDDetectorComPort.Visible = true;
                textBoxTSRedRange.Text = "6200,6600";
                textBoxTSGreenRange.Text = "5400,5900";
                textBoxTSBlueRange.Text = "4200,4900";
            }
            else
            {
                comboBoxTSLEDDetectorComPort.Visible = false;
                textBoxTSRedRange.Text = "11000,13000";
                textBoxTSGreenRange.Text = "7000,9000";
                textBoxTSBlueRange.Text = "4200,4900";
            }
        }

        private void comboBoxTSAmbDLSType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox mybutton = sender as ComboBox;
            if (mybutton.Text == "RedLED")
                numericUpDownTSAmbDLSLEDV.Visible = true;
            else
                numericUpDownTSAmbDLSLEDV.Visible = false;

        }



        private void bAutofindRefRadio_Click(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();         // get list of serial ports
            string temp = string.Empty;
            bool willOpen = true;
            bool tookCmd = false;
            bool foundPort = false;
            string partno = string.Empty;
            string serialno = string.Empty;
            string model = string.Empty;

            TextBasedConsole tradio = new TextBasedConsole();
            PacketBasedConsole pradio = new PacketBasedConsole();
            pradio.CommandRecieveTimeoutMS = 1000;
            Button mybutton = sender as Button;
            mybutton.Text = "Looking";
            mybutton.BackColor = Color.Yellow;
            Application.DoEvents();
            foreach (var item in ports)
            {
                try // see if it will open
                {
                    willOpen = true;
                    if (sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.TestRadio).channels[0].type == FixtureDefintions.ChannelType.COBSPacketSerial)
                    {
                        pradio.PortName = item;
                        pradio.Open();       // try to open the port
                    }
                    else
                    {
                        tradio.Open(item);
                    }
                }
                catch (Exception)
                {
                    if (sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.TestRadio).channels[0].type == FixtureDefintions.ChannelType.COBSPacketSerial)
                    {
                        temp = pradio.LastErrorMessage;
                        willOpen = false;
                    }
                    else
                    {
                        temp = tradio.LastErrorMessage;
                        willOpen = false;
                    }
                }
                if (willOpen)  // if it will open, see if it will take the command
                {
                    try
                    {
                        if (sconfig.GetInstrumentData(FixtureDefintions.InstrumentID.TestRadio).channels[0].type == FixtureDefintions.ChannelType.COBSPacketSerial)
                        {
                            pradio.ClearBuffers();
                            tookCmd = pradio.GetHLAData(ref partno, ref serialno, ref model);
                            pradio.Close();
                            if (model.Contains(REF_RADIO_MODEL))
                            {
                                foundPort = true;
                            }
                        }
                        else
                        {
                            tradio.ClearBuffers();
                            tradio.WriteLine("d ver", 500);
                            foundPort = tradio.WaitForPrompt(500, "REF>", out partno);
                            tradio.Close();
                        }
                    }
                    catch (Exception)
                    {
                        tookCmd = false;
                    }
                }
                if (foundPort)  // if the port was found
                {
                    comboBoxTSTestRadioComPort.Items.Clear();
                    comboBoxTSTestRadioComPort.Items.Add(item);
                    comboBoxTSTestRadioComPort.SelectedIndex = 0;
                    comboBoxTSTestRadioComPort.BackColor = Color.Green;

                    break;
                }
            }
            mybutton.Text = "Auto Find";
            mybutton.UseVisualStyleBackColor = true;
            if (!foundPort)
            {
                comboBoxTSTestRadioComPort.BackColor = Color.Red;
                comboBoxTSTestRadioComPort.Text = "COM0";
            }

        }

        private void bAutofindFlasher_Click(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();         // get list of serial ports
            string temp = string.Empty;
            bool willOpen = true;
            bool foundPort = false;
            string partno = string.Empty;
            string serialno = string.Empty;
            string model = string.Empty;
            Flasher flasher = new Flasher();

            Button mybutton = sender as Button;
            mybutton.Text = "Looking";
            mybutton.BackColor = Color.Yellow;
            Application.DoEvents();
            foreach (var item in ports)
            {
                try // see if it will open
                {
                    willOpen = true;
                    flasher.Open(item);       // try to open the port
                }
                catch (Exception)
                {
                    temp = flasher.LastErrorMsg;
                    willOpen = false;
                }
                if (willOpen)  // if it will open, see if it will take the command
                {
                    flasher.ClearBuffers();
                    flasher.GetStatus();
                    foundPort = flasher.LastErrorStatus;
                    flasher.Close();
                }
                if (foundPort)  // if the port was found
                {
                    cbFlasherComPort.Items.Clear();
                    cbFlasherComPort.Items.Add(item);
                    cbFlasherComPort.SelectedIndex = 0;
                    cbFlasherComPort.BackColor = Color.Green;
                    mybutton.UseVisualStyleBackColor = true;
                    break;
                }
            }
            mybutton.Text = "Auto Find";
            mybutton.UseVisualStyleBackColor = true;
            if (!foundPort)
            {
                cbFlasherComPort.BackColor = Color.Red;
                cbFlasherComPort.Text = "COM0";
            }

        }


        /// <summary>
        /// Will try to auto find the CU port.
        /// </summary>
        /// <remarks>
        /// Assumes that there is a working DUT running DVTMAN in the fixture and console
        /// has been configured.
        /// Will turn on the unit
        ///     for each com port other than the console port
        ///         open the port as a CU port
        ///         do a p cu-test
        ///         if it passes, assume it is the correct port
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void autofindPM_Click(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();         // get list of serial ports
            //SensorPacketConsole dconsole = new SensorPacketConsole();
            //CUConsole cconsole = new CUConsole();
            //SensorDVTMANTests lclmanTests = new SensorDVTMANTests();
            //MainWindow.uutdata uutdata = new MainWindow.uutdata();
            //string msg = string.Empty;
            //LimitFileFunctions.TestToRun testToRun = new LimitFileFunctions.TestToRun();
            //string port = "COM0";
            //int status;

            //comboBoxTSCUComPort.BackColor = Color.Red;
            //Button mybutton = sender as Button;
            //mybutton.Text = "Looking";
            //Application.DoEvents();
            //if (sconfig.ConfigDUTComPort == "COM0")        // if dut com not configured
            //{
            //    MessageBox.Show("DUT Com Port must be configured first.");
            //    return;
            //}
            //lclFixtureDefintions.ControlDUTPower(1);             // power up DUT
            //dconsole.Open(sconfig.ConfigDUTComPort);
            //dconsole.ClearBuffers();
            //foreach (var item in ports)
            //{
            //    if (item != sconfig.ConfigDUTComPort)
            //    {
            //        cconsole.Open(item);
            //        status = lclmanTests.CUPortTest(testToRun, null, uutdata, out msg);
            //        cconsole.Close();
            //        if (status == 0)
            //        {
            //            port = item;
            //            break;
            //        }
            //    }
            //}
            
        }

        private void autofindSM_Click(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();         // get list of serial ports
            string temp = string.Empty;
            bool foundPort = false;
            SerialPort smport = new SerialPort();

            Button mybutton = sender as Button;
            mybutton.Text = "Looking";
            mybutton.BackColor = Color.Yellow;
            Application.DoEvents();
            foreach (var item in ports)
            {
                smport.BaudRate = 19200;
                smport.PortName = item;
                smport.NewLine = "\r";
                smport.ReadTimeout = 1000;
                smport.Open();
                smport.WriteLine("");               // need to clear any old stuff
                try
                {
                    smport.ReadLine();
                }
                catch (Exception)
                {
                    temp = string.Empty;
                }
                smport.WriteLine("SM_ID?");
                try
                {
                    if ((temp = smport.ReadLine()).Contains("SWITCH-MATE"))
                    {
                        cbSwitchMateCom.Items.Clear();
                        cbSwitchMateCom.Items.Add(item);
                        cbSwitchMateCom.SelectedIndex = 0;
                        smport.Close();
                        foundPort = true;
                        break;
                    }
                }
                catch (Exception)
                {
                    foundPort = false;
                }
                smport.Close();
            }
            mybutton.Text = "Auto Find";
            mybutton.UseVisualStyleBackColor = true;
            if (foundPort)
                cbSwitchMateCom.BackColor = Color.Green;
            else
            {
                cbSwitchMateCom.BackColor = Color.Red;
                cbFlasherComPort.Text = "COM0";
            }
        }

        private void autofindLED_Click(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();         // get list of serial ports
            string temp = string.Empty;
            bool willOpen = true;
            bool tookCmd = false;

            SerialPort ledport = new SerialPort();

            Button mybutton = sender as Button;
            mybutton.Text = "Looking";
            mybutton.BackColor = Color.Yellow;

            Application.DoEvents();
            foreach (var item in ports)
            {
                temp = string.Empty;
                try // see if it will open
                {
                    willOpen = true;
                    ledport.BaudRate = 19200;
                    ledport.PortName = item;
                    ledport.ReadTimeout = 1000;
                    ledport.Open();
                }
                catch (Exception ex)
                {
                    temp = ex.Message;
                    willOpen = false;
                }
                if (willOpen)  // if it will open, see if it will read values
                {
                    try
                    {
                        tookCmd = true;
                        ledport.ReadLine();     // get a couple of lines
                        ledport.ReadLine();
                        temp = ledport.ReadLine();     // get a couple of lines
                    }
                    catch (Exception ex)
                    {
                        temp = ex.Message;
                        ledport.Close();
                        tookCmd = false;
                    }

                }

                if (willOpen & tookCmd & temp.Contains("w="))  // if the port was found
                {
                    comboBoxTSLEDDetectorComPort.Items.Clear();
                    comboBoxTSLEDDetectorComPort.Items.Add(item);
                    comboBoxTSLEDDetectorComPort.SelectedIndex = 0;
                    comboBoxTSLEDDetectorComPort.BackColor = Color.Green;
                    ledport.Close();
                    break;
                }
                if (ledport.IsOpen)
                    ledport.Close();
            }
            mybutton.Text = "Auto Find";
            mybutton.UseVisualStyleBackColor = true;
            if (comboBoxTSLEDDetectorComPort.BackColor != Color.Green)
            {
                comboBoxTSLEDDetectorComPort.BackColor = Color.Red;
                comboBoxTSLEDDetectorComPort.Text = "COM0";
            }
        }

        /// <summary>
        /// Look for a reference radio.
        /// </summary>
        /// <remarks>
        /// It will look for something that response to a get HLA data command. If
        /// it finds one, if it does not have "CK-1U-01" then assumed that it is the DUT.
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bAutoFindDUTComPort_Click(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();         // get list of serial ports
            string temp = string.Empty;
            bool foundPort = false;
            bool powercontrol = true;           // true = fixture has DUT power control
            PacketBasedConsole pradio = new PacketBasedConsole();
            TextBasedConsole tradio = new TextBasedConsole();
            bool isTextBaseConsole = false;
            bool willOpen = true;
            bool tookCmd = true;
            string partno = string.Empty;
            string model = string.Empty;
            string serialno = string.Empty;
            List<FixtureDefintions.Instrument> initList = new List<FixtureDefintions.Instrument>();

            Button mybutton = sender as Button;

            if (sconfig.GetChannelData(FixtureDefintions.CH_DUT_POWER_CNTL) == null)    // there is no power control
            {
                MessageBox.Show("DUT running DVTMAN and has power.");
                Application.DoEvents();
                powercontrol = false;
            }
            else        // there is power control so let the fixture turn it on and off
            {
                MessageBox.Show("DUT running DVTMAN has to be loaded into the fixture.");
                Application.DoEvents();
            }

            if (powercontrol)
            {
                if (sconfig.GetChannelInstrument(FixtureDefintions.CH_DUT_POWER_CNTL).connection == string.Empty)      // if the power control has not been config
                {
                    MessageBox.Show("DUT Power Control must be configured first");
                    comboBoxTSDUTComPort.BackColor = Color.White;
                    mybutton.Text = "Auto Find";
                    return;
                }
                // get the instrument that has the DUT power control. Add to list so that it is the only instrument in the list
                initList.Add(sconfig.GetInstrumentData(sconfig.GetChannelInstrument(FixtureDefintions.CH_DUT_POWER_CNTL).instrumentID));
            }

            if (sconfig.GetChannelData(FixtureDefintions.CH_DUT_CONSOLE).type == FixtureDefintions.ChannelType.TextSerial)
                isTextBaseConsole = true;


            using (FixtureFunctions fixture = new FixtureFunctions(sconfig))
            {
                if (!fixture.InitFixture(initList))
                {
                    MessageBox.Show("Error initializing the fixture. " + fixture.LastFunctionErrorMessage);
                }
                else
                {
                    if (powercontrol)
                        fixture.ControlDUTPower(FixtureDefintions.PowerControl.ON, 1000);
                    else
                        MessageBox.Show("Make sure DUT has power.");
                    mybutton.Text = "Looking";
                    mybutton.BackColor = Color.Yellow;
                    Application.DoEvents();

                    foreach (var item in ports)
                    {
                        try // see if it will open
                        {
                            willOpen = true;
                            if (isTextBaseConsole)
                            {
                                tradio.Open(item);       // try to open the port
                            }
                            else
                            {
                                pradio.PortName = item;
                                pradio.Open();       // try to open the port
                            }
                        }
                        catch (Exception)
                        {
                            if (isTextBaseConsole)
                                temp = tradio.LastErrorMessage;
                            else
                                temp = pradio.LastErrorMessage;
                            willOpen = false;
                        }
                        if (willOpen)  // if it will open, see if it will take the command
                        {
                            try
                            {
                                if (isTextBaseConsole)
                                {
                                    tradio.ClearBuffers();
                                    tradio.WriteLine("", 1000);
                                    tookCmd = tradio.WaitForPrompt(1000, "REF>", out model);
                                    model = "CK-1U-01";        // this is fake out the last part
                                }
                                else
                                {
                                    pradio.ClearBuffers();
                                    tookCmd = pradio.GetHLAData(ref partno, ref serialno, ref model);
                                    pradio.Close();
                                }
                            }
                            catch (Exception)
                            {
                                tookCmd = false;
                            }
                        }
                        if (willOpen & tookCmd) // if it opened and it took the command, check result
                        {
                            try
                            {
                                if (!model.Contains("CK-1U-01"))
                                {
                                    foundPort = true;
                                }
                            }
                            catch (Exception)
                            {
                                foundPort = false;
                            }
                        }
                        if (foundPort)  // if the port was found
                        {
                            comboBoxTSDUTComPort.Items.Clear();
                            comboBoxTSDUTComPort.Items.Add(item);
                            comboBoxTSDUTComPort.SelectedIndex = 0;
                            comboBoxTSDUTComPort.BackColor = Color.Green;
                            break;
                        }
                    }
                    mybutton.Text = "Auto Find";
                    mybutton.UseVisualStyleBackColor = true;
                    if (comboBoxTSDUTComPort.BackColor != Color.Green)
                    {
                        comboBoxTSDUTComPort.BackColor = Color.Red;
                        comboBoxTSDUTComPort.Text = "COM0";
                    }
                    if (powercontrol)
                        fixture.ControlDUTPower(FixtureDefintions.PowerControl.OFF, 100);
                }
            }

        }
    }
}
