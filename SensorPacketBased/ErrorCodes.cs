﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestStationErrorCodes
{
    class ErrorCodes
    {
        public static class SystemGroups
        {
            public const int LimitsErrors = -1000;
            public const int ResultDatabaseErrors = -2000;
            public const int MACDatabaseErrors = -3000;
                                //Open,                   //  1 - database open error
                                //NUMBER_PER_Read,        //  2 - error reading NUMBER_PER field
                                //MAC_DataRead,           //  3 - error reading MAC data
                                //SN_Format,              //  4 - incorrect SN format
                                //MAC_Gen,                //  5 - error generating MACs
                                //LockingTable,           //  6 - error locking mac table
                                //MAC_NotValidForFamily,  //  7 - mac being used is not in the family pool
                                //MAC_AssignedToDiffSN,   //  8 - mac is assigned to a different sn
                                //MAC_Recording,          //  9 - error recording mac(s) in data base
                                //MAC_Mismatch,           //  10 - MAC does not match SN in database
                                //Not_Open,               //  12 - database not open when assumed it should be
                                //Last_MAC_Read,          //  13 - error reading the last mac used
                                //Read_StartStop,         //  14 - error reading the start/stop mac data
                                //MAC_OutOfRange,         //  15 - new mac is out of the start/stop range.

            public const int SNDatabaseErrors = -4000;
                                //open,                   // 1  - database open error
                                //locking,                // 2  - error locking the table
                                //recordingData,          // 3  - error trying to record data
                                //not_open,               // 4 -  expected database to be open
                                //last_sn_read,           // 5  - error reading the last serial number
                                //dup_check,              // 6  - database error looking for duplicate sn
                                //dup_sn_found,           // 7  - duplicate sn was found
                                //xxxxx,          // 8  - xxxxx
                                //mac_find_db_error,      // 9  - error looking for mac in productcode table

            public const int ProductDatabaseErrors = -5000;
            public const int FixtureErrorsBase = -6000;
            public const int ShopFloorErrors = -7000;
        }

        public static class System
        {
            public const int ParameterFormatError = -1;                 // parameter entered by operator is incorrect format
            public const int OperatorCancel = -2;                       // operator canceled 
            public const int InvalidTestDefined = -3;                   // test defined in the limit file is not a defined test
            public const int ProductDBMissingProdCode = -4;             // product code defined is not in the product database
            public const int TestNotImplemented = -5;                   // placeholder code that has not been wrote yet
            public const int MissingParameter = -6;                     // missing a manitory parameter
            public const int MissingLabelFile = -7;                     // label file missing
            public const int InvalidLabelFieldName = -8;                // the label field name does not exist in the label file
            public const int LabelPrintError = -9;                      // there was a error printing the label
            public const int ConfigFileOpenError = -10;                 // error opening the station configuration file
            public const int InvalidInstrumentDefined = -11;            // instrument called for is not defined
            public const int FixtureNotIntialized = -12;                // fixture has not been initialized

            public const int InvalidLimitOperation = -1000;             // a limit operation or type is invalid

            public const int ResultDBRecordCreateError = -2000;         // error trying to create a test record head
            public const int ResultDatabaseWriteFail = -2001;                 // error trying to write data to the result database


            public const int FixtureBase = -6000;                       // fixture componet failed to initialize

            public const int FlasherBase = -6100;                       // base code for flasher errors. See Flasher.cs for defines

            public const int DAQBase = -6150;                           // base of DAQ errors. See FixturInstrBase for defines

            public const int SFSErrorSendingResult = -7000;             // did not get a OK back when sending results
            public const int SFSFailNotSentToSFS = -7001;               // operator choose to not send fail to SFS
        }


        public static class DUT
        {
            public const int ValueOutsideOfLimit        = 100;
            public const int DupicateSN = 101;
            public const int PCBADataMismatch = 102;
            public const int NotInDVTMAN = 103;
            public const int AdvertNotFound = 104;
            public const int VersionCheckFailed = 105;
            public const int ProgramFlashFailed = 106;
            public const int DUTHardwareError = 107;
            public const int ChangedOverError = 108;
            public const int OperatorAbort = 109;
            public const int NotInManufactureMode = 110;
            public const int DutUnexpectedCmdReturn = 111;

            public const int CUComError                 = 200;
            public const int FailedToPerformCUFunction = 201;

            public const int DutProgramErrorBase        = 300;

            public const int RefusedByPFS = 400;

        }

        //public enum Program
        //{
        //    Success,                        // 0
        //    ValueOutsideOfLimit,            // 1  x
        //    DUTComError,                    // 2  
        //    DutUnexpectedCmdReturn,         // 3  x
        //    CUComError,                     // 4  x
        //    DupicateSN,                     // 5  x
        //    PCBADataMismatch,               // 6  x
        //    NotConfForChangeover,           // 7
        //    SeqFail,                        // 8
        //    NotInDVTMAN,                    // 9  x
        //    FailedToPerformCUFunction,      // 10  x
        //    OperatorAbort,                  // 11
        //    FixtureHardwareError,           // 12
        //    ChangedOver,                    // 13  x
        //    RefusedByPFS,                   // 14  x
        //    AdvertNotFound,                 // 15  x
        //    CommandTimeout,                 // 16  x
        //    DUTHardwareError,               // 17  x
        //    CmdResponseStatusFailed,        // 18  x
        //    CmdResponsePayloadLenWrong,     // 19  x
        //    CmdPacketCheckSumError,         // 20  x
        //    CmdMsgTypeNotDVTMAN,            // 21  x
        //    CmdResponseHeaderLenWrong,      // 22  x
        //    CmdPacketFormatError,           // 23  x
        //    VersionCheckFailed,             // 24  x
        //    ProgramFlashFailed,             // 25  x
        //}

        //static public string[] ProgramStrings =
        //{
        //    "Success",                              // 0
        //    "Value outside of limits",              // 1
        //    "DUT com error",                        // 2
        //    "DUT unexpected command return",        // 3
        //    "CU com error",                         // 4
        //    "Dupicate Serial Number found",         // 5
        //    "PCBA Data Mismatch",                   // 6
        //    "Not configured for Change-over",       // 7
        //    "Sequence failure",                     // 8
        //    "DUT not in DVTMAN",                    // 9
        //    "Failed to perform a CU function call", // 10
        //    "Operator abort",                       // 11
        //    "Fixture hardware error",               // 12
        //    "DUT has changed over",                 // 13
        //    "Refused by PFS",                       // 14
        //    "Advert not found",                     // 15
        //    "Command timeout",                      // 16
        //    "DUT Hardware error found",             // 17
        //    "Got a fail status response",           // 18
        //    "Command payload lenght incorrect",     // 19
        //    "Command checksum error",               // 20
        //    "Command Message type not DVTMAN",      // 21
        //    "Command response header to short",     // 22
        //    "Command packet format is wrong",       // 23
        //    "Version check failed",                 // 24
        //    "Failed to flash DUT",                  // 25
        //};

        ///// <summary>
        ///// Return the Error String for a error code.
        ///// </summary>
        ///// <param name="ErrorCode"></param>
        ///// <param name="ErrorString"></param>
        //static public string GetErrorString(int ErrorCode)
        //{
        //    string ErrorString = string.Empty;

        //    if (ErrorCode < 0)      // if a system error code
        //    {
        //        ErrorString = SystemStrings[Math.Abs(ErrorCode) - 1];
        //    }
        //    else
        //    {
        //        ErrorString = ProgramStrings[ErrorCode];

        //    }
        //    return ErrorString;
        //}
    }
}
