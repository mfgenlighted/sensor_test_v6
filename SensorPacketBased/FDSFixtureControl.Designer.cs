﻿namespace SensorManufSY2
{
    partial class FDSFixtureControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textLidStatus = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textPushButtonStatus = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TempProbeReading = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.DUT3p3vReading = new System.Windows.Forms.Label();
            this.checkBoxTestLamp = new System.Windows.Forms.CheckBox();
            this.numericUpDownLEDVoltage = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.buttonExit = new System.Windows.Forms.Button();
            this.checkBoxTestIndicator = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textLEDFreq = new System.Windows.Forms.Label();
            this.textLEDIntest = new System.Windows.Forms.Label();
            this.LEDColor = new System.Windows.Forms.TextBox();
            this.rtStatusWindow = new System.Windows.Forms.RichTextBox();
            this.nBTtimeout = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tbTestRadioMAC = new System.Windows.Forms.Label();
            this.bTestRaio = new System.Windows.Forms.Button();
            this.nudLQIM = new System.Windows.Forms.NumericUpDown();
            this.nudLQIT = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lbTestChannel = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tControlVoltage = new System.Windows.Forms.Label();
            this.RadioTest = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.nudPowerLevel = new System.Windows.Forms.NumericUpDown();
            this.bReftoDUT = new System.Windows.Forms.Button();
            this.bDUTtoRef = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lbOscStatus = new System.Windows.Forms.Label();
            this.lbPIRMin = new System.Windows.Forms.Label();
            this.lbPIRMax = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.bPIROscTest = new System.Windows.Forms.Button();
            this.bPCBAConfig = new System.Windows.Forms.Button();
            this.bHLAConfig = new System.Windows.Forms.Button();
            this.tControlVoltage2 = new System.Windows.Forms.Label();
            this.DebugWindowtoolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.bGetVersions = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.lbTagRadioMac = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.bMACConfig = new System.Windows.Forms.Button();
            this.bReadManData = new System.Windows.Forms.Button();
            this.bClearManData = new System.Windows.Forms.Button();
            this.bSetRadioParams = new System.Windows.Forms.Button();
            this.bSetTestRadioParams = new System.Windows.Forms.Button();
            this.bReadDUTRadioParams = new System.Windows.Forms.Button();
            this.bReadTestRadioParams = new System.Windows.Forms.Button();
            this.cbDisplayData = new System.Windows.Forms.CheckBox();
            this.bClearInputBuffers = new System.Windows.Forms.Button();
            this.cbSetRefRadio = new System.Windows.Forms.CheckBox();
            this.bHwConfig = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbNone = new System.Windows.Forms.RadioButton();
            this.rbBlue = new System.Windows.Forms.RadioButton();
            this.rbGreen = new System.Windows.Forms.RadioButton();
            this.rbRed = new System.Windows.Forms.RadioButton();
            this.bSetDim1 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.nudDim2Voltage = new System.Windows.Forms.NumericUpDown();
            this.nudDim1Voltage = new System.Windows.Forms.NumericUpDown();
            this.bReadSensors = new System.Windows.Forms.Button();
            this.tbAmbient = new System.Windows.Forms.TextBox();
            this.tbTemp = new System.Windows.Forms.TextBox();
            this.tbPIR = new System.Windows.Forms.TextBox();
            this.tbTempx = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.bPOST = new System.Windows.Forms.Button();
            this.bReboot = new System.Windows.Forms.Button();
            this.nudRebootWaitMS = new System.Windows.Forms.NumericUpDown();
            this.label22 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.bCalTemp = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.bProgramCode = new System.Windows.Forms.Button();
            this.bClearWindow = new System.Windows.Forms.Button();
            this.bStartbackground = new System.Windows.Forms.Button();
            this.nudMaxCOResponseSec = new System.Windows.Forms.NumericUpDown();
            this.nudMaxCUQueryResponseSec = new System.Windows.Forms.NumericUpDown();
            this.lbMaxCOResponseSec = new System.Windows.Forms.Label();
            this.lbMaxCUQuerySec = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLEDVoltage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nBTtimeout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLQIM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLQIT)).BeginInit();
            this.RadioTest.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPowerLevel)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDim2Voltage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDim1Voltage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRebootWaitMS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxCOResponseSec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxCUQueryResponseSec)).BeginInit();
            this.SuspendLayout();
            // 
            // textLidStatus
            // 
            this.textLidStatus.AutoSize = true;
            this.textLidStatus.Location = new System.Drawing.Point(184, 26);
            this.textLidStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.textLidStatus.Name = "textLidStatus";
            this.textLidStatus.Size = new System.Drawing.Size(29, 20);
            this.textLidStatus.TabIndex = 0;
            this.textLidStatus.Text = "----";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 26);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Lid Status";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 49);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(148, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Push Button Status";
            // 
            // textPushButtonStatus
            // 
            this.textPushButtonStatus.AutoSize = true;
            this.textPushButtonStatus.Location = new System.Drawing.Point(184, 49);
            this.textPushButtonStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.textPushButtonStatus.Name = "textPushButtonStatus";
            this.textPushButtonStatus.Size = new System.Drawing.Size(29, 20);
            this.textPushButtonStatus.TabIndex = 3;
            this.textPushButtonStatus.Text = "----";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 72);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 20);
            this.label4.TabIndex = 4;
            this.label4.Text = "Temp Probe";
            // 
            // TempProbeReading
            // 
            this.TempProbeReading.AutoSize = true;
            this.TempProbeReading.Location = new System.Drawing.Point(184, 72);
            this.TempProbeReading.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.TempProbeReading.Name = "TempProbeReading";
            this.TempProbeReading.Size = new System.Drawing.Size(19, 20);
            this.TempProbeReading.TabIndex = 5;
            this.TempProbeReading.Text = "--";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(18, 126);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(99, 20);
            this.label6.TabIndex = 6;
            this.label6.Text = "3.3V reading";
            // 
            // DUT3p3vReading
            // 
            this.DUT3p3vReading.AutoSize = true;
            this.DUT3p3vReading.Location = new System.Drawing.Point(184, 126);
            this.DUT3p3vReading.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.DUT3p3vReading.Name = "DUT3p3vReading";
            this.DUT3p3vReading.Size = new System.Drawing.Size(24, 20);
            this.DUT3p3vReading.TabIndex = 7;
            this.DUT3p3vReading.Text = "---";
            // 
            // checkBoxTestLamp
            // 
            this.checkBoxTestLamp.AutoSize = true;
            this.checkBoxTestLamp.Location = new System.Drawing.Point(57, 229);
            this.checkBoxTestLamp.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.checkBoxTestLamp.Name = "checkBoxTestLamp";
            this.checkBoxTestLamp.Size = new System.Drawing.Size(110, 24);
            this.checkBoxTestLamp.TabIndex = 8;
            this.checkBoxTestLamp.Text = "Test Lamp";
            this.checkBoxTestLamp.UseVisualStyleBackColor = true;
            this.checkBoxTestLamp.CheckedChanged += new System.EventHandler(this.checkBoxTestLamp_CheckedChanged);
            // 
            // numericUpDownLEDVoltage
            // 
            this.numericUpDownLEDVoltage.DecimalPlaces = 1;
            this.numericUpDownLEDVoltage.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownLEDVoltage.Location = new System.Drawing.Point(14, 266);
            this.numericUpDownLEDVoltage.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.numericUpDownLEDVoltage.Maximum = new decimal(new int[] {
            48,
            0,
            0,
            65536});
            this.numericUpDownLEDVoltage.Name = "numericUpDownLEDVoltage";
            this.numericUpDownLEDVoltage.Size = new System.Drawing.Size(62, 26);
            this.numericUpDownLEDVoltage.TabIndex = 9;
            this.numericUpDownLEDVoltage.ValueChanged += new System.EventHandler(this.numericUpDownLEDVoltage_ValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(86, 275);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 20);
            this.label8.TabIndex = 10;
            this.label8.Text = "LED Voltage";
            // 
            // buttonExit
            // 
            this.buttonExit.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonExit.Location = new System.Drawing.Point(42, 928);
            this.buttonExit.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(112, 35);
            this.buttonExit.TabIndex = 17;
            this.buttonExit.Text = "Exit";
            this.buttonExit.UseVisualStyleBackColor = true;
            // 
            // checkBoxTestIndicator
            // 
            this.checkBoxTestIndicator.AutoSize = true;
            this.checkBoxTestIndicator.Location = new System.Drawing.Point(57, 306);
            this.checkBoxTestIndicator.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.checkBoxTestIndicator.Name = "checkBoxTestIndicator";
            this.checkBoxTestIndicator.Size = new System.Drawing.Size(116, 24);
            this.checkBoxTestIndicator.TabIndex = 21;
            this.checkBoxTestIndicator.Text = "DUT Power";
            this.checkBoxTestIndicator.UseVisualStyleBackColor = true;
            this.checkBoxTestIndicator.CheckedChanged += new System.EventHandler(this.checkBoxTestIndicator_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 337);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 20);
            this.label5.TabIndex = 22;
            this.label5.Text = "LED detector";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(50, 366);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 20);
            this.label7.TabIndex = 23;
            this.label7.Text = "Freq";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(50, 386);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 20);
            this.label9.TabIndex = 24;
            this.label9.Text = "Intest";
            // 
            // textLEDFreq
            // 
            this.textLEDFreq.AutoSize = true;
            this.textLEDFreq.Location = new System.Drawing.Point(126, 366);
            this.textLEDFreq.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.textLEDFreq.Name = "textLEDFreq";
            this.textLEDFreq.Size = new System.Drawing.Size(24, 20);
            this.textLEDFreq.TabIndex = 25;
            this.textLEDFreq.Text = "---";
            // 
            // textLEDIntest
            // 
            this.textLEDIntest.AutoSize = true;
            this.textLEDIntest.Location = new System.Drawing.Point(126, 386);
            this.textLEDIntest.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.textLEDIntest.Name = "textLEDIntest";
            this.textLEDIntest.Size = new System.Drawing.Size(24, 20);
            this.textLEDIntest.TabIndex = 26;
            this.textLEDIntest.Text = "---";
            // 
            // LEDColor
            // 
            this.LEDColor.Location = new System.Drawing.Point(231, 366);
            this.LEDColor.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.LEDColor.Name = "LEDColor";
            this.LEDColor.ReadOnly = true;
            this.LEDColor.Size = new System.Drawing.Size(31, 26);
            this.LEDColor.TabIndex = 27;
            this.LEDColor.TabStop = false;
            // 
            // rtStatusWindow
            // 
            this.rtStatusWindow.Location = new System.Drawing.Point(435, 45);
            this.rtStatusWindow.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rtStatusWindow.Name = "rtStatusWindow";
            this.rtStatusWindow.Size = new System.Drawing.Size(1112, 333);
            this.rtStatusWindow.TabIndex = 36;
            this.rtStatusWindow.Text = "";
            // 
            // nBTtimeout
            // 
            this.nBTtimeout.Location = new System.Drawing.Point(94, 129);
            this.nBTtimeout.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.nBTtimeout.Name = "nBTtimeout";
            this.nBTtimeout.Size = new System.Drawing.Size(78, 26);
            this.nBTtimeout.TabIndex = 37;
            this.nBTtimeout.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(50, 98);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(180, 20);
            this.label10.TabIndex = 38;
            this.label10.Text = "Scan Timeout(Seconds)";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(18, 157);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(118, 20);
            this.label11.TabIndex = 41;
            this.label11.Text = "Test radio MAC";
            // 
            // tbTestRadioMAC
            // 
            this.tbTestRadioMAC.AutoSize = true;
            this.tbTestRadioMAC.Location = new System.Drawing.Point(184, 157);
            this.tbTestRadioMAC.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.tbTestRadioMAC.Name = "tbTestRadioMAC";
            this.tbTestRadioMAC.Size = new System.Drawing.Size(24, 20);
            this.tbTestRadioMAC.TabIndex = 42;
            this.tbTestRadioMAC.Text = "---";
            // 
            // bTestRaio
            // 
            this.bTestRaio.Location = new System.Drawing.Point(256, 106);
            this.bTestRaio.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bTestRaio.Name = "bTestRaio";
            this.bTestRaio.Size = new System.Drawing.Size(112, 35);
            this.bTestRaio.TabIndex = 45;
            this.bTestRaio.Text = "Test Radio";
            this.bTestRaio.UseVisualStyleBackColor = true;
            this.bTestRaio.Click += new System.EventHandler(this.bTestRaio_Click);
            // 
            // nudLQIM
            // 
            this.nudLQIM.Location = new System.Drawing.Point(117, 52);
            this.nudLQIM.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.nudLQIM.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudLQIM.Name = "nudLQIM";
            this.nudLQIM.Size = new System.Drawing.Size(88, 26);
            this.nudLQIM.TabIndex = 46;
            this.nudLQIM.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // nudLQIT
            // 
            this.nudLQIT.Location = new System.Drawing.Point(117, 91);
            this.nudLQIT.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.nudLQIT.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudLQIT.Name = "nudLQIT";
            this.nudLQIT.Size = new System.Drawing.Size(88, 26);
            this.nudLQIT.TabIndex = 47;
            this.nudLQIT.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(58, 55);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(48, 20);
            this.label13.TabIndex = 48;
            this.label13.Text = "LQIM";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(62, 95);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(44, 20);
            this.label14.TabIndex = 49;
            this.label14.Text = "LQIT";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(40, 20);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(65, 20);
            this.label15.TabIndex = 51;
            this.label15.Text = "channel";
            // 
            // lbTestChannel
            // 
            this.lbTestChannel.AutoSize = true;
            this.lbTestChannel.Location = new System.Drawing.Point(117, 18);
            this.lbTestChannel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbTestChannel.Name = "lbTestChannel";
            this.lbTestChannel.Size = new System.Drawing.Size(24, 20);
            this.lbTestChannel.TabIndex = 52;
            this.lbTestChannel.Text = "---";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(18, 103);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(119, 20);
            this.label16.TabIndex = 53;
            this.label16.Text = "Control Voltage";
            // 
            // tControlVoltage
            // 
            this.tControlVoltage.AutoSize = true;
            this.tControlVoltage.Location = new System.Drawing.Point(184, 103);
            this.tControlVoltage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.tControlVoltage.Name = "tControlVoltage";
            this.tControlVoltage.Size = new System.Drawing.Size(24, 20);
            this.tControlVoltage.TabIndex = 54;
            this.tControlVoltage.Text = "---";
            // 
            // RadioTest
            // 
            this.RadioTest.Controls.Add(this.label12);
            this.RadioTest.Controls.Add(this.nudPowerLevel);
            this.RadioTest.Controls.Add(this.lbTestChannel);
            this.RadioTest.Controls.Add(this.label15);
            this.RadioTest.Controls.Add(this.label14);
            this.RadioTest.Controls.Add(this.label13);
            this.RadioTest.Controls.Add(this.nudLQIT);
            this.RadioTest.Controls.Add(this.nudLQIM);
            this.RadioTest.Controls.Add(this.bTestRaio);
            this.RadioTest.Location = new System.Drawing.Point(390, 512);
            this.RadioTest.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.RadioTest.Name = "RadioTest";
            this.RadioTest.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.RadioTest.Size = new System.Drawing.Size(404, 151);
            this.RadioTest.TabIndex = 55;
            this.RadioTest.TabStop = false;
            this.RadioTest.Text = "Radio Test";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(256, 31);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(94, 20);
            this.label12.TabIndex = 54;
            this.label12.Text = "Power Level";
            // 
            // nudPowerLevel
            // 
            this.nudPowerLevel.Location = new System.Drawing.Point(256, 63);
            this.nudPowerLevel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.nudPowerLevel.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudPowerLevel.Name = "nudPowerLevel";
            this.nudPowerLevel.Size = new System.Drawing.Size(98, 26);
            this.nudPowerLevel.TabIndex = 53;
            this.DebugWindowtoolTip1.SetToolTip(this.nudPowerLevel, "Power level 0=max, 15=lowest");
            // 
            // bReftoDUT
            // 
            this.bReftoDUT.Location = new System.Drawing.Point(76, 57);
            this.bReftoDUT.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bReftoDUT.Name = "bReftoDUT";
            this.bReftoDUT.Size = new System.Drawing.Size(112, 35);
            this.bReftoDUT.TabIndex = 40;
            this.bReftoDUT.Text = "Ref to DUT";
            this.bReftoDUT.UseVisualStyleBackColor = true;
            this.bReftoDUT.Click += new System.EventHandler(this.bReftoDUT_Click);
            // 
            // bDUTtoRef
            // 
            this.bDUTtoRef.Location = new System.Drawing.Point(76, 22);
            this.bDUTtoRef.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bDUTtoRef.Name = "bDUTtoRef";
            this.bDUTtoRef.Size = new System.Drawing.Size(112, 35);
            this.bDUTtoRef.TabIndex = 39;
            this.bDUTtoRef.Text = "DUT to Ref";
            this.bDUTtoRef.UseVisualStyleBackColor = true;
            this.bDUTtoRef.Click += new System.EventHandler(this.bDUTtoRef_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lbOscStatus);
            this.groupBox3.Controls.Add(this.lbPIRMin);
            this.groupBox3.Controls.Add(this.lbPIRMax);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.bPIROscTest);
            this.groupBox3.Location = new System.Drawing.Point(386, 675);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox3.Size = new System.Drawing.Size(408, 105);
            this.groupBox3.TabIndex = 57;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "PIR Test";
            // 
            // lbOscStatus
            // 
            this.lbOscStatus.AutoSize = true;
            this.lbOscStatus.Location = new System.Drawing.Point(334, 29);
            this.lbOscStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbOscStatus.Name = "lbOscStatus";
            this.lbOscStatus.Size = new System.Drawing.Size(24, 20);
            this.lbOscStatus.TabIndex = 5;
            this.lbOscStatus.Text = "---";
            // 
            // lbPIRMin
            // 
            this.lbPIRMin.AutoSize = true;
            this.lbPIRMin.Location = new System.Drawing.Point(256, 62);
            this.lbPIRMin.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbPIRMin.Name = "lbPIRMin";
            this.lbPIRMin.Size = new System.Drawing.Size(24, 20);
            this.lbPIRMin.TabIndex = 4;
            this.lbPIRMin.Text = "---";
            // 
            // lbPIRMax
            // 
            this.lbPIRMax.AutoSize = true;
            this.lbPIRMax.Location = new System.Drawing.Point(255, 31);
            this.lbPIRMax.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbPIRMax.Name = "lbPIRMax";
            this.lbPIRMax.Size = new System.Drawing.Size(24, 20);
            this.lbPIRMax.TabIndex = 3;
            this.lbPIRMax.Text = "---";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(210, 62);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(34, 20);
            this.label18.TabIndex = 2;
            this.label18.Text = "Min";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(206, 31);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(38, 20);
            this.label17.TabIndex = 1;
            this.label17.Text = "Max";
            // 
            // bPIROscTest
            // 
            this.bPIROscTest.Location = new System.Drawing.Point(34, 48);
            this.bPIROscTest.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bPIROscTest.Name = "bPIROscTest";
            this.bPIROscTest.Size = new System.Drawing.Size(135, 35);
            this.bPIROscTest.TabIndex = 0;
            this.bPIROscTest.Text = "Oscillation Test";
            this.bPIROscTest.UseVisualStyleBackColor = true;
            this.bPIROscTest.Click += new System.EventHandler(this.bPIROscTest_Click);
            // 
            // bPCBAConfig
            // 
            this.bPCBAConfig.Location = new System.Drawing.Point(20, 437);
            this.bPCBAConfig.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bPCBAConfig.Name = "bPCBAConfig";
            this.bPCBAConfig.Size = new System.Drawing.Size(135, 35);
            this.bPCBAConfig.TabIndex = 58;
            this.bPCBAConfig.Text = "PCBA Config";
            this.DebugWindowtoolTip1.SetToolTip(this.bPCBAConfig, "Program PCBA SN. Does not display\r\nin status window. Result in Bluetooth\r\nTest Wi" +
        "ndow.");
            this.bPCBAConfig.UseVisualStyleBackColor = true;
            this.bPCBAConfig.Click += new System.EventHandler(this.bPCBAConfig_Click);
            // 
            // bHLAConfig
            // 
            this.bHLAConfig.Location = new System.Drawing.Point(20, 472);
            this.bHLAConfig.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bHLAConfig.Name = "bHLAConfig";
            this.bHLAConfig.Size = new System.Drawing.Size(135, 35);
            this.bHLAConfig.TabIndex = 59;
            this.bHLAConfig.Text = "HLA Config";
            this.DebugWindowtoolTip1.SetToolTip(this.bHLAConfig, "Program HLA data based on\r\nconfiguration.");
            this.bHLAConfig.UseVisualStyleBackColor = true;
            this.bHLAConfig.Click += new System.EventHandler(this.bHLAConfig_Click);
            // 
            // tControlVoltage2
            // 
            this.tControlVoltage2.AutoSize = true;
            this.tControlVoltage2.Location = new System.Drawing.Point(226, 103);
            this.tControlVoltage2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.tControlVoltage2.Name = "tControlVoltage2";
            this.tControlVoltage2.Size = new System.Drawing.Size(24, 20);
            this.tControlVoltage2.TabIndex = 60;
            this.tControlVoltage2.Text = "---";
            // 
            // bGetVersions
            // 
            this.bGetVersions.Location = new System.Drawing.Point(231, 615);
            this.bGetVersions.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bGetVersions.Name = "bGetVersions";
            this.bGetVersions.Size = new System.Drawing.Size(135, 35);
            this.bGetVersions.TabIndex = 66;
            this.bGetVersions.Text = "Get Versions";
            this.DebugWindowtoolTip1.SetToolTip(this.bGetVersions, "Program HLA data based on\r\nconfiguration.");
            this.bGetVersions.UseVisualStyleBackColor = true;
            this.bGetVersions.Click += new System.EventHandler(this.bGetVersions_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(22, 183);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(120, 20);
            this.label19.TabIndex = 62;
            this.label19.Text = "BLE Radio Mac";
            // 
            // lbTagRadioMac
            // 
            this.lbTagRadioMac.AutoSize = true;
            this.lbTagRadioMac.Location = new System.Drawing.Point(189, 182);
            this.lbTagRadioMac.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbTagRadioMac.Name = "lbTagRadioMac";
            this.lbTagRadioMac.Size = new System.Drawing.Size(24, 20);
            this.lbTagRadioMac.TabIndex = 63;
            this.lbTagRadioMac.Text = "---";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 412);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 20);
            this.label3.TabIndex = 65;
            this.label3.Text = "DUT Command";
            // 
            // bMACConfig
            // 
            this.bMACConfig.Location = new System.Drawing.Point(20, 508);
            this.bMACConfig.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bMACConfig.Name = "bMACConfig";
            this.bMACConfig.Size = new System.Drawing.Size(112, 35);
            this.bMACConfig.TabIndex = 67;
            this.bMACConfig.Text = "MAC Config";
            this.bMACConfig.UseVisualStyleBackColor = true;
            this.bMACConfig.Click += new System.EventHandler(this.bMACConfig_Click);
            // 
            // bReadManData
            // 
            this.bReadManData.Location = new System.Drawing.Point(164, 437);
            this.bReadManData.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bReadManData.Name = "bReadManData";
            this.bReadManData.Size = new System.Drawing.Size(142, 35);
            this.bReadManData.TabIndex = 68;
            this.bReadManData.Text = "Read Man Data";
            this.bReadManData.UseVisualStyleBackColor = true;
            this.bReadManData.Click += new System.EventHandler(this.bReadManData_Click);
            // 
            // bClearManData
            // 
            this.bClearManData.Location = new System.Drawing.Point(164, 528);
            this.bClearManData.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bClearManData.Name = "bClearManData";
            this.bClearManData.Size = new System.Drawing.Size(142, 35);
            this.bClearManData.TabIndex = 69;
            this.bClearManData.Text = "Clear Man Data";
            this.bClearManData.UseVisualStyleBackColor = true;
            this.bClearManData.Click += new System.EventHandler(this.bClearManData_Click);
            // 
            // bSetRadioParams
            // 
            this.bSetRadioParams.Location = new System.Drawing.Point(435, 432);
            this.bSetRadioParams.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bSetRadioParams.Name = "bSetRadioParams";
            this.bSetRadioParams.Size = new System.Drawing.Size(196, 35);
            this.bSetRadioParams.TabIndex = 70;
            this.bSetRadioParams.Text = "Set DUT Radio Params";
            this.bSetRadioParams.UseVisualStyleBackColor = true;
            this.bSetRadioParams.Click += new System.EventHandler(this.bSetRadioParams_Click);
            // 
            // bSetTestRadioParams
            // 
            this.bSetTestRadioParams.Location = new System.Drawing.Point(435, 468);
            this.bSetTestRadioParams.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bSetTestRadioParams.Name = "bSetTestRadioParams";
            this.bSetTestRadioParams.Size = new System.Drawing.Size(196, 35);
            this.bSetTestRadioParams.TabIndex = 71;
            this.bSetTestRadioParams.Text = "Set Test Radio Params";
            this.bSetTestRadioParams.UseVisualStyleBackColor = true;
            this.bSetTestRadioParams.Click += new System.EventHandler(this.bSetRefRadioParams_Click);
            // 
            // bReadDUTRadioParams
            // 
            this.bReadDUTRadioParams.Location = new System.Drawing.Point(640, 432);
            this.bReadDUTRadioParams.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bReadDUTRadioParams.Name = "bReadDUTRadioParams";
            this.bReadDUTRadioParams.Size = new System.Drawing.Size(112, 35);
            this.bReadDUTRadioParams.TabIndex = 72;
            this.bReadDUTRadioParams.Text = "Read";
            this.bReadDUTRadioParams.UseVisualStyleBackColor = true;
            this.bReadDUTRadioParams.Click += new System.EventHandler(this.bReadDUTRadioParams_Click);
            // 
            // bReadTestRadioParams
            // 
            this.bReadTestRadioParams.Location = new System.Drawing.Point(640, 468);
            this.bReadTestRadioParams.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bReadTestRadioParams.Name = "bReadTestRadioParams";
            this.bReadTestRadioParams.Size = new System.Drawing.Size(112, 35);
            this.bReadTestRadioParams.TabIndex = 73;
            this.bReadTestRadioParams.Text = "Read";
            this.bReadTestRadioParams.UseVisualStyleBackColor = true;
            this.bReadTestRadioParams.Click += new System.EventHandler(this.bReadTestRadioParams_Click);
            // 
            // cbDisplayData
            // 
            this.cbDisplayData.AutoSize = true;
            this.cbDisplayData.Location = new System.Drawing.Point(164, 406);
            this.cbDisplayData.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbDisplayData.Name = "cbDisplayData";
            this.cbDisplayData.Size = new System.Drawing.Size(122, 24);
            this.cbDisplayData.TabIndex = 74;
            this.cbDisplayData.Text = "Display data";
            this.cbDisplayData.UseVisualStyleBackColor = true;
            this.cbDisplayData.CheckedChanged += new System.EventHandler(this.cbDisplayData_CheckedChanged);
            // 
            // bClearInputBuffers
            // 
            this.bClearInputBuffers.Location = new System.Drawing.Point(164, 483);
            this.bClearInputBuffers.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bClearInputBuffers.Name = "bClearInputBuffers";
            this.bClearInputBuffers.Size = new System.Drawing.Size(174, 35);
            this.bClearInputBuffers.TabIndex = 75;
            this.bClearInputBuffers.Text = "Clear Input Buffers";
            this.bClearInputBuffers.UseVisualStyleBackColor = true;
            this.bClearInputBuffers.Click += new System.EventHandler(this.bClearInputBuffers_Click);
            // 
            // cbSetRefRadio
            // 
            this.cbSetRefRadio.AutoSize = true;
            this.cbSetRefRadio.Location = new System.Drawing.Point(298, 406);
            this.cbSetRefRadio.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbSetRefRadio.Name = "cbSetRefRadio";
            this.cbSetRefRadio.Size = new System.Drawing.Size(136, 24);
            this.cbSetRefRadio.TabIndex = 76;
            this.cbSetRefRadio.Text = "Set Ref Radio";
            this.cbSetRefRadio.UseVisualStyleBackColor = true;
            // 
            // bHwConfig
            // 
            this.bHwConfig.Location = new System.Drawing.Point(24, 543);
            this.bHwConfig.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bHwConfig.Name = "bHwConfig";
            this.bHwConfig.Size = new System.Drawing.Size(112, 35);
            this.bHwConfig.TabIndex = 77;
            this.bHwConfig.Text = "HW Config";
            this.bHwConfig.UseVisualStyleBackColor = true;
            this.bHwConfig.Click += new System.EventHandler(this.bHwConfig_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.bDUTtoRef);
            this.groupBox1.Controls.Add(this.nBTtimeout);
            this.groupBox1.Controls.Add(this.bReftoDUT);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Location = new System.Drawing.Point(802, 497);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(300, 169);
            this.groupBox1.TabIndex = 78;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Bluetooth Test";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbNone);
            this.groupBox2.Controls.Add(this.rbBlue);
            this.groupBox2.Controls.Add(this.rbGreen);
            this.groupBox2.Controls.Add(this.rbRed);
            this.groupBox2.Location = new System.Drawing.Point(252, 789);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Size = new System.Drawing.Size(141, 152);
            this.groupBox2.TabIndex = 79;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "LED Control";
            // 
            // rbNone
            // 
            this.rbNone.AutoSize = true;
            this.rbNone.Location = new System.Drawing.Point(9, 117);
            this.rbNone.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rbNone.Name = "rbNone";
            this.rbNone.Size = new System.Drawing.Size(72, 24);
            this.rbNone.TabIndex = 3;
            this.rbNone.TabStop = true;
            this.rbNone.Text = "None";
            this.rbNone.UseVisualStyleBackColor = true;
            this.rbNone.CheckedChanged += new System.EventHandler(this.cbRed_CheckedChanged);
            // 
            // rbBlue
            // 
            this.rbBlue.AutoSize = true;
            this.rbBlue.Location = new System.Drawing.Point(9, 82);
            this.rbBlue.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rbBlue.Name = "rbBlue";
            this.rbBlue.Size = new System.Drawing.Size(66, 24);
            this.rbBlue.TabIndex = 2;
            this.rbBlue.TabStop = true;
            this.rbBlue.Text = "Blue";
            this.rbBlue.UseVisualStyleBackColor = true;
            this.rbBlue.CheckedChanged += new System.EventHandler(this.cbRed_CheckedChanged);
            // 
            // rbGreen
            // 
            this.rbGreen.AutoSize = true;
            this.rbGreen.Location = new System.Drawing.Point(9, 55);
            this.rbGreen.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rbGreen.Name = "rbGreen";
            this.rbGreen.Size = new System.Drawing.Size(79, 24);
            this.rbGreen.TabIndex = 1;
            this.rbGreen.TabStop = true;
            this.rbGreen.Text = "Green";
            this.rbGreen.UseVisualStyleBackColor = true;
            this.rbGreen.CheckedChanged += new System.EventHandler(this.cbRed_CheckedChanged);
            // 
            // rbRed
            // 
            this.rbRed.AutoSize = true;
            this.rbRed.Location = new System.Drawing.Point(10, 29);
            this.rbRed.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rbRed.Name = "rbRed";
            this.rbRed.Size = new System.Drawing.Size(64, 24);
            this.rbRed.TabIndex = 0;
            this.rbRed.TabStop = true;
            this.rbRed.Text = "Red";
            this.rbRed.UseVisualStyleBackColor = true;
            this.rbRed.CheckedChanged += new System.EventHandler(this.cbRed_CheckedChanged);
            // 
            // bSetDim1
            // 
            this.bSetDim1.Location = new System.Drawing.Point(27, 109);
            this.bSetDim1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bSetDim1.Name = "bSetDim1";
            this.bSetDim1.Size = new System.Drawing.Size(112, 35);
            this.bSetDim1.TabIndex = 80;
            this.bSetDim1.Text = "Set Dim";
            this.bSetDim1.UseVisualStyleBackColor = true;
            this.bSetDim1.Click += new System.EventHandler(this.bSetDim1_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.nudDim2Voltage);
            this.groupBox4.Controls.Add(this.nudDim1Voltage);
            this.groupBox4.Controls.Add(this.bSetDim1);
            this.groupBox4.Location = new System.Drawing.Point(27, 752);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox4.Size = new System.Drawing.Size(177, 154);
            this.groupBox4.TabIndex = 82;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Dim outputs";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(22, 74);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(46, 20);
            this.label21.TabIndex = 85;
            this.label21.Text = "Dim2";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(24, 37);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(46, 20);
            this.label20.TabIndex = 84;
            this.label20.Text = "Dim1";
            // 
            // nudDim2Voltage
            // 
            this.nudDim2Voltage.Location = new System.Drawing.Point(78, 69);
            this.nudDim2Voltage.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.nudDim2Voltage.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudDim2Voltage.Name = "nudDim2Voltage";
            this.nudDim2Voltage.Size = new System.Drawing.Size(62, 26);
            this.nudDim2Voltage.TabIndex = 83;
            // 
            // nudDim1Voltage
            // 
            this.nudDim1Voltage.Location = new System.Drawing.Point(78, 29);
            this.nudDim1Voltage.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.nudDim1Voltage.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudDim1Voltage.Name = "nudDim1Voltage";
            this.nudDim1Voltage.Size = new System.Drawing.Size(62, 26);
            this.nudDim1Voltage.TabIndex = 82;
            // 
            // bReadSensors
            // 
            this.bReadSensors.Location = new System.Drawing.Point(453, 789);
            this.bReadSensors.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bReadSensors.Name = "bReadSensors";
            this.bReadSensors.Size = new System.Drawing.Size(142, 35);
            this.bReadSensors.TabIndex = 83;
            this.bReadSensors.Text = "Read Sensors";
            this.bReadSensors.UseVisualStyleBackColor = true;
            this.bReadSensors.Click += new System.EventHandler(this.bReadSensors_Click);
            // 
            // tbAmbient
            // 
            this.tbAmbient.Location = new System.Drawing.Point(560, 865);
            this.tbAmbient.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbAmbient.Name = "tbAmbient";
            this.tbAmbient.Size = new System.Drawing.Size(148, 26);
            this.tbAmbient.TabIndex = 84;
            // 
            // tbTemp
            // 
            this.tbTemp.Location = new System.Drawing.Point(560, 926);
            this.tbTemp.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbTemp.Name = "tbTemp";
            this.tbTemp.Size = new System.Drawing.Size(148, 26);
            this.tbTemp.TabIndex = 86;
            // 
            // tbPIR
            // 
            this.tbPIR.Location = new System.Drawing.Point(560, 834);
            this.tbPIR.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbPIR.Name = "tbPIR";
            this.tbPIR.Size = new System.Drawing.Size(148, 26);
            this.tbPIR.TabIndex = 87;
            // 
            // tbTempx
            // 
            this.tbTempx.AutoSize = true;
            this.tbTempx.Location = new System.Drawing.Point(430, 931);
            this.tbTempx.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.tbTempx.Name = "tbTempx";
            this.tbTempx.Size = new System.Drawing.Size(49, 20);
            this.tbTempx.TabIndex = 88;
            this.tbTempx.Text = "Temp";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(430, 869);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(68, 20);
            this.label24.TabIndex = 90;
            this.label24.Text = "Ambient";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(430, 845);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(36, 20);
            this.label25.TabIndex = 91;
            this.label25.Text = "PIR";
            // 
            // bPOST
            // 
            this.bPOST.Location = new System.Drawing.Point(849, 737);
            this.bPOST.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bPOST.Name = "bPOST";
            this.bPOST.Size = new System.Drawing.Size(112, 35);
            this.bPOST.TabIndex = 92;
            this.bPOST.Text = "POST";
            this.bPOST.UseVisualStyleBackColor = true;
            this.bPOST.Click += new System.EventHandler(this.bPOST_Click);
            // 
            // bReboot
            // 
            this.bReboot.Location = new System.Drawing.Point(849, 789);
            this.bReboot.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bReboot.Name = "bReboot";
            this.bReboot.Size = new System.Drawing.Size(112, 35);
            this.bReboot.TabIndex = 93;
            this.bReboot.Text = "Reboot";
            this.bReboot.UseVisualStyleBackColor = true;
            this.bReboot.Click += new System.EventHandler(this.bReboot_Click);
            // 
            // nudRebootWaitMS
            // 
            this.nudRebootWaitMS.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.nudRebootWaitMS.Location = new System.Drawing.Point(972, 792);
            this.nudRebootWaitMS.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.nudRebootWaitMS.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudRebootWaitMS.Name = "nudRebootWaitMS";
            this.nudRebootWaitMS.Size = new System.Drawing.Size(84, 26);
            this.nudRebootWaitMS.TabIndex = 94;
            this.nudRebootWaitMS.Value = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(954, 768);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(129, 20);
            this.label22.TabIndex = 95;
            this.label22.Text = "max wait time ms";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1157, 525);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(176, 35);
            this.button1.TabIndex = 96;
            this.button1.Text = "Change Over";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // bCalTemp
            // 
            this.bCalTemp.Location = new System.Drawing.Point(33, 635);
            this.bCalTemp.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bCalTemp.Name = "bCalTemp";
            this.bCalTemp.Size = new System.Drawing.Size(112, 35);
            this.bCalTemp.TabIndex = 97;
            this.bCalTemp.Text = "Cal Temp";
            this.bCalTemp.UseVisualStyleBackColor = true;
            this.bCalTemp.Click += new System.EventHandler(this.bCalTemp_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(27, 591);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(159, 35);
            this.button2.TabIndex = 98;
            this.button2.Text = "Config all man data";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.bProgAllManData_Click);
            // 
            // bProgramCode
            // 
            this.bProgramCode.Location = new System.Drawing.Point(194, 675);
            this.bProgramCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bProgramCode.Name = "bProgramCode";
            this.bProgramCode.Size = new System.Drawing.Size(148, 35);
            this.bProgramCode.TabIndex = 99;
            this.bProgramCode.Text = "Program Code";
            this.bProgramCode.UseVisualStyleBackColor = true;
            this.bProgramCode.Click += new System.EventHandler(this.bProgramCode_Click);
            // 
            // bClearWindow
            // 
            this.bClearWindow.Location = new System.Drawing.Point(1401, 391);
            this.bClearWindow.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bClearWindow.Name = "bClearWindow";
            this.bClearWindow.Size = new System.Drawing.Size(129, 35);
            this.bClearWindow.TabIndex = 100;
            this.bClearWindow.Text = "Clear Window";
            this.bClearWindow.UseVisualStyleBackColor = true;
            this.bClearWindow.Click += new System.EventHandler(this.bClearWindow_Click);
            // 
            // bStartbackground
            // 
            this.bStartbackground.Location = new System.Drawing.Point(294, 34);
            this.bStartbackground.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bStartbackground.Name = "bStartbackground";
            this.bStartbackground.Size = new System.Drawing.Size(112, 35);
            this.bStartbackground.TabIndex = 101;
            this.bStartbackground.Text = "start";
            this.bStartbackground.UseVisualStyleBackColor = true;
            this.bStartbackground.Click += new System.EventHandler(this.bStartbackground_Click);
            // 
            // nudMaxCOResponseSec
            // 
            this.nudMaxCOResponseSec.Location = new System.Drawing.Point(1190, 596);
            this.nudMaxCOResponseSec.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.nudMaxCOResponseSec.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudMaxCOResponseSec.Name = "nudMaxCOResponseSec";
            this.nudMaxCOResponseSec.Size = new System.Drawing.Size(88, 26);
            this.nudMaxCOResponseSec.TabIndex = 102;
            this.nudMaxCOResponseSec.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // nudMaxCUQueryResponseSec
            // 
            this.nudMaxCUQueryResponseSec.Location = new System.Drawing.Point(1190, 653);
            this.nudMaxCUQueryResponseSec.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.nudMaxCUQueryResponseSec.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudMaxCUQueryResponseSec.Name = "nudMaxCUQueryResponseSec";
            this.nudMaxCUQueryResponseSec.Size = new System.Drawing.Size(88, 26);
            this.nudMaxCUQueryResponseSec.TabIndex = 103;
            this.nudMaxCUQueryResponseSec.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // lbMaxCOResponseSec
            // 
            this.lbMaxCOResponseSec.AutoSize = true;
            this.lbMaxCOResponseSec.Location = new System.Drawing.Point(1153, 575);
            this.lbMaxCOResponseSec.Name = "lbMaxCOResponseSec";
            this.lbMaxCOResponseSec.Size = new System.Drawing.Size(247, 20);
            this.lbMaxCOResponseSec.TabIndex = 104;
            this.lbMaxCOResponseSec.Text = "Max sec for changeover response";
            // 
            // lbMaxCUQuerySec
            // 
            this.lbMaxCUQuerySec.AutoSize = true;
            this.lbMaxCUQuerySec.Location = new System.Drawing.Point(1153, 625);
            this.lbMaxCUQuerySec.Name = "lbMaxCUQuerySec";
            this.lbMaxCUQuerySec.Size = new System.Drawing.Size(160, 20);
            this.lbMaxCUQuerySec.TabIndex = 105;
            this.lbMaxCUQuerySec.Text = "Max sec for CU query";
            // 
            // FDSFixtureControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1581, 982);
            this.ControlBox = false;
            this.Controls.Add(this.lbMaxCUQuerySec);
            this.Controls.Add(this.lbMaxCOResponseSec);
            this.Controls.Add(this.nudMaxCUQueryResponseSec);
            this.Controls.Add(this.nudMaxCOResponseSec);
            this.Controls.Add(this.bStartbackground);
            this.Controls.Add(this.bClearWindow);
            this.Controls.Add(this.bProgramCode);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.bCalTemp);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.nudRebootWaitMS);
            this.Controls.Add(this.bReboot);
            this.Controls.Add(this.bPOST);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.tbTempx);
            this.Controls.Add(this.tbPIR);
            this.Controls.Add(this.tbTemp);
            this.Controls.Add(this.tbAmbient);
            this.Controls.Add(this.bReadSensors);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.rtStatusWindow);
            this.Controls.Add(this.bHwConfig);
            this.Controls.Add(this.cbSetRefRadio);
            this.Controls.Add(this.bClearInputBuffers);
            this.Controls.Add(this.cbDisplayData);
            this.Controls.Add(this.bReadTestRadioParams);
            this.Controls.Add(this.bReadDUTRadioParams);
            this.Controls.Add(this.bSetTestRadioParams);
            this.Controls.Add(this.bSetRadioParams);
            this.Controls.Add(this.bClearManData);
            this.Controls.Add(this.bReadManData);
            this.Controls.Add(this.bMACConfig);
            this.Controls.Add(this.bGetVersions);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lbTagRadioMac);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.tControlVoltage2);
            this.Controls.Add(this.bHLAConfig);
            this.Controls.Add(this.bPCBAConfig);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.RadioTest);
            this.Controls.Add(this.tControlVoltage);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.tbTestRadioMAC);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.LEDColor);
            this.Controls.Add(this.textLEDIntest);
            this.Controls.Add(this.textLEDFreq);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.checkBoxTestIndicator);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.numericUpDownLEDVoltage);
            this.Controls.Add(this.checkBoxTestLamp);
            this.Controls.Add(this.DUT3p3vReading);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TempProbeReading);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textPushButtonStatus);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textLidStatus);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FDSFixtureControl";
            this.Text = "FDSFixtureControl";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FDSFixtureControl_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLEDVoltage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nBTtimeout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLQIM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLQIT)).EndInit();
            this.RadioTest.ResumeLayout(false);
            this.RadioTest.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPowerLevel)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDim2Voltage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDim1Voltage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRebootWaitMS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxCOResponseSec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxCUQueryResponseSec)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label textLidStatus;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label textPushButtonStatus;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label TempProbeReading;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label DUT3p3vReading;
        private System.Windows.Forms.CheckBox checkBoxTestLamp;
        private System.Windows.Forms.NumericUpDown numericUpDownLEDVoltage;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.CheckBox checkBoxTestIndicator;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label textLEDFreq;
        private System.Windows.Forms.Label textLEDIntest;
        private System.Windows.Forms.TextBox LEDColor;
        private System.Windows.Forms.RichTextBox rtStatusWindow;
        private System.Windows.Forms.NumericUpDown nBTtimeout;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label tbTestRadioMAC;
        private System.Windows.Forms.Button bTestRaio;
        private System.Windows.Forms.NumericUpDown nudLQIM;
        private System.Windows.Forms.NumericUpDown nudLQIT;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lbTestChannel;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label tControlVoltage;
        private System.Windows.Forms.GroupBox RadioTest;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lbPIRMin;
        private System.Windows.Forms.Label lbPIRMax;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button bPIROscTest;
        private System.Windows.Forms.Label lbOscStatus;
        private System.Windows.Forms.Button bPCBAConfig;
        private System.Windows.Forms.Button bHLAConfig;
        private System.Windows.Forms.Label tControlVoltage2;
        private System.Windows.Forms.ToolTip DebugWindowtoolTip1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lbTagRadioMac;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button bGetVersions;
        private System.Windows.Forms.Button bMACConfig;
        private System.Windows.Forms.Button bReadManData;
        private System.Windows.Forms.Button bClearManData;
        private System.Windows.Forms.Button bSetRadioParams;
        private System.Windows.Forms.Button bSetTestRadioParams;
        private System.Windows.Forms.Button bReadDUTRadioParams;
        private System.Windows.Forms.Button bReadTestRadioParams;
        private System.Windows.Forms.Button bReftoDUT;
        private System.Windows.Forms.Button bDUTtoRef;
        private System.Windows.Forms.CheckBox cbDisplayData;
        private System.Windows.Forms.Button bClearInputBuffers;
        private System.Windows.Forms.CheckBox cbSetRefRadio;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown nudPowerLevel;
        private System.Windows.Forms.Button bHwConfig;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rbBlue;
        private System.Windows.Forms.RadioButton rbGreen;
        private System.Windows.Forms.RadioButton rbRed;
        private System.Windows.Forms.RadioButton rbNone;
        private System.Windows.Forms.Button bSetDim1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.NumericUpDown nudDim1Voltage;
        private System.Windows.Forms.NumericUpDown nudDim2Voltage;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button bReadSensors;
        private System.Windows.Forms.TextBox tbAmbient;
        private System.Windows.Forms.TextBox tbTemp;
        private System.Windows.Forms.TextBox tbPIR;
        private System.Windows.Forms.Label tbTempx;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button bPOST;
        private System.Windows.Forms.Button bReboot;
        private System.Windows.Forms.NumericUpDown nudRebootWaitMS;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button bCalTemp;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button bProgramCode;
        private System.Windows.Forms.Button bClearWindow;
        private System.Windows.Forms.Button bStartbackground;
        private System.Windows.Forms.NumericUpDown nudMaxCOResponseSec;
        private System.Windows.Forms.NumericUpDown nudMaxCUQueryResponseSec;
        private System.Windows.Forms.Label lbMaxCOResponseSec;
        private System.Windows.Forms.Label lbMaxCUQuerySec;
    }
}