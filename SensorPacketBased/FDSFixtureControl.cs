﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;

using TestStationErrorCodes;
using LimitsDatabase;
using SensorProdTests;

namespace SensorManufSY2
{
    //
    //  Fixture control. 
    //      Packet based DUT
    //      Kalapana Test radio
    //
    public partial class FDSFixtureControl : Form
    {
        FixtureFunctions fixture;
        StationConfigure stationConfig;

        public delegate void SetTextCallback(string text);
        public delegate void SetTextTypeCallback(string text, StatusType type);
        string msg = string.Empty;

        public delegate void updateFixtureReadings(string lid, string pushButton, string temp);
        public delegate void updateDUTReadings(string svoltage, string cvoltage, string cvoltage2);
        public delegate void updateLEDReadings(int intensity, int freq, Color backColor);
        public delegate void updateText(string msg);

        Thread tPIROscTest;

        Thread tDUTCommand;

        ThreadStart updateScreenThreadStart;
        Thread updateScreenThread;
        ThreadStart updateLEDScreenThreadStart;
        Thread updateLEDScreenThread;
        bool runThreads = true;

        MainWindow mainWindow;


        public void sc_StatusUpdated(object sender, StatusUpdatedEventArgs e)
        {
            this.addTextToStatusWindow(e.statusText, e.statusType);
        }

        private void addTextToStatusWindow(string msg)
        {
            if (this.rtStatusWindow.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(addTextToStatusWindow);
                this.Invoke(d, new object[] { msg});
            }
            else
            {
                this.rtStatusWindow.AppendText(msg);
                this.rtStatusWindow.ScrollToCaret();
                this.rtStatusWindow.Update();
            }
        }
        /// <summary>
        /// Adds text to the status screen;
        /// </summary>
        /// <param name="msg"></param>
        public void addTextToStatusWindow(string msg, StatusType type)
        {
            if (this.rtStatusWindow.InvokeRequired)
            {
                SetTextTypeCallback d = new SetTextTypeCallback(addTextToStatusWindow);
                this.Invoke(d, new object[] { msg, type });
            }
            else
            {
                switch (type)
                {
                    case StatusType.text:
                        this.rtStatusWindow.SelectionColor = Color.Black;
                        break;
                    case StatusType.passed:
                        this.rtStatusWindow.SelectionColor = Color.Green;
                        break;
                    case StatusType.failed:
                        this.rtStatusWindow.SelectionColor = Color.Red;
                        break;
                    case StatusType.serialSend:
                        this.rtStatusWindow.SelectionColor = Color.Blue;
                        break;
                    case StatusType.serialRcv:
                        this.rtStatusWindow.SelectionColor = Color.Gray;
                        break;
                    case StatusType.prompt:
                        this.rtStatusWindow.SelectionColor = Color.Purple;
                        break;
                    case StatusType.cuSend:
                        this.rtStatusWindow.SelectionColor = Color.Aqua;
                        break;
                    case StatusType.cuRcv:
                        this.rtStatusWindow.SelectionColor = Color.RosyBrown;
                        break;
                    case StatusType.snifferSend:
                        this.rtStatusWindow.SelectionColor = Color.Aqua;
                        break;
                    case StatusType.snifferRcv:
                        this.rtStatusWindow.SelectionColor = Color.RosyBrown;
                        break;
                    default:
                        this.rtStatusWindow.SelectionColor = Color.Black;
                        break;
                }
                this.rtStatusWindow.AppendText(msg);
                this.rtStatusWindow.ScrollToCaret();
                this.rtStatusWindow.Update();
            }
        }

        public FDSFixtureControl(MainWindow mainWindowobj)
        {
            string outmsg = string.Empty;


            InitializeComponent();

            mainWindow = mainWindowobj;
            stationConfig = mainWindowobj.stationConfig;
            fixture = new FixtureFunctions(stationConfig);

            if (!fixture.InitFixture())
            {
                MessageBox.Show("Error setting up the fixture: " + fixture.LastFunctionErrorMessage);
                return;
            }

            fixture.StatusUpdated += sc_StatusUpdated;
            try
            {
                if (stationConfig.GetChannelData(FixtureDefintions.CH_REF_RADIO) != null)
                {
                    fixture.PacketRefRadio.StatusUpdated += sc_StatusUpdated;
                    tbTestRadioMAC.Text = fixture.RefRadioMAC;
                    lbTestChannel.Text = stationConfig.ConfigTestRadioChannel;
                    lbTagRadioMac.Text = fixture.RefBLEMAC;
                }

                updateScreenThreadStart = new ThreadStart(StartUpdateScreen);
                updateScreenThread = new Thread(updateScreenThreadStart);
                updateScreenThread.Start();

                if (stationConfig.GetChannelData(FixtureDefintions.CH_LED_DETECTOR) != null)
                {
                    updateLEDScreenThreadStart = new ThreadStart(StartLEDUpdateScreen);
                    updateLEDScreenThread = new Thread(updateLEDScreenThreadStart);
                    updateLEDScreenThread.Start();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error initializing fixture. " + ex.Message);
            }
        }


        private void checkBoxTestLamp_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxTestLamp.Checked)
                fixture.ControlPIRSource(FixtureDefintions.PowerControl.ON);
            else
                fixture.ControlPIRSource(FixtureDefintions.PowerControl.OFF);
        }
        /// <summary>
        /// DUT Power control check box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxTestIndicator_CheckedChanged(object sender, EventArgs e)
        {
            string outmsg = string.Empty;

            if (checkBoxTestIndicator.Checked)
                fixture.ControlDUTPower(FixtureDefintions.PowerControl.ON, 100);
            else
                fixture.ControlDUTPower(FixtureDefintions.PowerControl.OFF, 100);
        }

        private void numericUpDownLEDVoltage_ValueChanged(object sender, EventArgs e)
        {
            fixture.SetAmbientSensorLED(Convert.ToDouble(numericUpDownLEDVoltage.Value));
        }


        private void FDSFixtureControl_FormClosing(object sender, FormClosingEventArgs e)
        {
            runThreads = false;
            if (stationConfig.GetChannelData(FixtureDefintions.CH_PIR_SOURCE_CNTL) != null)
                fixture.ControlPIRSource(0);
            fixture.ControlDUTPower(FixtureDefintions.PowerControl.OFF, 100);
            if (updateScreenThread != null)
            {
                while (updateScreenThread.IsAlive) 
                {
                    Thread.Sleep(0);
                }
                updateScreenThread.Join();
            }
            if (updateLEDScreenThread != null)
            {
                while (updateLEDScreenThread.IsAlive)
                {
                    Thread.Sleep(0);
                }
                updateLEDScreenThread.Join();
            }
            fixture.Dispose();
        }

        /// <summary>
        /// DUT to ref test
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bDUTtoRef_Click(object sender, EventArgs e)
        {
            if (tDUTCommand != null)
            {
                if (tDUTCommand.IsAlive)
                {
                    rtStatusWindow.AppendText("Already scanning\n");
                    return;
                }
            }
            tDUTCommand = new Thread(() => lclbDUTtoRef_Click());
            tDUTCommand.Start();
        }

        private void lclbDUTtoRef_Click()
        {
            string mac = string.Empty;
            string mac2 = string.Empty;
            string mac3 = string.Empty;

            updateText UpdateTextnDel = new updateText(lclUpdateTestResult);

            uint numberOfFoundBeacons = 0;
            rtStatusWindow.BeginInvoke(UpdateTextnDel, "Scan started...");

            BluetoothFunctions ble = new BluetoothFunctions(stationConfig, fixture);
            ble.fixture = fixture;
            fixture.PacketDutConsole.GetMACData(ref mac, ref mac2);
            ble.BluetoothTest(BluetoothFunctions.BLEDirection.DUTtoRef, mac2, (uint)nBTtimeout.Value, ref numberOfFoundBeacons);
            rtStatusWindow.BeginInvoke(UpdateTextnDel, "Beacons found = " + numberOfFoundBeacons.ToString() + "\n");
        }

        private void bReftoDUT_Click(object sender, EventArgs e)
        {
            if (tDUTCommand != null)
            {
                if (tDUTCommand.IsAlive)
                {
                    rtStatusWindow.AppendText("Already scanning\n");
                    return;
                }
            }
            tDUTCommand = new Thread(() => lclbReftoDUT_Click());
            tDUTCommand.Start();

        }

        private void lclbReftoDUT_Click()
        {
            string mac = string.Empty;
            string mac2 = string.Empty;
            updateText UpdateTextnDel = new updateText(lclUpdateTestResult);

            uint numberOfFoundBeacons = 0;
            rtStatusWindow.BeginInvoke(UpdateTextnDel, "Scan started....");

            BluetoothFunctions ble = new BluetoothFunctions(stationConfig, fixture);
            ble.fixture = fixture;
            fixture.PacketDutConsole.GetMACData(ref mac, ref mac2);
            ble.BluetoothTest(BluetoothFunctions.BLEDirection.ReftoDUT, mac2, (uint)nBTtimeout.Value, ref numberOfFoundBeacons);
            rtStatusWindow.BeginInvoke(UpdateTextnDel, "Beacons found = " + numberOfFoundBeacons.ToString() + "\n");
        }


        /// <summary>
        /// ref to DUT test
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bDUTScanBluetooth_Click(object sender, EventArgs e)
        {
            //List<string> devices;
            //rtScanStatus.Clear();
            //devices = new List<string>();
            //string outmsg = string.Empty;

            //if (tBLEDUTScan != null)
            //{
            //    if (tBLEDUTScan.IsAlive)
            //    {
            //        rtScanStatus.AppendText("Already scanning\n");
            //        return;
            //    }
            //}

            //rtScanStatus.AppendText("Scan started");

            //BluetoothFunctions blescan = new BluetoothFunctions(stationConfig.ConfigBLEComPort, stationConfig.ConfigTagComPort, senConsole);
            //blescan.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(sc_StatusUpdated);

            //tBLEScan = new Thread(() => blescan.DutFindBluetoothDevices(Convert.ToInt32(nBTtimeout.Value), cbResetBLEatScan.Checked ?  5:0));
            //tBLEScan.Start();
            //rtScanStatus.AppendText("..\n");
        }



        private void bStartAdvertise_Click(object sender, EventArgs e)
        {
            //if (tBLEAdvert != null)
            //{
            //    if (tBLEAdvert.IsAlive)
            //    {
            //        rtScanStatus.AppendText("Already changing over\n");
            //        return;
            //    }
            //}
            //BluetoothFunctions btadvert = new BluetoothFunctions(stationConfig.ConfigBLEComPort, stationConfig.ConfigTagComPort, senConsole);
            //btadvert.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(sc_StatusUpdated);

            //tBLEAdvert = new Thread(() => btadvert.DoBLEAdvertise());
            //tBLEAdvert.Start();

        }


        private void StartUpdateScreen()
        {

            updateFixtureReadings UpdateFixtureScreenDel = new updateFixtureReadings(UpdateFixtureReadings);
            updateDUTReadings UpdateDUTReadingsDel = new updateDUTReadings(UpdateDUTReadings);

            while (runThreads)
	        {
                this.textLidStatus.BeginInvoke(UpdateFixtureScreenDel, fixture.IsCoverClosed() ? "CLOSED" : "OPEN",
                                                                fixture.IsBoxButtonPushed() ? "Pushed" : "Not Pushed",
                                                                fixture.ReadTemperatureProbe().ToString("N1"));
                this.textLidStatus.BeginInvoke(UpdateDUTReadingsDel, fixture.ReadOneMeasurement(FixtureDefintions.CH_V3P3_VOLTAGE).ToString("N1"),
                                                    fixture.ReadOneMeasurement(FixtureDefintions.CH_DIM1_OUTPUT).ToString("N1"),
                                                    fixture.ReadOneMeasurement(FixtureDefintions.CH_DIM2_OUTPUT).ToString("N1"));
                Thread.Sleep(500);
            }
        }

        private void StartLEDUpdateScreen()
        {
            int intensity = 0;
            int freq = 0;
            string msg = string.Empty;
            Color backColor;

            updateLEDReadings UpdateLEDScreenDel = new updateLEDReadings(UpdateLEDReadings);
            while (runThreads)
            {
                if (fixture.IsLEDColorInrange("Red", stationConfig.ConfigLedDetector.ColorRED, ref freq, ref intensity, ref msg))
                {
                    backColor = Color.Red;
                }
                else if (fixture.IsLEDColorInrange("Green", stationConfig.ConfigLedDetector.ColorGREEN, ref freq, ref intensity, ref msg))
                {
                    backColor = Color.Green;
                }
                else if (fixture.IsLEDColorInrange("Blue", stationConfig.ConfigLedDetector.ColorBLUE, ref freq, ref intensity, ref msg))
                {
                    backColor = Color.Blue;
                }
                else
                {
                    backColor = Color.Gray;
                }
                this.textLEDFreq.BeginInvoke(UpdateLEDScreenDel, intensity, freq, backColor);
                Thread.Sleep(500);
            }

        }

        private void UpdateFixtureReadings(string lid, string pushButton, string temp)
        {
            textLidStatus.Text = lid;
            textPushButtonStatus.Text = pushButton;
            TempProbeReading.Text = temp;
        }

        private void UpdateDUTReadings(string svoltage, string cvoltage, string cvoltage2)
        {
            DUT3p3vReading.Text = svoltage;
            tControlVoltage.Text = cvoltage;
            tControlVoltage2.Text = cvoltage2;
        }

        private void UpdateLEDReadings(int intensity, int freq, Color backColor)
        {
            LEDColor.BackColor = backColor;
            textLEDFreq.Text = freq.ToString("N0");
            textLEDIntest.Text = intensity.ToString("N0");
        }



        private void richTextBoxCUWindow_TextChanged(object sender, EventArgs e)
        {

        }

        private void bTestRaio_Click(object sender, EventArgs e)
        {
            if (tDUTCommand != null)
            {
                if (tDUTCommand.IsAlive)
                {
                    rtStatusWindow.AppendText("Already doing radio test\n");
                    return;
                }
            }
            tDUTCommand = new Thread(() => lclTestNetworks());
            tDUTCommand.Start();
        }

        private void lclTestNetworks ()
        {
            string resultmessage = string.Empty;
            string cuMessage = string.Empty;
            int stepStatus = 0;
            uint oldLevel = fixture.RadioDefaultTxPower;

            updateText UpdateTextnDel = new updateText(lclUpdateTestResult);

            MainWindow.uutdata uutData = new MainWindow.uutdata();            // structure to save the data to configure the DUT with

            SensorDVTMANTests sensorDVTMANTests = new SensorDVTMANTests();
            sensorDVTMANTests.fixture = fixture;
            sensorDVTMANTests.stationConfig = stationConfig;

            // estToRun(string name, int testnumber, string function, string dllFile, string dllClass, string testType, string limittype)
            LimitsData.stepsItem testparams = new LimitsData.stepsItem();     // parameters for a single test
            testparams.limits = new List<LimitsData.limitsItem>();
            testparams.parameters = new List<LimitsData.parametersItem>();
            testparams.limits.Add(new LimitsData.limitsItem() { name = "RADIOLQIM", type = "VALUE", operation = "GE", value1 = nudLQIM.Value.ToString(), value2 = "" });
            testparams.limits.Add(new LimitsData.limitsItem() { name = "RADIOLQIT", type = "VALUE", operation = "GE", value1 = nudLQIT.Value.ToString(), value2 = "" });

            
            uutData.testChannel = Convert.ToInt32(stationConfig.ConfigTestRadioChannel);
            fixture.RadioDefaultTxPower = (uint)nudPowerLevel.Value;
            stepStatus = sensorDVTMANTests.TestNetworks(testparams, null, uutData, ref resultmessage);
            fixture.RadioDefaultTxPower = oldLevel;
            if (stepStatus == 0)
                resultmessage = resultmessage + "\n";
            else
                resultmessage = "Test Failed: " + resultmessage + "\n";
            rtStatusWindow.BeginInvoke(UpdateTextnDel, resultmessage);
        }

        private void lclUpdateTestResult(string msg)
        {
            this.rtStatusWindow.AppendText(msg);
        }

        private void bPIROscTest_Click(object sender, EventArgs e)
        {
            if (tPIROscTest  != null)
            {
                if (tPIROscTest.IsAlive)
                {
                    rtStatusWindow.AppendText("Already doing PIR Osc Test test\n");
                    return;
                }
            }
            tPIROscTest = new Thread(() => lclPIROscTest());
            tPIROscTest.Start();

        }

        delegate void delUpdateMinMax(int min, int max);
        private void lclPIROscTest()
        {
            //MainWindow.codeClasses codeClasses = new MainWindow.codeClasses();
            updateDUTReadings delUpdateMinMax = new updateDUTReadings(lclUpdatePIRMinMax);
            updateText delUpdateText = new updateText(lclUpdateOscStatus);
            int status;

            //codeClasses.fixtureFunctions = fixture;

            PIRTests pirTests = new PIRTests(fixture);

            if ((status = pirTests.IsPIROscillating(5)) == 1)
                lbOscStatus.BeginInvoke(delUpdateText, "Oscillating");
            else if (status == -1)
                lbOscStatus.BeginInvoke(delUpdateText, pirTests.ErrorMsg);
            else
                lbOscStatus.BeginInvoke(delUpdateText, "Not Oscillating");

            if (pirTests.pirData.Count != 0)
            {
                lbPIRMin.BeginInvoke(delUpdateMinMax, pirTests.pirData.Min().ToString(), pirTests.pirData.Max().ToString(), "");
            }
            else
            {
                lbPIRMin.BeginInvoke(delUpdateMinMax, "no data", "no data", "");
            }

        }
        private void lclUpdateOscStatus(string msg)
        {
            lbOscStatus.Text = msg;
        }

        private void lclUpdatePIRMinMax(string min, string max, string nothing)
        {
            lbPIRMin.Text = min;
            lbPIRMax.Text = max;
        }

        private void bPCBAConfig_Click(object sender, EventArgs e)
        {
            if (tDUTCommand != null)
            {
                if (tDUTCommand.IsAlive)
                {
                    rtStatusWindow.AppendText("Already doing function\n");
                    return;
                }
            }
            tDUTCommand = new Thread(() => lclPCBConfig_Click());
            tDUTCommand.Start();
        }
        /// <summary>
        /// Will program PCBA info.
        /// </summary>
        private void lclPCBConfig_Click()
        { 
            string sn;
            string pn;

            GetPCBAInfo iForm = new GetPCBAInfo();

            if (iForm.ShowDialog() == DialogResult.OK)
            {
                sn = iForm.SN.ToUpper();
                pn = iForm.pcbaPN;
                iForm.Dispose();
            }
            else   // operator canceled
            {
                sn = string.Empty;
                iForm.Dispose();
                return;
            }

            if (cbSetRefRadio.Checked)
            {
                if (!fixture.PacketRefRadio.SetPCBAData(pn, sn))
                {
                    addTextToStatusWindow(fixture.LastFunctionErrorMessage);
                    return;
                }
                pn = string.Empty;
                sn = string.Empty;
                if (!fixture.PacketRefRadio.GetPCBAData(ref pn, ref sn))
                {
                    addTextToStatusWindow(fixture.LastFunctionErrorMessage);
                    return;
                }
                addTextToStatusWindow("Ref Radio Readback SN=" + sn + " PN=" + pn + "\n");

            }
            else
            {
                if (!fixture.PacketDutConsole.SetPCBAData(pn, sn))
                {
                    addTextToStatusWindow(fixture.LastFunctionErrorMessage);
                    return;
                }
                pn = string.Empty;
                sn = string.Empty;
                if (!fixture.PacketDutConsole.GetPCBAData(ref pn, ref sn))
                {
                    addTextToStatusWindow(fixture.LastFunctionErrorMessage);
                    return;
                }
                addTextToStatusWindow("Readback SN=" + sn + " PN=" + pn + "\n");
            }
        }


        private void bHLAConfig_Click(object sender, EventArgs e)
        {
            if (tDUTCommand != null)
            {
                if (tDUTCommand.IsAlive)
                {
                    rtStatusWindow.AppendText("Already doing function\n");
                    return;
                }
            }
            tDUTCommand = new Thread(() => lclbHLAConfig_Click());
            tDUTCommand.Start();
        }

        private void lclbHLAConfig_Click()
        {
            string sn;
            string pn;
            string model;

            GetHLAInfo iForm = new GetHLAInfo();

            if (iForm.ShowDialog() == DialogResult.OK)
            {
                sn = iForm.SN.ToUpper();
                pn = iForm.PN;
                model = iForm.Model;
                iForm.Dispose();
            }
            else   // operator canceled
            {
                sn = string.Empty;
                iForm.Dispose();
                return;
            }

            if (cbSetRefRadio.Checked)
            {
                fixture.PacketRefRadio.SetHLAData(pn, sn, model);
                pn = string.Empty;
                sn = string.Empty;
                model = string.Empty;
                fixture.PacketRefRadio.GetHLAData(ref pn, ref sn, ref model);
                addTextToStatusWindow("Ref Radio Readback SN=" + sn + " PN=" + pn + "Model=" + model + "\n");
            }
            else
            {
                fixture.PacketDutConsole.SetHLAData(pn, sn, model);
                pn = string.Empty;
                sn = string.Empty;
                model = string.Empty;
                fixture.PacketDutConsole.GetHLAData(ref pn, ref sn, ref model);
                addTextToStatusWindow("Readback SN=" + sn + " PN=" + pn + "Model=" + model + "\n");
            }

        }


        private void bStartTagAdvert_Click(object sender, EventArgs e)
        {
            //if (tBLETagAdvert != null)
            //{
            //    if (tBLETagAdvert.IsAlive)
            //    {
            //        rtScanStatus.AppendText("Already running over\n");
            //        return;
            //    }
            //}
            //BluetoothFunctions btadvert = new BluetoothFunctions(stationConfig.ConfigBLEComPort, stationConfig.ConfigTagComPort, senConsole);
            //btadvert.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(sc_StatusUpdated);

            //tBLETagAdvert = new Thread(() => btadvert.DoInitTag());
            //tBLETagAdvert.Start();

        }

        private void bGetVersions_Click(object sender, EventArgs e)
        {
            if (tDUTCommand != null)
            {
                if (tDUTCommand.IsAlive)
                {
                    rtStatusWindow.AppendText("Already doing function\n");
                    return;
                }
            }
            tDUTCommand = new Thread(() => lclbGetVersions_Click());
            tDUTCommand.Start();
        }

        private void lclbGetVersions_Click()
        {
            string imageID = string.Empty;
            int msgVersion = 0;

            if (cbSetRefRadio.Checked)
            {
                fixture.PacketRefRadio.ClearBuffers();
                fixture.PacketRefRadio.GetImageID(ref imageID);
                addTextToStatusWindow("Ref Radio Readback ID=" + imageID + "\n");
                fixture.PacketRefRadio.GetMsgVersion(ref msgVersion);
                addTextToStatusWindow("Ref Radio Msg Verions=" + msgVersion.ToString() + "\n");
            }
            else
            {
                fixture.PacketDutConsole.ClearBuffers();
                fixture.PacketDutConsole.GetImageID(ref imageID);
                addTextToStatusWindow("Readback ID=" + imageID + "\n");
                fixture.PacketDutConsole.GetMsgVersion(ref msgVersion);
                addTextToStatusWindow("Msg Verions=" + msgVersion.ToString() + "\n");
            }

        }

        private void bMACConfig_Click(object sender, EventArgs e)
        {
            if (tDUTCommand != null)
            {
                if (tDUTCommand.IsAlive)
                {
                    rtStatusWindow.AppendText("Already doing function\n");
                    return;
                }
            }
            tDUTCommand = new Thread(() => lclbMACConfig_Click());
            tDUTCommand.Start();

        }

        private void lclbMACConfig_Click()
        {
            string mac1 = string.Empty;
            string mac2 = string.Empty;

            GetMAC iForm = new GetMAC();

            if (iForm.ShowDialog() == DialogResult.OK)
            {
                mac1 = iForm.MAC1;
                mac2 = iForm.MAC2;
                iForm.Dispose();
            }
            else   // operator canceled
            {
                mac1 = string.Empty;
                mac2 = string.Empty;
                iForm.Dispose();
                return;
            }

            if (cbSetRefRadio.Checked)      // if setting ref radio settings
            {
                fixture.PacketRefRadio.SetMACData(mac1, mac2);
                mac1 = string.Empty;
                mac2 = string.Empty;
                fixture.PacketRefRadio.GetMACData(ref mac1, ref mac2);
                addTextToStatusWindow("Ref Radio Readback MAC1=" + mac1 + " MAC2=" + mac2 + "\n");
            }
            else
            {
                fixture.PacketDutConsole.SetMACData(mac1, mac2);
                mac1 = string.Empty;
                mac2 = string.Empty;
                fixture.PacketDutConsole.GetMACData(ref mac1, ref mac2);
                addTextToStatusWindow("Readback MAC1=" + mac1 + " MAC2=" + mac2 + "\n");
            }


        }

        private void bReadManData_Click(object sender, EventArgs e)
        {
            if (tDUTCommand != null)
            {
                if (tDUTCommand.IsAlive)
                {
                    rtStatusWindow.AppendText("Already doing function\n");
                    return;
                }
            }
            tDUTCommand = new Thread(() => lclbReadManData_Click());
            tDUTCommand.Start();

        }

        private void lclbReadManData_Click()
        {
            string mac1 = string.Empty;
            string mac2 = string.Empty;
            string pn = string.Empty;
            string sn = string.Empty;
            string model = string.Empty;
            byte[] ahwdata = new byte[5];

            if (cbSetRefRadio.Checked)
            {
                if (!fixture.PacketRefRadio.GetMACData(ref mac1, ref mac2))
                {
                    addTextToStatusWindow(fixture.PacketRefRadio.LastErrorMessage);
                    return;
                }
                addTextToStatusWindow("Ref Radio Readback MAC1=" + mac1 + " MAC2=" + mac2 + "\n");
                if (!fixture.PacketRefRadio.GetHLAData(ref pn, ref sn, ref model))
                {
                    addTextToStatusWindow(fixture.PacketRefRadio.LastErrorMessage);
                    return;
                }
                addTextToStatusWindow("Ref Radio Readback HLA SN=" + sn + " PN=" + pn + "Model=" + model + "\n");
                if (!fixture.PacketRefRadio.GetPCBAData(ref pn, ref sn))
                {
                    addTextToStatusWindow(fixture.PacketRefRadio.LastErrorMessage);
                    return;
                }
                addTextToStatusWindow("Ref radio Readback PCBA SN=" + sn + " PN=" + pn + "\n");
                if (!fixture.PacketRefRadio.GetHwConfigData(ref ahwdata))
                {
                    addTextToStatusWindow(fixture.PacketRefRadio.LastErrorMessage);
                    return;
                }
                addTextToStatusWindow("Ref radio Readback HW Config=" + ahwdata[0].ToString() + "\n");
            }
            else
            {
                if (!fixture.PacketDutConsole.GetMACData(ref mac1, ref mac2))
                {
                    addTextToStatusWindow(fixture.PacketDutConsole.LastErrorMessage);
                    return;
                }
                addTextToStatusWindow("Enlighted Radio Readback MAC1=" + mac1 + " MAC2=" + mac2 + "\n");
                if (!fixture.PacketDutConsole.GetHLAData(ref pn, ref sn, ref model))
                {
                    addTextToStatusWindow(fixture.PacketDutConsole.LastErrorMessage);
                    return;
                }
                addTextToStatusWindow("Enlighted Radio Readback HLA SN=" + sn + " PN=" + pn + "Model=" + model + "\n");
                if (!fixture.PacketDutConsole.GetPCBAData(ref pn, ref sn))
                {
                    addTextToStatusWindow(fixture.PacketDutConsole.LastErrorMessage);
                    return;
                }
                addTextToStatusWindow("Enlighted radio Readback PCBA SN=" + sn + " PN=" + pn + "\n");
                if (!fixture.PacketDutConsole.GetHwConfigData(ref ahwdata))
                {
                    addTextToStatusWindow(fixture.PacketDutConsole.LastErrorMessage);
                    return;
                }
                addTextToStatusWindow("Enlighted radio Readback HW Config=" + ahwdata[0].ToString() + "\n");
            }

        }

        private void bClearManData_Click(object sender, EventArgs e)
        {
            if (tDUTCommand != null)
            {
                if (tDUTCommand.IsAlive)
                {
                    rtStatusWindow.AppendText("Already doing function\n");
                    return;
                }
            }
            tDUTCommand = new Thread(() => lclbClearManData_Click());
            tDUTCommand.Start();
        }

        private void lclbClearManData_Click()
        {
            if (cbSetRefRadio.Checked)
                fixture.PacketRefRadio.ClearManData();
            else
                fixture.PacketDutConsole.ClearManData();
        }

        private void bSetRadioParams_Click(object sender, EventArgs e)
        {
            if (tDUTCommand != null)
            {
                if (tDUTCommand.IsAlive)
                {
                    rtStatusWindow.AppendText("Already doing function\n");
                    return;
                }
            }
            tDUTCommand = new Thread(() => lclbSetRadioParams_Click());
            tDUTCommand.Start();

        }

        private void lclbSetRadioParams_Click()
        {
            uint channel;
            uint panID;
            uint txPower;
            string encryptKey;
            uint ttl;
            uint rate;

            GetRadioParams iForm = new GetRadioParams();

            if (iForm.ShowDialog() == DialogResult.OK)
            {
                channel = iForm.Channel;
                panID = iForm.PanID;
                txPower = iForm.TxPower;
                encryptKey = iForm.EncryptKey;
                ttl = iForm.Ttl;
                rate = iForm.Rate;
                iForm.Dispose();
            }
            else
                return;

            fixture.PacketDutConsole.SetRadioData(channel, panID, txPower, Encoding.ASCII.GetBytes(encryptKey), ttl, rate);
            addTextToStatusWindow("Data sent\n");
        }

        private void bReadDUTRadioParams_Click(object sender, EventArgs e)
        {
            if (tDUTCommand != null)
            {
                if (tDUTCommand.IsAlive)
                {
                    rtStatusWindow.AppendText("Already doing function\n");
                    return;
                }
            }
            tDUTCommand = new Thread(() => lclbReadDUTRadioParams_Click());
            tDUTCommand.Start();
        }

        private void lclbReadDUTRadioParams_Click()
        {
            uint channel = 0;
            uint panID = 0;
            uint txPower = 0;
            byte[] encryptKey = new byte[16];
            uint ttl = 0;
            uint rate = 0;
            string outmsg;

            fixture.PacketDutConsole.GetRadioData(ref channel, ref panID, ref txPower, ref encryptKey, ref ttl, ref rate);
            outmsg = string.Format("channel: {0} panID: {1} txpower: {2} ttl: {3} rate: {4}\nkey: {5}\n", channel, panID, txPower, ttl, rate, Encoding.ASCII.GetString(encryptKey));
            addTextToStatusWindow(outmsg);
        }

        private void bReadTestRadioParams_Click(object sender, EventArgs e)
        {
            if (tDUTCommand != null)
            {
                if (tDUTCommand.IsAlive)
                {
                    rtStatusWindow.AppendText("Already doing function\n");
                    return;
                }
            }
            tDUTCommand = new Thread(() => lclbReadTestRadioParams_Click());
            tDUTCommand.Start();
        }

        private void lclbReadTestRadioParams_Click()
        {
            uint channel = 0;
            uint panID = 0;
            uint txPower = 0;
            byte[] encryptKey = new byte[16];
            uint ttl = 0;
            uint rate = 0;
            string outmsg;

            if (fixture.PacketRefRadio.GetRadioData(ref channel, ref panID, ref txPower, ref encryptKey, ref ttl, ref rate))
                outmsg = string.Format("channel: {0} panID: {1} txpower: {2} ttl: {3} rate: {4}\nkey: {5}\n", channel, panID, txPower, ttl, rate, Encoding.ASCII.GetString(encryptKey));
            else
                outmsg = string.Format("{0} : {1}", fixture.PacketRefRadio.LastErrorCode, fixture.PacketRefRadio.LastErrorMessage);
            addTextToStatusWindow(outmsg);
        }

        private void bSetRefRadioParams_Click(object sender, EventArgs e)
        {
            if (tDUTCommand != null)
            {
                if (tDUTCommand.IsAlive)
                {
                    rtStatusWindow.AppendText("Already doing function\n");
                    return;
                }
            }
            tDUTCommand = new Thread(() => lclbSetRefRadioParams_Click());
            tDUTCommand.Start();
            
        }

        private void lclbSetRefRadioParams_Click()
        {
            uint channel;
            uint panID;
            uint txPower;
            string encryptKey;
            uint ttl;
            uint rate;

            GetRadioParams iForm = new GetRadioParams();

            if (iForm.ShowDialog() == DialogResult.OK)
            {
                channel = iForm.Channel;
                panID = iForm.PanID;
                txPower = iForm.TxPower;
                encryptKey = iForm.EncryptKey;
                ttl = iForm.Ttl;
                rate = iForm.Rate;
                iForm.Dispose();
            }
            else
                return;

            fixture.PacketRefRadio.SetRadioData(channel, panID, txPower, Encoding.ASCII.GetBytes(encryptKey), ttl, rate);
            addTextToStatusWindow("Data sent\n");
        }

        private void lclbDUTtoRef_CLick()
        {

        }

        private void cbDisplayData_CheckedChanged(object sender, EventArgs e)
        {
            if (cbDisplayData.Checked)
            {
                fixture.PacketDutConsole.DisplayRcvChar = true;
                fixture.PacketDutConsole.DisplaySendChar = true;
                if (stationConfig.GetChannelData(FixtureDefintions.CH_REF_RADIO) != null)
                {
                    fixture.PacketRefRadio.DisplayRcvChar = true;
                    fixture.PacketRefRadio.DisplaySendChar = true;
                }
            }
            else
            {
                fixture.PacketDutConsole.DisplayRcvChar = false;
                fixture.PacketDutConsole.DisplaySendChar = false;
                if (stationConfig.GetChannelData(FixtureDefintions.CH_REF_RADIO) != null)
                {
                    fixture.PacketRefRadio.DisplayRcvChar = false;
                    fixture.PacketRefRadio.DisplaySendChar = false;
                }
            }
        }

        private void bClearInputBuffers_Click(object sender, EventArgs e)
        {
            if (fixture.PacketDutConsole != null)
                fixture.PacketDutConsole.ClearBuffers();
            if (fixture.PacketRefRadio != null)
                fixture.PacketRefRadio.ClearBuffers();
        }

        private void bHwConfig_Click(object sender, EventArgs e)
        {
            if (tDUTCommand != null)
            {
                if (tDUTCommand.IsAlive)
                {
                    rtStatusWindow.AppendText("Already doing function\n");
                    return;
                }
            }
            tDUTCommand = new Thread(() => lclbSetHwConfigParams_Click());
            tDUTCommand.Start();
        }

        private void lclbSetHwConfigParams_Click()
        {
            string hwdata;
            byte ahwdata;

            GetHWInfo iForm = new GetHWInfo();
            if (iForm.ShowDialog() == DialogResult.OK)
            {
                hwdata = iForm.Data;
            }
            else
                return;
            ahwdata = Convert.ToByte(hwdata);
            if (cbSetRefRadio.Checked)
                fixture.PacketRefRadio.SetHwConfig(ahwdata);
            else
                fixture.PacketDutConsole.SetHwConfig(ahwdata);
        }

        private void cbRed_CheckedChanged(object sender, EventArgs e)
        {
            bool red;
            bool green;
            bool blue;

            red = rbRed.Checked;
            green = rbGreen.Checked;
            blue = rbBlue.Checked;

            fixture.PacketDutConsole.SetLeds(red, green, blue);
        }

        private void bSetDim1_Click(object sender, EventArgs e)
        {
            fixture.PacketDutConsole.SetDimVoltage((uint)nudDim1Voltage.Value * 10, (uint)nudDim2Voltage.Value * 10);
        }

        private void bReadSensors_Click(object sender, EventArgs e)
        {
            UInt16 pir = 0;
            UInt32 ambient = 0;
            UInt16 temp = 0;

            fixture.PacketDutConsole.GetSensorData(ref pir, ref ambient, ref temp);
            tbPIR.Text = pir.ToString();
            tbAmbient.Text = ambient.ToString();
            tbTemp.Text = temp.ToString();
        }

        private void bPOST_Click(object sender, EventArgs e)
        {
            UInt32 savedCRC = 0;
            UInt32 calCRC = 0;
            int status = 0;

            fixture.PacketDutConsole.PerformPost(ref status, ref savedCRC, ref calCRC);
            addTextToStatusWindow("status:" + status.ToString() + " saved CRC:" + savedCRC.ToString("X") + " calc CRC:" + calCRC.ToString("X") + "\n");
        }

        private void bReboot_Click(object sender, EventArgs e)
        {
            DateTime starttime = DateTime.Now;
            int readversion = 0;
            if (cbSetRefRadio.Checked)
            {
                fixture.PacketRefRadio.PerformReboot();
                if (fixture.PacketRefRadio.WaitForMsgVersion(ref readversion, (int)nudRebootWaitMS.Value))
                    addTextToStatusWindow("Reboot Ref Radio performed. Time=" + (DateTime.Now - starttime) + "\n");
                else
                    addTextToStatusWindow("Reboot Ref Radio performed with error. Time=" + (DateTime.Now - starttime) + "\n");
            }
            else
            {
                fixture.PacketDutConsole.PerformReboot();
                if (fixture.PacketDutConsole.WaitForMsgVersion(ref readversion, (int)nudRebootWaitMS.Value))
                    addTextToStatusWindow("Reboot performed. Time=" + (DateTime.Now - starttime) + "\n");
                else
                    addTextToStatusWindow("Reboot performed with error. Time=" + (DateTime.Now - starttime) + "\n");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool bstatus = true;
            int status = 0;

            var yn = MessageBox.Show("do you want to do the change over?", "Change Over", MessageBoxButtons.YesNo);
            if (yn == DialogResult.Yes)
            {
                addTextToStatusWindow("Changeover started...");
                bstatus = fixture.PacketDutConsole.PreformChangeOver(Convert.ToInt32(nudMaxCOResponseSec.Value) * 1000, ref status);
                addTextToStatusWindow("Changeover response found\n");
                if (!bstatus)
                {
                    addTextToStatusWindow("Error doing changeover command. " + fixture.PacketDutConsole.LastErrorMessage + "\n");
                }
                else
                {
                    addTextToStatusWindow("Looking for CU request..");
                    byte[] cuResponse = new byte[6];
                    // need to first reset the baud rate to 19200
                    fixture.PacketDutConsole.Close();
                    Thread.Sleep(100);      // let the port close
                    fixture.CUDutConsole.Open(fixture.PacketDutConsole.PortName);
                    Thread.Sleep(100);
                    fixture.CUDutConsole.DiscardAllBuffers();
                    // wait up to offDelay for the CU message
                    if (fixture.CUDutConsole.CUReadResponse(ref cuResponse, Convert.ToInt32(nudMaxCUQueryResponseSec.Value) * 1000) == 0)
                    {
                        addTextToStatusWindow("Error doing changeover command. \n");
                    }
                    else
                        addTextToStatusWindow(string.Format("Found changeover command. \n {0} {1} {2} {3} {4} {5}\n", cuResponse[0], cuResponse[1], cuResponse[2], cuResponse[3], cuResponse[4], cuResponse[5]));
                    fixture.CUDutConsole.Close();
                    Thread.Sleep(100);
                    fixture.PacketDutConsole.Open();

                }

            }
        }

        private void bCalTemp_Click(object sender, EventArgs e)
        {
            uint rawReading = 0;
            double probeTemp = 0;
            string msg = string.Empty;

            probeTemp = fixture.ReadTemperatureProbe();
            fixture.PacketDutConsole.PreformTempCal((uint)probeTemp, ref rawReading);
            addTextToStatusWindow("Probe temp: " + probeTemp.ToString("N1") + " Raw reading: " + rawReading.ToString() + "\n");
        }

        private void bProgAllManData_Click(object sender, EventArgs e)
        {
            if (tDUTCommand != null)
            {
                if (tDUTCommand.IsAlive)
                {
                    rtStatusWindow.AppendText("Already doing function\n");
                    return;
                }
            }
            tDUTCommand = new Thread(() => lclbProgAllManData_Click());
            tDUTCommand.Start();

        }

        private void lclbProgAllManData_Click()
        {
            string pcbasn;
            string pcbapn;
            string hlasn;
            string hlapn;
            string productcode;
            string hardwareconfig;
            byte[] ahardwareconfig = new byte[5];
            byte bhardwareconfig;
            string mac1;
            string mac2;

            fixture.PacketDutConsole.ClearBuffers();

            GetAllManData iForm = new GetAllManData();
            if (cbSetRefRadio.Checked)
                fixture.PacketRefRadio.ClearBuffers();

                if (iForm.ShowDialog() == DialogResult.OK)
            {
                pcbasn = iForm.PCBASN.ToUpper();
                pcbapn = iForm.PCBAPN;
                hlasn = iForm.HLASN;
                hlapn = iForm.HLAPN;
                productcode = iForm.ProductCode;
                mac1 = iForm.MAC1;
                mac2 = iForm.MAC2;
                hardwareconfig = iForm.HardwareCode;
                iForm.Dispose();
            }
            else   // operator canceled
            {
                iForm.Dispose();
                return;
            }

            if (cbSetRefRadio.Checked)
            {
                fixture.PacketRefRadio.SetHLAData(hlapn, hlasn, productcode);
                hlapn = string.Empty;
                hlasn = string.Empty;
                productcode  = string.Empty;
                if (!fixture.PacketRefRadio.GetHLAData(ref hlapn, ref hlasn, ref productcode))
                {
                    addTextToStatusWindow(fixture.LastFunctionErrorMessage);
                    return;
                }
                addTextToStatusWindow("Ref Radio Readback SN=" + hlasn + " PN=" + hlapn + "Model=" + productcode + "\n");
            }
            else
            {
                fixture.PacketDutConsole.SetHLAData(hlapn, hlasn, productcode);
                hlapn = string.Empty;
                hlasn = string.Empty;
                productcode = string.Empty;
                if (!fixture.PacketDutConsole.GetHLAData(ref hlapn, ref hlasn, ref productcode))
                {
                    addTextToStatusWindow(fixture.LastFunctionErrorMessage);
                    return;
                }
                addTextToStatusWindow("Radio Readback SN=" + hlasn + " PN=" + hlapn + "Model=" + productcode + "\n");
            }

            if (cbSetRefRadio.Checked)
            {
                if (!fixture.PacketRefRadio.SetPCBAData(pcbapn, pcbasn))
                {
                    addTextToStatusWindow(fixture.LastFunctionErrorMessage);
                    return;
                }
                pcbapn = string.Empty;
                pcbasn = string.Empty;
                if (!fixture.PacketRefRadio.GetPCBAData(ref pcbapn, ref pcbasn))
                {
                    addTextToStatusWindow(fixture.LastFunctionErrorMessage);
                    return;
                }
                addTextToStatusWindow("Ref Radio Readback SN=" + pcbasn + " PN=" + pcbapn + "\n");

            }
            else
            {
                if (!fixture.PacketDutConsole.SetPCBAData(pcbapn, pcbasn))
                {
                    addTextToStatusWindow(fixture.LastFunctionErrorMessage);
                    return;
                }
                pcbapn = string.Empty;
                pcbasn = string.Empty;
                if (!fixture.PacketDutConsole.GetPCBAData(ref pcbapn, ref pcbasn))
                {
                    addTextToStatusWindow(fixture.LastFunctionErrorMessage);
                    return;
                }
                addTextToStatusWindow("Readback SN=" + pcbasn + " PN=" + pcbapn + "\n");
            }

            if (cbSetRefRadio.Checked)      // if setting ref radio settings
            {
                if (!fixture.PacketRefRadio.SetMACData(mac1, mac2))
                {
                    addTextToStatusWindow(fixture.LastFunctionErrorMessage);
                    return;
                }
                mac1 = string.Empty;
                mac2 = string.Empty;
                if (!fixture.PacketRefRadio.GetMACData(ref mac1, ref mac2))
                {
                    addTextToStatusWindow(fixture.LastFunctionErrorMessage);
                    return;
                }
                addTextToStatusWindow("Ref Radio Readback MAC1=" + mac1 + " MAC2=" + mac2 + "\n");
            }
            else
            {
                if (!fixture.PacketDutConsole.SetMACData(mac1, mac2))
                {
                    addTextToStatusWindow(fixture.LastFunctionErrorMessage);
                    return;
                }
                mac1 = string.Empty;
                mac2 = string.Empty;
                if (!fixture.PacketDutConsole.GetMACData(ref mac1, ref mac2))
                {
                    addTextToStatusWindow(fixture.LastFunctionErrorMessage);
                    return;
                }
                addTextToStatusWindow("Readback MAC1=" + mac1 + " MAC2=" + mac2 + "\n");
            }

            bhardwareconfig = Convert.ToByte(hardwareconfig);
            if (cbSetRefRadio.Checked)
            {
                if (!fixture.PacketRefRadio.SetHwConfig(bhardwareconfig))
                {
                    addTextToStatusWindow(fixture.LastFunctionErrorMessage);
                    return;
                }
                ahardwareconfig[0] = 0;
                if (!fixture.PacketRefRadio.GetHwConfigData(ref ahardwareconfig))
                {
                    addTextToStatusWindow(fixture.LastFunctionErrorMessage);
                    return;
                }
            }
            else
            {
                if (!fixture.PacketDutConsole.SetHwConfig(bhardwareconfig))
                {
                    addTextToStatusWindow(fixture.LastFunctionErrorMessage);
                    return;
                }
                ahardwareconfig[0] = 0;
                if (!fixture.PacketDutConsole.GetHwConfigData(ref ahardwareconfig))
                {
                    addTextToStatusWindow(fixture.LastFunctionErrorMessage);
                    return;
                }
            }


        }

        private void bProgramCode_Click(object sender, EventArgs e)
        {
            if (tDUTCommand != null)
            {
                if (tDUTCommand.IsAlive)
                {
                    rtStatusWindow.AppendText("Already doing function\n");
                    return;
                }
            }
            tDUTCommand = new Thread(() => lclbProgramCode_Click());
            tDUTCommand.Start();

        }

        private void lclbProgramCode_Click()
        {
            string filetouse = string.Empty;
            int ecode = 0;
            string emsg = string.Empty;


            Flasher flasher = new Flasher(stationConfig.GetChannelInstrument(FixtureDefintions.CH_FLASHER).connection);
            if (!flasher.Open())
            {
                MessageBox.Show("Error opening Flasher port. " + flasher.LastErrorMsg);
                flasher.Close();
            }
            flasher.GetExistingFileToUse(ref filetouse, ref emsg, ref ecode);

            addTextToStatusWindow("Started programming.... ");
            flasher.ProgramDevice(filetouse, 15, 1);
            if (flasher.LastErrorStatus)
                addTextToStatusWindow(" DONE\n");
            else
                addTextToStatusWindow(" FAILED: " + flasher.LastErrorMsg + "\n");
            flasher.Close();
        }

        private void bClearWindow_Click(object sender, EventArgs e)
        {
            rtStatusWindow.Clear();
        }

        private void bStartbackground_Click(object sender, EventArgs e)
        {
            try
            {
                if (stationConfig.GetChannelData(FixtureDefintions.CH_REF_RADIO) != null)
                {
                    fixture.PacketRefRadio.StatusUpdated += sc_StatusUpdated;
                    tbTestRadioMAC.Text = fixture.RefRadioMAC;
                    lbTestChannel.Text = stationConfig.ConfigTestRadioChannel;
                    lbTagRadioMac.Text = fixture.RefBLEMAC;
                }

                updateScreenThreadStart = new ThreadStart(StartUpdateScreen);
                updateScreenThread = new Thread(updateScreenThreadStart);
                updateScreenThread.Start();

                if (stationConfig.GetChannelData(FixtureDefintions.CH_LED_DETECTOR) != null)
                {
                    updateLEDScreenThreadStart = new ThreadStart(StartLEDUpdateScreen);
                    updateLEDScreenThread = new Thread(updateLEDScreenThreadStart);
                    updateLEDScreenThread.Start();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error initializing fixture. " + ex.Message);
            }

        }
    }

}
