﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SensorManufSY2
{
    public class FixtureDefintions
    {
        //------------------------------------
        //  fixture defines
        //  Defines the functions that the fixture has for each type
        //---------------------------------------
        public const string CH_DIM1_OUTPUT = "DIM1_DC";
        public const string CH_DIM2_OUTPUT = "DIM2_DC";
        public const string CH_V3P3_VOLTAGE = "3V3_DC";
        public const string CH_TEMPERTURE_PROBE = "TEMPERTURE_PROBE_TEMP";
        public const string CH_OPERATOR_BUTTON = "OPERATOR_BUTTON";
        public const string CH_FIXTURE_LID_SWITCH = "FIXTURE_CLOSE_DI";
        public const string CH_AMBIENT_DETECTOR_LED = "AMBIENT_DECTOR_DAC";
        public const string CH_DUT_POWER_CNTL = "DUT_POWER_CNTL";
        public const string CH_PIR_SOURCE_CNTL = "PIR_SOURCE_CNTL";
        public const string CH_LED_DETECTOR = "LED_DETECTOR";
        public const string CH_DUT_POWER_CNTL1 = "DUT_POWER_CNTRL1";
        public const string CH_DUT_POWER_CNTL2 = "DUT_POWER_CNTRL2";
        public const string CH_DUT_POWER_CNTL3 = "DUT_POWER_CNTRL3";
        public const string CH_DUT_POWER_CNTL4 = "DUT_POWER_CNTRL4";
        public const string CH_FIXTURE_POWER = "FIXTURE_POWER";
        public const string CH_DUT_CONSOLE_CONNECT = "DUT_CONSOLE_CONNECT";
        public const string CH_DUT_TESTING_LED = "DUT_TESTING_LED";
        public const string CH_REF_RADIO = "REF_RADIO";
        public const string CH_DUT_CONSOLE = "DUT_CONSOLE";
        public const string CH_HLA_PRINTER = "HLA_PRINTER";
        public const string CH_PCBA_PRINTER = "PCBA_PRINTER";
        public const string CH_FLASHER = "FLASHER";
        public const string CH_DUT_INTERFACE = "DUT_INTERFACE";
        public const string CH_DUT_ENLIGHTED_RADIO = "DUT_ENLIGHTED_RADIO";
        public const string CH_DUT_BLE_RADIO = "DUT_BLE_RADIO";

        public const string FT_SU_5S_FCT = "SU-5S-FCT";
        public const string FT_SU_5E_FCT = "SU-5E-FCT";
        public const string FT_SU_5E_FNL = "SU-5E-FNL";
        public const string FT_SU_5S_FNL = "SU-5S-FNL";
        public const string FT_GW_2_1_FCT = "GW-2.1-FCT";
        public const string FT_LABELPRINT_LBP = "LabelPrint-LBP";

        public enum InstrumentID
        {
            NotDefined, LABJACK, A34970A, SwitchMate, LEDDetector,
            TestRadio, DUTConsole,
            PCBAPrinter, HLAPrinter, Flasher
        };

        // types
        //  Temperatu 
        public enum ChannelType
        {
            undefined,          // has not been defined
            DInput,             // digital input
            DOutput,            // digital ouput
            ACInput,            // AC input
            DCInput,            // DC input
            TemperatureK,       // Temperature input in K
            TemperatureC,       // Temperature input in C
            TemperatureF,       // Temperature in F
            Contact,            // relay
            Frequency,          // frequency input
            DAC,                // DAC output
            COBSPacketSerial,   // commuications is COBS packet based
            TextSerial,         // commuication is text based
            Printer,            // printer
            Enlighted2channel,  // enlighted 2 channel interface
            Dali,               // Dali interface
        };

        public enum PowerControl { OFF, ON };

        public class Instrument
        {
            public InstrumentID instrumentID;
            public List<Channel> channels;
            public string connection;               // how to connect. ie comm port name, visa name
            public string stationconfigTag;         // xml tag in the station file under the StationConfig/TestStations/'station to use'
        };

        public class Channel
        {
            public string name = string.Empty;
            public int channel = 0;
            public ChannelType type = ChannelType.undefined;
            public double lastReading = 0;
            public int range = 0;                   // if set to 0, it will autorange if instrument can
            public double gain = 1;
        };


        public class fixtureTypes
        {
            public string desc = string.Empty;
            public string fixtureType = string.Empty;       // defines the config of a fixture

            // DUT interfaces
            public bool hasEnlightedInterface = false;      // enlighted interface, CU and 0-10V output
            public bool hasDaliInterface = false;           // has 2 wire dali interface

            // DUT radios
            public bool hasBLE = false;                     // has bluetooth.
            public bool hasEnlighteRadio = false;           // has Enlighted radio

            // instruments
            public List<Instrument> InstrumentDefs;               // definition of the channels on DAQ
        };

        public List<fixtureTypes> fixtureTypeList = new List<fixtureTypes>
        {

            //Kona fixture
            new fixtureTypes
            {
                desc = "Functional SU-5S fixture",
                fixtureType = FT_SU_5S_FCT,
                InstrumentDefs = new List<Instrument>() {
                    new Instrument() {instrumentID = InstrumentID.LABJACK, connection = "DRIVER", stationconfigTag = "DAQ",   // connection = DRIVER, dirver connected
                        channels = new List<Channel>() {
                            new Channel() { name = CH_DIM1_OUTPUT, type = ChannelType.DCInput, channel = 2, lastReading =0, range = 10, gain = 3 },
                            new Channel() { name = CH_DIM2_OUTPUT, type = ChannelType.DCInput, channel = 0, lastReading =0, range = 10, gain = 3 },
                            new Channel() { name = CH_V3P3_VOLTAGE, type = ChannelType.DCInput, channel = 1, lastReading =0, range = 10},
                            new Channel() { name = CH_TEMPERTURE_PROBE, type = ChannelType.TemperatureC, channel = 3, lastReading =0, range = 10},
                            new Channel() { name = CH_OPERATOR_BUTTON, type = ChannelType.DInput, channel = 4, lastReading =0, range = 10},
                            new Channel() { name = CH_FIXTURE_LID_SWITCH, type = ChannelType.DInput, channel = 5, lastReading =0, range = 10},
                            new Channel() { name = CH_AMBIENT_DETECTOR_LED, type = ChannelType.DAC, channel = 0, lastReading = 0, range = 10 },
                        },
                    },
                    new Instrument() {instrumentID = InstrumentID.SwitchMate, connection = string.Empty, stationconfigTag = "Switch-Mate",
                        channels = new List<Channel>() {
                            new Channel() { name = CH_DUT_POWER_CNTL, type = ChannelType.Contact, channel = 1, lastReading = 0, range = 0 },
                            new Channel() { name = CH_PIR_SOURCE_CNTL, type = ChannelType.Contact, channel = 2, lastReading = 0, range = 0 },
                        },
                    },
                    new Instrument() { instrumentID = InstrumentID.LEDDetector, connection = string.Empty, stationconfigTag = "LEDDetector",
                        channels = new List<Channel>() {
                            new Channel() { name = CH_LED_DETECTOR, type = ChannelType.TextSerial }
                        },
                    },
                    new  Instrument() { instrumentID = InstrumentID.TestRadio, connection = string.Empty, stationconfigTag = "TestRadio",
                        channels = new List<Channel>() {
                            new Channel () { name = CH_REF_RADIO, type = ChannelType.COBSPacketSerial }
                        },
                    },
                    new Instrument() { instrumentID = InstrumentID.DUTConsole, connection = string.Empty, stationconfigTag = "DUT",
                        channels = new List<Channel>() {
                            new Channel() { name =  CH_DUT_CONSOLE,  type = ChannelType.COBSPacketSerial },
                            new Channel() { name = CH_DUT_INTERFACE, type = ChannelType.Enlighted2channel },
                            new Channel() { name = CH_DUT_ENLIGHTED_RADIO },
                            new Channel() { name = CH_DUT_BLE_RADIO }
                        },
                    },
                    new Instrument() { instrumentID = InstrumentID.Flasher, connection = string.Empty, stationconfigTag = "Flasher",
                        channels = new List<Channel>()
                        {
                            new Channel() { name = CH_FLASHER },
                        },
                    }
                },
            },

                        new fixtureTypes
            {
                desc = "Functional SU-5E fixture",
                fixtureType = FT_SU_5E_FCT,
                InstrumentDefs = new List<Instrument>() {
                    new Instrument() {instrumentID = InstrumentID.LABJACK, connection = "DRIVER", stationconfigTag = "DAQ",   // connection = DRIVER, dirver connected
                        channels = new List<Channel>() {
                            new Channel() { name = CH_DIM1_OUTPUT, type = ChannelType.DCInput, channel = 2, lastReading =0, range = 10, gain = 3 },
                            new Channel() { name = CH_DIM2_OUTPUT, type = ChannelType.DCInput, channel = 0, lastReading =0, range = 10, gain = 3 },
                            new Channel() { name = CH_V3P3_VOLTAGE, type = ChannelType.DCInput, channel = 1, lastReading =0, range = 10},
                            new Channel() { name = CH_TEMPERTURE_PROBE, type = ChannelType.TemperatureC, channel = 3, lastReading =0, range = 10},
                            new Channel() { name = CH_OPERATOR_BUTTON, type = ChannelType.DInput, channel = 4, lastReading =0, range = 10},
                            new Channel() { name = CH_FIXTURE_LID_SWITCH, type = ChannelType.DInput, channel = 5, lastReading =0, range = 10},
                            new Channel() { name = CH_AMBIENT_DETECTOR_LED, type = ChannelType.DAC, channel = 0, lastReading = 0, range = 10 },
                        },
                    },
                    new Instrument() {instrumentID = InstrumentID.SwitchMate, connection = string.Empty, stationconfigTag = "Switch-Mate",
                        channels = new List<Channel>() {
                            new Channel() { name = CH_DUT_POWER_CNTL, type = ChannelType.Contact, channel = 1, lastReading = 0, range = 0 },
                            new Channel() { name = CH_PIR_SOURCE_CNTL, type = ChannelType.Contact, channel = 2, lastReading = 0, range = 0 },
                        },
                    },
                    new Instrument() { instrumentID = InstrumentID.LEDDetector, connection = string.Empty, stationconfigTag = "LEDDetector",
                        channels = new List<Channel>() {
                            new Channel() { name = CH_LED_DETECTOR, type = ChannelType.TextSerial }
                        },
                    },
                    new  Instrument() { instrumentID = InstrumentID.TestRadio, connection = string.Empty, stationconfigTag = "TestRadio",
                        channels = new List<Channel>() {
                            new Channel () { name = CH_REF_RADIO, type = ChannelType.COBSPacketSerial }
                        },
                    },
                    new Instrument() { instrumentID = InstrumentID.DUTConsole, connection = string.Empty, stationconfigTag = "DUT",
                        channels = new List<Channel>() {
                            new Channel() { name =  CH_DUT_CONSOLE,  type = ChannelType.COBSPacketSerial },
                            new Channel() { name = CH_DUT_INTERFACE, type = ChannelType.Enlighted2channel },
                            new Channel() { name = CH_DUT_ENLIGHTED_RADIO },
                            new Channel() { name = CH_DUT_BLE_RADIO }
                        },
                    },
                    new Instrument() { instrumentID = InstrumentID.Flasher, connection = string.Empty, stationconfigTag = "Flasher",
                        channels = new List<Channel>()
                        {
                            new Channel() { name = CH_FLASHER },
                        },
                    }
                },
            },

            new fixtureTypes
            {
                desc = "SU-5E Final",
                fixtureType = FT_SU_5E_FNL,
                InstrumentDefs = new List<Instrument>() {
                     new Instrument() {instrumentID = InstrumentID.HLAPrinter, connection = string.Empty, stationconfigTag = "ProductLabelPrinter",
                         channels = new List<Channel>() {
                            new Channel() { name =  CH_HLA_PRINTER}
                         },
                     },
                     new Instrument() { instrumentID = InstrumentID.DUTConsole, connection = string.Empty, stationconfigTag = "DUT",
                        channels = new List<Channel>() {
                            new Channel() { name =  CH_DUT_CONSOLE,  type = ChannelType.COBSPacketSerial },
                            new Channel() { name = CH_DUT_INTERFACE, type = ChannelType.Enlighted2channel },
                            new Channel() { name = CH_DUT_ENLIGHTED_RADIO },
                            new Channel() { name = CH_DUT_BLE_RADIO }
                        },
                     },
                },
            },

            new fixtureTypes
            {
                desc = "SU-5S Final",
                fixtureType = FT_SU_5S_FNL,
                InstrumentDefs = new List<Instrument>() {
                     new Instrument() {instrumentID = InstrumentID.HLAPrinter, connection = string.Empty, stationconfigTag = "ProductLabelPrinter",
                         channels = new List<Channel>() {
                            new Channel() { name =  CH_HLA_PRINTER}
                         },
                     },
                     new Instrument() {instrumentID = InstrumentID.PCBAPrinter, connection = string.Empty, stationconfigTag = "PCBALabelPrinter",
                         channels = new List<Channel>() {
                            new Channel() { name =  CH_PCBA_PRINTER}
                         },
                     },
                     new Instrument() { instrumentID = InstrumentID.DUTConsole, connection = string.Empty, stationconfigTag = "DUT",
                        channels = new List<Channel>() {
                            new Channel() { name =  CH_DUT_CONSOLE,  type = ChannelType.COBSPacketSerial },
                            new Channel() { name = CH_DUT_INTERFACE, type = ChannelType.Enlighted2channel },
                            new Channel() { name = CH_DUT_ENLIGHTED_RADIO },
                            new Channel() { name = CH_DUT_BLE_RADIO }
                        },
                     },
                },


            },

            new fixtureTypes
            {
                desc = "Label Print",
                fixtureType = FT_LABELPRINT_LBP,
                InstrumentDefs = new List<Instrument>() {
                     new Instrument() {instrumentID = InstrumentID.HLAPrinter, connection = string.Empty, stationconfigTag = "ProductLabelPrinter",
                         channels = new List<Channel>() {
                            new Channel() { name =  CH_HLA_PRINTER}
                         },
                     },
                     new Instrument() {instrumentID = InstrumentID.PCBAPrinter, connection = string.Empty, stationconfigTag = "PCBALabelPrinter",
                         channels = new List<Channel>() {
                            new Channel() { name =  CH_PCBA_PRINTER}
                         },
                     },
                },


            },



        //        // old gateway fixture
        //        //new FixtureInstrument.fixtureTypes
        //        //{
        //        //    desc = "GW-2 Functional",
        //        //    fixtureType = "GW-2-FCT",
        //        //    hasEnlightedInterface = false,       // enlighted interface, CU and 0-10V output
        //        //    hasConsolePort = true,              // has a console port to the DUT
        //        //    usesCUPortForConsole = false,        // The console port is on the CU port.
        //        //    usesPacketBasedConsole = false,      // not packet based 
        //        //    usesTextBaseConsole = true,         // uses text based
        //        //    hasBLE = false,                     // has bluetooth.
        //        //    hasTestRadio = true,               // has a test radio(Enlighted only)
        //        //    usesAtmelRefRadio = true,           // uses old atmel ref radio
        //        //    hasSniffer = false,                 // no sniffer
        //        //    channelDefs = new List<FixtureInstrument.Channel>() {
        //        //        new FixtureInstrument.Channel() { name = CH_V3P3_VOLTAGE, type = FixtureInstrument.ChannelType.DCInput, channel = 2, lastReading =0, instrumentID = FixtureInstrument.InstrumentID.LABJACK, range = 10},
        //        //        new FixtureInstrument.Channel() { name = CH_FIXTURE_LID_SWITCH, type = FixtureInstrument.ChannelType.DInput, channel = 4, lastReading =0, instrumentID = FixtureInstrument.InstrumentID.LABJACK, range = 10},
        //        //        new FixtureInstrument.Channel() { name = CH_LED_DETECTOR, type = FixtureInstrument.ChannelType.Frequency, channel = 5, instrumentID = FixtureInstrument.InstrumentID.LABJACK, range = 10 }
        //        //        new FixtureInstrument.Channel() { name =  CH_PCBA_PRINTER, instrumentID = FixtureInstrument.InstrumentID.PCBAPrinter},
        //        //        new FixtureInstrument.Channel() { name =  CH_HLA_PRINTER, instrumentID = FixtureInstrument.InstrumentID.HLAPrinter},
        //        //    },
        //        //},


                // new fixture
                new fixtureTypes
                {
                    desc = "GW-2.1 Functional",
                    fixtureType = FT_GW_2_1_FCT,
                    InstrumentDefs = new List<Instrument>() {
                        new Instrument() {instrumentID = InstrumentID.A34970A, connection = string.Empty, stationconfigTag = "DAQ",
                            channels = new List<Channel>() {
                                new Channel() {  name = CH_V3P3_VOLTAGE, type = ChannelType.DCInput, channel = 101, lastReading = 0, range = 10},
                                new Channel() { name = CH_FIXTURE_LID_SWITCH, type = ChannelType.DCInput, channel = 104, lastReading = 0,  range = 100},    // 12V max
                                new Channel() { name = CH_OPERATOR_BUTTON, type = ChannelType.DCInput, channel = 105, lastReading = 0,  range = 100},       // 12V max
                                new Channel() { name = CH_FIXTURE_POWER, type = ChannelType.DCInput, channel = 106, lastReading = 0,  range = 10},
                                new Channel() { name = CH_DUT_POWER_CNTL1, type = ChannelType.Contact, channel = 201, lastReading = 0,  range = 10},
                                new Channel() { name = CH_DUT_POWER_CNTL2, type = ChannelType.Contact, channel = 202, lastReading = 0,  range = 10},
                                new Channel() { name = CH_DUT_POWER_CNTL3, type = ChannelType.Contact, channel = 203, lastReading = 0,  range = 10},
                                new Channel() { name = CH_DUT_POWER_CNTL4, type = ChannelType.Contact, channel = 204, lastReading = 0,  range = 10},
                                new Channel() { name = CH_DUT_CONSOLE_CONNECT, type = ChannelType.Contact, channel = 206, lastReading = 0,  range = 10},
                                new Channel() { name = CH_DUT_TESTING_LED, type = ChannelType.Contact, channel = 207, lastReading = 0,  range = 10}
                            },
                        },
                        new Instrument() { instrumentID = InstrumentID.LEDDetector, connection = string.Empty, stationconfigTag = "LEDDetector",
                            channels = new List<Channel>() {
                                new Channel() { name = CH_LED_DETECTOR, type = ChannelType.TextSerial }
                            },
                        },
                        new  Instrument() { instrumentID = InstrumentID.TestRadio, connection = string.Empty, stationconfigTag = "TestRadio",
                            channels = new List<Channel>() {
                                new Channel () { name = CH_REF_RADIO, type = ChannelType.TextSerial }
                            },
                        },
                     new Instrument() {instrumentID = InstrumentID.HLAPrinter, connection = string.Empty, stationconfigTag = "ProductLabelPrinter",
                         channels = new List<Channel>() {
                            new Channel() { name =  CH_HLA_PRINTER}
                         },
                     },
                        new Instrument() { instrumentID = InstrumentID.DUTConsole, connection = string.Empty, stationconfigTag = "DUT",
                            channels = new List<Channel>() {
                                new Channel() { name =  CH_DUT_CONSOLE,  type = ChannelType.TextSerial },
                                new Channel() { name = CH_DUT_INTERFACE, type = ChannelType.Enlighted2channel },
                                new Channel() { name = CH_DUT_ENLIGHTED_RADIO },
                                new Channel() { name = CH_DUT_BLE_RADIO }
                            },
                        },
                    },
                },

            };      // end of List<fixtureTypes>

    };

}
