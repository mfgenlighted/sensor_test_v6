﻿namespace SensorManufSY2
{
    partial class FixtureEquipList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbInterfacEnlighted = new System.Windows.Forms.CheckBox();
            this.cbInterfaceDali = new System.Windows.Forms.CheckBox();
            this.cbRadioEnlighted = new System.Windows.Forms.CheckBox();
            this.cbRadioBLE = new System.Windows.Forms.CheckBox();
            this.cbSensorPIR = new System.Windows.Forms.CheckBox();
            this.cbSensorAmbient = new System.Windows.Forms.CheckBox();
            this.cbLEDs = new System.Windows.Forms.CheckBox();
            this.cbConsolePort = new System.Windows.Forms.CheckBox();
            this.cbDUTPowerControl = new System.Windows.Forms.CheckBox();
            this.cbERCPowerControl = new System.Windows.Forms.CheckBox();
            this.cbERCButtonPushers = new System.Windows.Forms.CheckBox();
            this.cbOperatorButton = new System.Windows.Forms.CheckBox();
            this.cbTempProbe = new System.Windows.Forms.CheckBox();
            this.cbPowerLoad = new System.Windows.Forms.CheckBox();
            this.cbPowerMeter = new System.Windows.Forms.CheckBox();
            this.cbPCBAPrinter = new System.Windows.Forms.CheckBox();
            this.cbHLAPrinter = new System.Windows.Forms.CheckBox();
            this.cbLidSwitch = new System.Windows.Forms.CheckBox();
            this.cbRFSniffer = new System.Windows.Forms.CheckBox();
            this.cbLabjack = new System.Windows.Forms.CheckBox();
            this.Interfaces = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.tbDescription = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbFixtureType = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.cbSwitchMate = new System.Windows.Forms.CheckBox();
            this.Interfaces.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbInterfacEnlighted
            // 
            this.cbInterfacEnlighted.AutoSize = true;
            this.cbInterfacEnlighted.Enabled = false;
            this.cbInterfacEnlighted.Location = new System.Drawing.Point(15, 19);
            this.cbInterfacEnlighted.Name = "cbInterfacEnlighted";
            this.cbInterfacEnlighted.Size = new System.Drawing.Size(70, 17);
            this.cbInterfacEnlighted.TabIndex = 0;
            this.cbInterfacEnlighted.Text = "Enlighted";
            this.cbInterfacEnlighted.UseVisualStyleBackColor = true;
            // 
            // cbInterfaceDali
            // 
            this.cbInterfaceDali.AutoSize = true;
            this.cbInterfaceDali.Enabled = false;
            this.cbInterfaceDali.Location = new System.Drawing.Point(15, 42);
            this.cbInterfaceDali.Name = "cbInterfaceDali";
            this.cbInterfaceDali.Size = new System.Drawing.Size(44, 17);
            this.cbInterfaceDali.TabIndex = 1;
            this.cbInterfaceDali.Text = "Dali";
            this.cbInterfaceDali.UseVisualStyleBackColor = true;
            // 
            // cbRadioEnlighted
            // 
            this.cbRadioEnlighted.AutoSize = true;
            this.cbRadioEnlighted.Enabled = false;
            this.cbRadioEnlighted.Location = new System.Drawing.Point(15, 32);
            this.cbRadioEnlighted.Name = "cbRadioEnlighted";
            this.cbRadioEnlighted.Size = new System.Drawing.Size(70, 17);
            this.cbRadioEnlighted.TabIndex = 2;
            this.cbRadioEnlighted.Text = "Enlighted";
            this.cbRadioEnlighted.UseVisualStyleBackColor = true;
            // 
            // cbRadioBLE
            // 
            this.cbRadioBLE.AutoSize = true;
            this.cbRadioBLE.Enabled = false;
            this.cbRadioBLE.Location = new System.Drawing.Point(15, 55);
            this.cbRadioBLE.Name = "cbRadioBLE";
            this.cbRadioBLE.Size = new System.Drawing.Size(46, 17);
            this.cbRadioBLE.TabIndex = 3;
            this.cbRadioBLE.Text = "BLE";
            this.cbRadioBLE.UseVisualStyleBackColor = true;
            // 
            // cbSensorPIR
            // 
            this.cbSensorPIR.AutoSize = true;
            this.cbSensorPIR.Enabled = false;
            this.cbSensorPIR.Location = new System.Drawing.Point(5, 19);
            this.cbSensorPIR.Name = "cbSensorPIR";
            this.cbSensorPIR.Size = new System.Drawing.Size(44, 17);
            this.cbSensorPIR.TabIndex = 4;
            this.cbSensorPIR.Text = "PIR";
            this.cbSensorPIR.UseVisualStyleBackColor = true;
            // 
            // cbSensorAmbient
            // 
            this.cbSensorAmbient.AutoSize = true;
            this.cbSensorAmbient.Enabled = false;
            this.cbSensorAmbient.Location = new System.Drawing.Point(6, 36);
            this.cbSensorAmbient.Name = "cbSensorAmbient";
            this.cbSensorAmbient.Size = new System.Drawing.Size(64, 17);
            this.cbSensorAmbient.TabIndex = 5;
            this.cbSensorAmbient.Text = "Ambient";
            this.cbSensorAmbient.UseVisualStyleBackColor = true;
            // 
            // cbLEDs
            // 
            this.cbLEDs.AutoSize = true;
            this.cbLEDs.Enabled = false;
            this.cbLEDs.Location = new System.Drawing.Point(111, 128);
            this.cbLEDs.Name = "cbLEDs";
            this.cbLEDs.Size = new System.Drawing.Size(52, 17);
            this.cbLEDs.TabIndex = 6;
            this.cbLEDs.Text = "LEDs";
            this.cbLEDs.UseVisualStyleBackColor = true;
            // 
            // cbConsolePort
            // 
            this.cbConsolePort.AutoSize = true;
            this.cbConsolePort.Enabled = false;
            this.cbConsolePort.Location = new System.Drawing.Point(111, 151);
            this.cbConsolePort.Name = "cbConsolePort";
            this.cbConsolePort.Size = new System.Drawing.Size(85, 17);
            this.cbConsolePort.TabIndex = 7;
            this.cbConsolePort.Text = "Console port";
            this.cbConsolePort.UseVisualStyleBackColor = true;
            // 
            // cbDUTPowerControl
            // 
            this.cbDUTPowerControl.AutoSize = true;
            this.cbDUTPowerControl.Enabled = false;
            this.cbDUTPowerControl.Location = new System.Drawing.Point(11, 23);
            this.cbDUTPowerControl.Name = "cbDUTPowerControl";
            this.cbDUTPowerControl.Size = new System.Drawing.Size(118, 17);
            this.cbDUTPowerControl.TabIndex = 8;
            this.cbDUTPowerControl.Text = "DUT Power Control";
            this.cbDUTPowerControl.UseVisualStyleBackColor = true;
            // 
            // cbERCPowerControl
            // 
            this.cbERCPowerControl.AutoSize = true;
            this.cbERCPowerControl.Enabled = false;
            this.cbERCPowerControl.Location = new System.Drawing.Point(11, 47);
            this.cbERCPowerControl.Name = "cbERCPowerControl";
            this.cbERCPowerControl.Size = new System.Drawing.Size(117, 17);
            this.cbERCPowerControl.TabIndex = 9;
            this.cbERCPowerControl.Text = "ERC Power Control";
            this.cbERCPowerControl.UseVisualStyleBackColor = true;
            // 
            // cbERCButtonPushers
            // 
            this.cbERCButtonPushers.AutoSize = true;
            this.cbERCButtonPushers.Enabled = false;
            this.cbERCButtonPushers.Location = new System.Drawing.Point(11, 70);
            this.cbERCButtonPushers.Name = "cbERCButtonPushers";
            this.cbERCButtonPushers.Size = new System.Drawing.Size(123, 17);
            this.cbERCButtonPushers.TabIndex = 10;
            this.cbERCButtonPushers.Text = "ERC Button Pushers";
            this.cbERCButtonPushers.UseVisualStyleBackColor = true;
            // 
            // cbOperatorButton
            // 
            this.cbOperatorButton.AutoSize = true;
            this.cbOperatorButton.Enabled = false;
            this.cbOperatorButton.Location = new System.Drawing.Point(184, 47);
            this.cbOperatorButton.Name = "cbOperatorButton";
            this.cbOperatorButton.Size = new System.Drawing.Size(100, 17);
            this.cbOperatorButton.TabIndex = 11;
            this.cbOperatorButton.Text = "Operator button";
            this.cbOperatorButton.UseVisualStyleBackColor = true;
            // 
            // cbTempProbe
            // 
            this.cbTempProbe.AutoSize = true;
            this.cbTempProbe.Enabled = false;
            this.cbTempProbe.Location = new System.Drawing.Point(11, 112);
            this.cbTempProbe.Name = "cbTempProbe";
            this.cbTempProbe.Size = new System.Drawing.Size(84, 17);
            this.cbTempProbe.TabIndex = 12;
            this.cbTempProbe.Text = "Temp Probe";
            this.cbTempProbe.UseVisualStyleBackColor = true;
            // 
            // cbPowerLoad
            // 
            this.cbPowerLoad.AutoSize = true;
            this.cbPowerLoad.Enabled = false;
            this.cbPowerLoad.Location = new System.Drawing.Point(11, 135);
            this.cbPowerLoad.Name = "cbPowerLoad";
            this.cbPowerLoad.Size = new System.Drawing.Size(83, 17);
            this.cbPowerLoad.TabIndex = 13;
            this.cbPowerLoad.Text = "Power Load";
            this.cbPowerLoad.UseVisualStyleBackColor = true;
            // 
            // cbPowerMeter
            // 
            this.cbPowerMeter.AutoSize = true;
            this.cbPowerMeter.Enabled = false;
            this.cbPowerMeter.Location = new System.Drawing.Point(11, 162);
            this.cbPowerMeter.Name = "cbPowerMeter";
            this.cbPowerMeter.Size = new System.Drawing.Size(86, 17);
            this.cbPowerMeter.TabIndex = 14;
            this.cbPowerMeter.Text = "Power Meter";
            this.cbPowerMeter.UseVisualStyleBackColor = true;
            // 
            // cbPCBAPrinter
            // 
            this.cbPCBAPrinter.AutoSize = true;
            this.cbPCBAPrinter.Enabled = false;
            this.cbPCBAPrinter.Location = new System.Drawing.Point(184, 148);
            this.cbPCBAPrinter.Name = "cbPCBAPrinter";
            this.cbPCBAPrinter.Size = new System.Drawing.Size(87, 17);
            this.cbPCBAPrinter.TabIndex = 15;
            this.cbPCBAPrinter.Text = "PCBA Printer";
            this.cbPCBAPrinter.UseVisualStyleBackColor = true;
            // 
            // cbHLAPrinter
            // 
            this.cbHLAPrinter.AutoSize = true;
            this.cbHLAPrinter.Enabled = false;
            this.cbHLAPrinter.Location = new System.Drawing.Point(184, 171);
            this.cbHLAPrinter.Name = "cbHLAPrinter";
            this.cbHLAPrinter.Size = new System.Drawing.Size(80, 17);
            this.cbHLAPrinter.TabIndex = 16;
            this.cbHLAPrinter.Text = "HLA Printer";
            this.cbHLAPrinter.UseVisualStyleBackColor = true;
            // 
            // cbLidSwitch
            // 
            this.cbLidSwitch.AutoSize = true;
            this.cbLidSwitch.Enabled = false;
            this.cbLidSwitch.Location = new System.Drawing.Point(184, 23);
            this.cbLidSwitch.Name = "cbLidSwitch";
            this.cbLidSwitch.Size = new System.Drawing.Size(75, 17);
            this.cbLidSwitch.TabIndex = 17;
            this.cbLidSwitch.Text = "Lid Switch";
            this.cbLidSwitch.UseVisualStyleBackColor = true;
            // 
            // cbRFSniffer
            // 
            this.cbRFSniffer.AutoSize = true;
            this.cbRFSniffer.Enabled = false;
            this.cbRFSniffer.Location = new System.Drawing.Point(11, 185);
            this.cbRFSniffer.Name = "cbRFSniffer";
            this.cbRFSniffer.Size = new System.Drawing.Size(73, 17);
            this.cbRFSniffer.TabIndex = 18;
            this.cbRFSniffer.Text = "RF Sniffer";
            this.cbRFSniffer.UseVisualStyleBackColor = true;
            // 
            // cbLabjack
            // 
            this.cbLabjack.AutoSize = true;
            this.cbLabjack.Enabled = false;
            this.cbLabjack.Location = new System.Drawing.Point(156, 96);
            this.cbLabjack.Name = "cbLabjack";
            this.cbLabjack.Size = new System.Drawing.Size(115, 17);
            this.cbLabjack.TabIndex = 19;
            this.cbLabjack.Text = "LabJack Data Acq";
            this.cbLabjack.UseVisualStyleBackColor = true;
            // 
            // Interfaces
            // 
            this.Interfaces.Controls.Add(this.cbInterfacEnlighted);
            this.Interfaces.Controls.Add(this.cbInterfaceDali);
            this.Interfaces.Location = new System.Drawing.Point(6, 19);
            this.Interfaces.Name = "Interfaces";
            this.Interfaces.Size = new System.Drawing.Size(93, 74);
            this.Interfaces.TabIndex = 20;
            this.Interfaces.TabStop = false;
            this.Interfaces.Text = "Interfaces";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbRadioEnlighted);
            this.groupBox1.Controls.Add(this.cbRadioBLE);
            this.groupBox1.Location = new System.Drawing.Point(6, 102);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(93, 95);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Radios";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbSensorPIR);
            this.groupBox2.Controls.Add(this.cbSensorAmbient);
            this.groupBox2.Location = new System.Drawing.Point(105, 19);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(106, 88);
            this.groupBox2.TabIndex = 22;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Sensors";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.Interfaces);
            this.groupBox3.Controls.Add(this.groupBox2);
            this.groupBox3.Controls.Add(this.groupBox1);
            this.groupBox3.Controls.Add(this.cbLEDs);
            this.groupBox3.Controls.Add(this.cbConsolePort);
            this.groupBox3.Enabled = false;
            this.groupBox3.Location = new System.Drawing.Point(18, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(217, 224);
            this.groupBox3.TabIndex = 23;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "DUT";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.cbSwitchMate);
            this.groupBox4.Controls.Add(this.cbDUTPowerControl);
            this.groupBox4.Controls.Add(this.cbERCPowerControl);
            this.groupBox4.Controls.Add(this.cbLabjack);
            this.groupBox4.Controls.Add(this.cbERCButtonPushers);
            this.groupBox4.Controls.Add(this.cbRFSniffer);
            this.groupBox4.Controls.Add(this.cbOperatorButton);
            this.groupBox4.Controls.Add(this.cbPowerMeter);
            this.groupBox4.Controls.Add(this.cbPCBAPrinter);
            this.groupBox4.Controls.Add(this.cbPowerLoad);
            this.groupBox4.Controls.Add(this.cbHLAPrinter);
            this.groupBox4.Controls.Add(this.cbTempProbe);
            this.groupBox4.Controls.Add(this.cbLidSwitch);
            this.groupBox4.Enabled = false;
            this.groupBox4.Location = new System.Drawing.Point(18, 255);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(291, 241);
            this.groupBox4.TabIndex = 24;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Fixture";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(366, 403);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 25;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(366, 440);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 26;
            this.button2.Text = "Done";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // tbDescription
            // 
            this.tbDescription.Enabled = false;
            this.tbDescription.Location = new System.Drawing.Point(264, 47);
            this.tbDescription.Name = "tbDescription";
            this.tbDescription.Size = new System.Drawing.Size(198, 20);
            this.tbDescription.TabIndex = 27;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Enabled = false;
            this.label1.Location = new System.Drawing.Point(267, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 28;
            this.label1.Text = "Description";
            // 
            // tbFixtureType
            // 
            this.tbFixtureType.Enabled = false;
            this.tbFixtureType.Location = new System.Drawing.Point(264, 99);
            this.tbFixtureType.Name = "tbFixtureType";
            this.tbFixtureType.Size = new System.Drawing.Size(159, 20);
            this.tbFixtureType.TabIndex = 29;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Enabled = false;
            this.label2.Location = new System.Drawing.Point(270, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 30;
            this.label2.Text = "Fixture Type";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(356, 363);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(106, 23);
            this.button3.TabIndex = 31;
            this.button3.Text = "Edit List";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // cbSwitchMate
            // 
            this.cbSwitchMate.AutoSize = true;
            this.cbSwitchMate.Location = new System.Drawing.Point(156, 120);
            this.cbSwitchMate.Name = "cbSwitchMate";
            this.cbSwitchMate.Size = new System.Drawing.Size(85, 17);
            this.cbSwitchMate.TabIndex = 20;
            this.cbSwitchMate.Text = "Switch-Mate";
            this.cbSwitchMate.UseVisualStyleBackColor = true;
            // 
            // FixtureEquipList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(491, 508);
            this.ControlBox = false;
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbFixtureType);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbDescription);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Name = "FixtureEquipList";
            this.Text = "Fixture Equipment List";
            this.Interfaces.ResumeLayout(false);
            this.Interfaces.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox cbInterfacEnlighted;
        private System.Windows.Forms.CheckBox cbInterfaceDali;
        private System.Windows.Forms.CheckBox cbRadioEnlighted;
        private System.Windows.Forms.CheckBox cbRadioBLE;
        private System.Windows.Forms.CheckBox cbSensorPIR;
        private System.Windows.Forms.CheckBox cbSensorAmbient;
        private System.Windows.Forms.CheckBox cbLEDs;
        private System.Windows.Forms.CheckBox cbConsolePort;
        private System.Windows.Forms.CheckBox cbDUTPowerControl;
        private System.Windows.Forms.CheckBox cbERCPowerControl;
        private System.Windows.Forms.CheckBox cbERCButtonPushers;
        private System.Windows.Forms.CheckBox cbOperatorButton;
        private System.Windows.Forms.CheckBox cbTempProbe;
        private System.Windows.Forms.CheckBox cbPowerLoad;
        private System.Windows.Forms.CheckBox cbPowerMeter;
        private System.Windows.Forms.CheckBox cbPCBAPrinter;
        private System.Windows.Forms.CheckBox cbHLAPrinter;
        private System.Windows.Forms.CheckBox cbLidSwitch;
        private System.Windows.Forms.CheckBox cbRFSniffer;
        private System.Windows.Forms.CheckBox cbLabjack;
        private System.Windows.Forms.GroupBox Interfaces;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox tbDescription;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbFixtureType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.CheckBox cbSwitchMate;
    }
}