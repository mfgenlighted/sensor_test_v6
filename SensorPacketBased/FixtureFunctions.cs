﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Xml.Linq;
using System.Management;

using A34970ADriver;

namespace SensorManufSY2
{

    public class FixtureFunctions :  IDisposable
    {
        //----------------------
        // class properties
        //----------------------
        bool disposed = false;
        //private StationConfigure stationConfigure = null;   // all the config data
        //public StationConfigure StationConfigure { set { stationConfigure = value; } }
        public int DUTTurnOnDelayMs = 100;                 // time to let relays settle after commands

        public string LastFunctionErrorMessage = string.Empty;
        public int LastFunctionStatus = 0;
        public int BaseFixtureSystemErrorCode = -6000;          // base error number for system fails

        public class PassData
        {
            public string channelName;
            public double returnedData;
        }


        // instruments
        private LabJackDAQInstrument labjackDAQ;
        private A34970ADAQInstrument a34970ADAQ;
        private SwitchMateInstrument switchmateRelays;
        private OptomisticInstrument optomisticLEDDetector;
        private Flasher flasherProgrammer;

        private StationConfigure stationConfigure;

        //---------------------------------------
        //  Ref Radio data
        //---------------------------------------
        private string refRadioMAC = string.Empty;              // ref radio mac holder
        private string refBLEMAC = string.Empty;
            //---- default values
        public uint RadioDefaultChannel = 15;
        public uint RadioDefaultPanID = 0x6854;
        public uint RadioDefaultTxPower = 0;
        public string RadioDefaultEncryptKey = "manufacturetest1";
        public uint RadioDefaultTTL = 3;
        public uint RadioDeraultRate = 16;
            //----Properties
        public PacketBasedConsole PacketRefRadio;                      // handle for ref radio   
        public TextBasedConsole TextRefRadio;
        public bool IsRefRadioPacketBased = true;

        public string RefRadioMAC { get { return refRadioMAC; } } 
        public string RefBLEMAC { get { return refBLEMAC; } }


        //---------------------------------
        //  DUT Console
        //--------------------------------
        public PacketBasedConsole PacketDutConsole;
        public TextBasedConsole TextDutConsole;
        public CUCommications CUDutConsole;
        public bool IsDutConsolePacketBased = true;



        /// <summary>
        /// Will open the class, but will not initialize the fixture. StationConfig
        /// must be setup before calling InitFixture.
        /// </summary>
        public FixtureFunctions(StationConfigure config)
        {
            stationConfigure = config;                           // station configurations
        }


        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if (PacketRefRadio != null)
                    {
                        PacketRefRadio.Close();
                        PacketRefRadio = null;
                    }
                    if (TextRefRadio != null)
                    {
                        TextRefRadio.Close();
                        TextRefRadio = null;
                    }
                    if (PacketDutConsole != null)
                    {
                        PacketDutConsole.Close();
                        PacketDutConsole = null;
                    }
                    if (CUDutConsole != null)
                    {
                        CUDutConsole.Close();
                        CUDutConsole = null;
                    }
                    if (TextDutConsole != null)
                    {
                        TextDutConsole.Close();
                        TextDutConsole = null;
                    }
                    if (a34970ADAQ != null)
                    {
                        a34970ADAQ.Disconnect();
                        a34970ADAQ = null;
                    }
                    if (labjackDAQ != null)
                    {
                        labjackDAQ.Disconnect();
                        labjackDAQ = null;
                    }
                    if (switchmateRelays != null)
                    {
                        switchmateRelays.Disconnect();
                        switchmateRelays = null;
                    }
                    if (optomisticLEDDetector != null)
                    {
                        optomisticLEDDetector.Disconnect();
                        optomisticLEDDetector = null;
                    }
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public bool InitFixture()
        {
            return InitFixture(stationConfigure.currentFixDef.InstrumentDefs);
        }

        /// <summary>
        /// Will intialize the fixture hardware. will return false if error.
        /// </summary>
        public bool InitFixture(List<FixtureDefintions.Instrument> enableList)
        {
            string msgs = string.Empty;

            // Variables to satisfy certain method signatures
            double[] dummyDoubleArray = { 0 };

            LastFunctionErrorMessage = string.Empty;
            LastFunctionStatus = 0;

            if (stationConfigure == null)   // if it has not been set up yet
            {
                LastFunctionErrorMessage = "StationConfigure has not been set up before call InitFixture";
                LastFunctionStatus = FixtureErrorCodes.StationConfigNotRead;
                return false;
            }

            UpdateStatusWindow("Start Fixture init\n", StatusType.passed);


            // if  ref radio is installed
            if (enableList.Exists(x => x.instrumentID == FixtureDefintions.InstrumentID.TestRadio))
            {
                UpdateStatusWindow("   Setting up Ref Radio - ", StatusType.text);
                if (stationConfigure.GetChannelData(FixtureDefintions.CH_REF_RADIO).type == FixtureDefintions.ChannelType.TextSerial)
                    IsRefRadioPacketBased = false;
                if (IsRefRadioPacketBased)
                {
                    PacketRefRadio = new PacketBasedConsole();
                    if (!PacketRefRadio.Open(stationConfigure.GetInstrumentData(FixtureDefintions.InstrumentID.TestRadio).connection, 115200))
                    {
                        LastFunctionErrorMessage = PacketRefRadio.LastErrorMessage;
                        LastFunctionStatus = PacketRefRadio.LastErrorCode;
                        UpdateStatusWindow(" Failed - " + LastFunctionErrorMessage + "\n", StatusType.failed);
                        return false;
                    }
                    PacketRefRadio.DisplayRcvChar = stationConfigure.ConfigDisplayRcvChar;
                    PacketRefRadio.DisplaySendChar = stationConfigure.ConfigDisplaySendChar;
                    PacketRefRadio.IsThisDUTConsole = false;
                    if (!PacketRefRadio.GetMACData(ref refRadioMAC, ref refBLEMAC))
                    {
                        LastFunctionErrorMessage = PacketRefRadio.LastErrorMessage;
                        LastFunctionStatus = PacketRefRadio.LastErrorCode;
                        UpdateStatusWindow(" Failed Getting MAC - " + LastFunctionErrorMessage + "\n", StatusType.failed);
                        return false;
                    }
                }
                else
                {
                    TextRefRadio = new TextBasedConsole();
                    try
                    {
                        TextRefRadio.Open(stationConfigure.GetInstrumentData(FixtureDefintions.InstrumentID.TestRadio).connection);
                        TextRefRadio.WriteLine("d mac", 1000);
                        if (!TextRefRadio.WaitForPrompt(1000, "REF>", out msgs))
                        {
                            LastFunctionErrorMessage = PacketRefRadio.LastErrorMessage;
                            LastFunctionStatus = PacketRefRadio.LastErrorCode;
                            UpdateStatusWindow(" Failed Getting MAC - " + LastFunctionErrorMessage + "\n", StatusType.failed);
                            return false;
                        }
                        refRadioMAC = msgs.Substring(10, 19);     // MAC=00:00:68:54:F5:xx:xx:xx
                        refRadioMAC = refRadioMAC.Replace(":","").Substring(0, 12);                // get rid of ':'s
                    }
                    catch (Exception)
                    {
                        LastFunctionErrorMessage = TextRefRadio.LastErrorMessage;
                        LastFunctionStatus = FixtureErrorCodes.InstrumentNotConnected;
                        UpdateStatusWindow(" Failed - " + LastFunctionErrorMessage + "\n", StatusType.failed);
                        return false;
                    }
                }
                UpdateStatusWindow(" Passed - MAC: " + refRadioMAC + "\n", StatusType.passed);
            }

            // if has a console port
            if (enableList.Exists(x => x.instrumentID == FixtureDefintions.InstrumentID.DUTConsole))
            {
                // init the dut serial port
                UpdateStatusWindow("    Checking DUT console port - ", StatusType.text);
                if (stationConfigure.GetChannelData(FixtureDefintions.CH_DUT_CONSOLE).type == FixtureDefintions.ChannelType.TextSerial)
                    IsDutConsolePacketBased = false;

                if (IsDutConsolePacketBased)
                {
                    PacketDutConsole = new PacketBasedConsole();
                    PacketDutConsole.CommandRecieveTimeoutMS = 100;
                    if (!PacketDutConsole.Open(stationConfigure.GetChannelInstrument(FixtureDefintions.CH_DUT_CONSOLE).connection, 115200))
                    {
                        LastFunctionErrorMessage = PacketDutConsole.LastErrorMessage;
                        LastFunctionStatus = PacketDutConsole.LastErrorCode;
                        UpdateStatusWindow(" Failed - " + LastFunctionErrorMessage + "\n", StatusType.failed);
                        return false;
                    }
                    PacketDutConsole.IsThisDUTConsole = true;
                    // on Kona based units once they are changed over, the port turns into a CU port.
                    // set it up so it can be used.
                    CUDutConsole = new CUCommications();
                }
                else            // text based, so it is a gateway
                {
                    TextDutConsole = new TextBasedConsole();
                    TextDutConsole.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(OnStatusUpdated);
                    try
                    {
                        TextDutConsole.Open(stationConfigure.GetChannelInstrument(FixtureDefintions.CH_DUT_CONSOLE).connection);
                        if  (TextDutConsole.LastErrorCode != 0)
                        {
                            LastFunctionErrorMessage = TextDutConsole.LastErrorMessage;
                            LastFunctionStatus = FixtureErrorCodes.InstrumentNotConnected;
                            UpdateStatusWindow(" Failed - " + LastFunctionErrorMessage + "\n", StatusType.failed);
                            return false;
                        }
                    }
                    catch (Exception)
                    {
                        LastFunctionErrorMessage = TextDutConsole.LastErrorMessage;
                        LastFunctionStatus = FixtureErrorCodes.InstrumentNotConnected;
                        UpdateStatusWindow(" Failed - " + LastFunctionErrorMessage + "\n", StatusType.failed);
                        return false;
                    }

                }
                UpdateStatusWindow(" Passed\n", StatusType.passed);
            }

            // setup the instruments
            UpdateStatusWindow("   Setting up Instruments - ", StatusType.text);
            labjackDAQ = null;
            a34970ADAQ = null;
            switchmateRelays = null;
            optomisticLEDDetector = null;
            flasherProgrammer = null;

            if (enableList.Count != 0)      // if there are instruments in the enable list
            {

                foreach (var item in enableList)
                {
                    if (item.instrumentID == FixtureDefintions.InstrumentID.LABJACK)
                    {
                        if (labjackDAQ == null)
                            labjackDAQ = new LabJackDAQInstrument(item);
                        if (labjackDAQ.LastFunctionStatus != 0)
                        {
                            UpdateStatusWindow(labjackDAQ.LastFunctionErrorMessage + "\n", StatusType.failed);
                            LastFunctionErrorMessage = labjackDAQ.LastFunctionErrorMessage;
                            LastFunctionStatus = FixtureErrorCodes.DAQBase - labjackDAQ.LastFunctionStatus;       // system codes are negitive
                            return false;
                        }
                    }
                    if (item.instrumentID == FixtureDefintions.InstrumentID.A34970A)
                    {

                        if (a34970ADAQ == null)
                            a34970ADAQ = new A34970ADAQInstrument(item.connection, item);
                        if (a34970ADAQ.LastFunctionStatus != 0)
                        {
                            UpdateStatusWindow(a34970ADAQ.LastFunctionErrorMessage + "\n", StatusType.failed);
                            LastFunctionErrorMessage = a34970ADAQ.LastFunctionErrorMessage;
                            LastFunctionStatus = FixtureErrorCodes.DAQBase - a34970ADAQ.LastFunctionStatus;       // system codes are negitive
                            return false;
                        }
                    }
                    if (item.instrumentID == FixtureDefintions.InstrumentID.SwitchMate)
                    {
                        if (switchmateRelays == null)
                            switchmateRelays = new SwitchMateInstrument(stationConfigure, item.connection);
                        if (switchmateRelays.LastFunctionStatus != 0)
                        {
                            UpdateStatusWindow(switchmateRelays.LastFunctionErrorMessage + "\n", StatusType.failed);
                            LastFunctionErrorMessage = switchmateRelays.LastFunctionErrorMessage;
                            LastFunctionStatus = FixtureErrorCodes.DAQBase - switchmateRelays.LastFunctionStatus;       // system codes are negitive
                            return false;
                        }
                    }
                    if (item.instrumentID == FixtureDefintions.InstrumentID.LEDDetector)
                        if (optomisticLEDDetector == null)
                            optomisticLEDDetector = new OptomisticInstrument(item.connection);

                    if (item.instrumentID == FixtureDefintions.InstrumentID.Flasher)
                        flasherProgrammer = new Flasher(item.connection);
                        
                }
            }

            // now connect
            if (labjackDAQ != null)
            {
                if (!labjackDAQ.Connect())
                {
                    UpdateStatusWindow(labjackDAQ.LastFunctionErrorMessage + "\n", StatusType.failed);
                    LastFunctionErrorMessage = labjackDAQ.LastFunctionErrorMessage;
                    LastFunctionStatus = FixtureErrorCodes.DAQBase - labjackDAQ.LastFunctionStatus;       // system codes are negitive
                    return false;
                }
                UpdateStatusWindow(" LabJack connected.\n");
            }
            if (a34970ADAQ != null)
            {
                if (!a34970ADAQ.Connect())
                {
                    UpdateStatusWindow(a34970ADAQ.LastFunctionErrorMessage + "\n", StatusType.failed);
                    LastFunctionErrorMessage = a34970ADAQ.LastFunctionErrorMessage;
                    LastFunctionStatus = FixtureErrorCodes.DAQBase - a34970ADAQ.LastFunctionStatus;       // system codes are negitive
                    return false;
                }
                UpdateStatusWindow("  A3490A connected.\n");
            }
            if (switchmateRelays != null)
            {
                if (!switchmateRelays.Connect())
                {
                    UpdateStatusWindow(switchmateRelays.LastFunctionErrorMessage + "\n", StatusType.failed);
                    LastFunctionErrorMessage = switchmateRelays.LastFunctionErrorMessage;
                    LastFunctionStatus = FixtureErrorCodes.DAQBase - switchmateRelays.LastFunctionStatus;       // system codes are negitive
                    return false;
                }
                UpdateStatusWindow("  Switch-Mate connected.\n");
            }
            if (optomisticLEDDetector != null)
            {
                if (!optomisticLEDDetector.Connect())
                {
                    UpdateStatusWindow(optomisticLEDDetector.LastFunctionErrorMessage + "\n", StatusType.failed);
                    LastFunctionErrorMessage = optomisticLEDDetector.LastFunctionErrorMessage;
                    LastFunctionStatus = FixtureErrorCodes.DAQBase - optomisticLEDDetector.LastFunctionStatus;       // system codes are negitive
                    return false;
                }
                UpdateStatusWindow("  Optomistic LED detector connected.\n");
            }

            // if has code flasher
            if (flasherProgrammer != null)
            {
                if ((stationConfigure.GetChannelInstrument(FixtureDefintions.CH_FLASHER).connection == "COM0") | (stationConfigure.GetChannelInstrument(FixtureDefintions.CH_FLASHER).connection == string.Empty))
                {
                    UpdateStatusWindow("   Flasher COM port not set. Will run in manual mode.\n");
                }
                else
                {
                    UpdateStatusWindow("   Setting up Flasher - ", StatusType.text);
                    if (!flasherProgrammer.Open())
                    {
                        LastFunctionErrorMessage = "Flasher error code: " + flasherProgrammer.LastErrorCode + ", " + flasherProgrammer.LastErrorMsg;
                        LastFunctionStatus = FixtureErrorCodes.FlasherBase - flasherProgrammer.LastErrorCode;        // 
                        UpdateStatusWindow(" Failed - " + LastFunctionErrorMessage + "\n", StatusType.failed);
                        return false;
                    }
                    flasherProgrammer.GetStatus();
                    flasherProgrammer.Close();
                    if (!flasherProgrammer.LastErrorStatus)
                    {
                        LastFunctionErrorMessage = "Flasher error code: " + flasherProgrammer.LastErrorCode + ", " + flasherProgrammer.LastErrorMsg;
                        LastFunctionStatus = FixtureErrorCodes.FlasherBase - flasherProgrammer.LastErrorCode;        // system errors are negitive
                        UpdateStatusWindow(" Failed - " + LastFunctionErrorMessage + "\n", StatusType.failed);
                        return false;
                    }
                    UpdateStatusWindow(" Passed\n", StatusType.passed);
                }
            }

            UpdateStatusWindow(" Passed\n", StatusType.passed);


            return true;
        }

        private void TextDutConsole_StatusUpdated(object sender, StatusUpdatedEventArgs e)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Reads a list of measurements. Currently only two measurment instruments.
        /// LabJack and A34970A.
        /// </summary>
        /// <remarks>
        /// LastFunctionStatus
        /// </remarks>
        /// <param name="channelNames">string names of the measurement</param>
        /// <param name="readValues">Returned double values</param>
        public void ReadMeasurements(ref PassData[] channelNames)
        {
            FixtureDefintions.Channel[] ljList = new FixtureDefintions.Channel[channelNames.Length];
            FixtureDefintions.Channel[] aList = new FixtureDefintions.Channel[channelNames.Length];
            FixtureDefintions.Channel single = new FixtureDefintions.Channel();
            int ljindex = 0;
            int aindex = 0;

            LastFunctionStatus = 0;
            LastFunctionErrorMessage = string.Empty;

            // make list for each instrument
            foreach (var item in channelNames)
            {
                if (stationConfigure.GetChannelInstrument(item.channelName).instrumentID == FixtureDefintions.InstrumentID.LABJACK)
                {
                    ljList[ljindex] = new FixtureDefintions.Channel();
                    ljList[ljindex++].name = item.channelName;
                }
                if (stationConfigure.GetChannelInstrument(item.channelName).instrumentID == FixtureDefintions.InstrumentID.A34970A)
                {
                    aList[aindex] = new FixtureDefintions.Channel();
                    aList[aindex++] = stationConfigure.GetChannelData(item.channelName);
                }
            }

            if (aList[0] != null)  // if anything in the agilent list
            {
                if (a34970ADAQ != null)
                {
                    a34970ADAQ.ReadListOfChannels(ref aList);
                    if (a34970ADAQ.LastFunctionStatus != 0)
                    {
                        LastFunctionStatus = FixtureErrorCodes.DAQBase - a34970ADAQ.LastFunctionStatus;  // any error will be a system error
                        LastFunctionErrorMessage = a34970ADAQ.LastFunctionErrorMessage;
                    }
                }
                else
                {
                    LastFunctionStatus = FixtureErrorCodes.InstrumentNotConnected;  // any error will be a system error
                    LastFunctionErrorMessage = "Can not do measurements. A34970A not connected";
                }
            }

            if (ljList[0] != null) // if anything in the labjack list
            {
                if (labjackDAQ != null)
                {
                    labjackDAQ.ReadListOfChannels(ref ljList);
                    if (labjackDAQ.LastFunctionStatus != 0)
                    {
                        LastFunctionStatus = FixtureErrorCodes.DAQBase - labjackDAQ.LastFunctionStatus;  // any error will be a system error
                        LastFunctionErrorMessage = labjackDAQ.LastFunctionErrorMessage;
                    }
                }
                else
                {
                    LastFunctionStatus = FixtureErrorCodes.InstrumentNotConnected;  // any error will be a system error
                    LastFunctionErrorMessage = "Can not do measurements. Labjack not connected";
                }
            }

            // now have to get data back into orginal array
            foreach (var item in channelNames)
            {
                if (ljindex != 0)       // labjack read something
                {
                    foreach (var ljitem in ljList)
                    {
                        if (ljitem.name == item.channelName)
                        {
                            item.returnedData = ljitem.lastReading;
                            break;
                        }
                    }
                }
                if (aindex != 0)
                {
                    foreach (var aitem in aList)
                    {
                        if (aitem.name == item.channelName)
                        {
                            item.returnedData = aitem.lastReading;
                            break;
                        }
                    }
                }
            }
            return;
        }


        /// <summary>
        /// Read a single measurment. Pass the name of the measurement.
        /// </summary>
        /// <remarks>
        /// LastFunctionStatus
        /// </remarks>
        /// <param name="channel"></param>
        /// <returns></returns>
        public double ReadOneMeasurement(string measurement)
        {
            PassData[] channels = new PassData[1];

            channels[0] = new PassData();
            channels[0].channelName = measurement;
            ReadMeasurements(ref channels);
            return channels[0].returnedData;
        }

        /// <summary>
        /// Will set the channel to the value passed.
        /// </summary>
        /// <remarks>
        /// Error codes
        ///     FixtureErrorCodes.InstrumentNotConnected(-6003) - no instruments defined
        ///     FixtureErrorCodes.ChannelNotDefined(-6001)  - channel is not in channel list
        /// </remarks>
        /// <param name="channel"></param>
        /// <param name="value"></param>
        public void SetOutput(string channel, double value)
        {
            FixtureDefintions.Instrument single = new FixtureDefintions.Instrument();
            LastFunctionStatus = 0;
            LastFunctionErrorMessage = string.Empty;

            if (stationConfigure.fixtureDefs.fixtureTypeList == null)
            {
                LastFunctionErrorMessage = "No output defined";
                LastFunctionStatus = FixtureErrorCodes.InstrumentNotConnected;
                return;
            }

            single = stationConfigure.GetChannelInstrument(channel);
            if (single == null)
            {
                LastFunctionErrorMessage = "No output instruments defined for channel " + channel;
                LastFunctionStatus = FixtureErrorCodes.ChannelNotDefined;
                return;         // this is no instruments defined
            }

            if (single.instrumentID == FixtureDefintions.InstrumentID.A34970A)
            {
                if (a34970ADAQ != null)
                {
                    a34970ADAQ.SetChannel(channel, value);
                    a34970ADAQ.GetLastError(ref LastFunctionErrorMessage, ref LastFunctionStatus);
                    if (LastFunctionStatus != 0)        // only generates system errors
                        LastFunctionStatus = FixtureErrorCodes.DAQBase - LastFunctionStatus;
                }
                else
                {
                    LastFunctionErrorMessage = "Can not set ouput. A34970A not connected.";
                    LastFunctionStatus = FixtureErrorCodes.InstrumentNotConnected;
                }
            }
            else if (single.instrumentID == FixtureDefintions.InstrumentID.LABJACK)
            {
                if (labjackDAQ != null)
                {
                    labjackDAQ.SetChannel(channel, value);
                    labjackDAQ.GetLastError(ref LastFunctionErrorMessage, ref LastFunctionStatus);
                    if (LastFunctionStatus != 0)        // only generates system errors
                        LastFunctionStatus = FixtureErrorCodes.DAQBase - LastFunctionStatus;
                }
                else
                {
                    LastFunctionErrorMessage = "Can not set ouput. Labjack not connected.";
                    LastFunctionStatus = FixtureErrorCodes.InstrumentNotConnected;
                }
            }
            else if (single.instrumentID == FixtureDefintions.InstrumentID.SwitchMate)
            {
                if (switchmateRelays != null)
                {
                    switchmateRelays.SetChannel(channel, value);
                    switchmateRelays.GetLastError(ref LastFunctionErrorMessage, ref LastFunctionStatus);
                    if (LastFunctionStatus != 0)        // only generates system errors
                        LastFunctionStatus = FixtureErrorCodes.DAQBase - LastFunctionStatus;
                }
                else
                {
                    LastFunctionErrorMessage = "Can not set ouput. Switch-Mate not connected.";
                    LastFunctionStatus = FixtureErrorCodes.InstrumentNotConnected;
                }
            }
            else
            {
                LastFunctionStatus = FixtureErrorCodes.InvalidInstrumentDefined;
                LastFunctionErrorMessage = "Instrument ID(" + single.instrumentID + ") not valid for SetOutput Function.";
            }
        }

        //--------------------------------------
        // specialized function calls
        //-------------------------------------

        /// <summary>
        /// Will see if a cover closed switch is defined
        /// </summary>
        /// <returns>true = channel is defined</returns>
        public bool DoesCoverClosedExist()
        {
            return stationConfigure.GetChannelInstrument(FixtureDefintions.CH_FIXTURE_LID_SWITCH) != null;
        }

        /// <summary>
        /// Returns the lid status
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <returns>true = cover closed</returns>
        public bool IsCoverClosed()
        {
            double value;
            LastFunctionStatus = 0;
            LastFunctionErrorMessage = string.Empty;

            value = ReadOneMeasurement(FixtureDefintions.CH_FIXTURE_LID_SWITCH);


            return (value < 1);
        }

        /// <summary>
        /// Will see if a Box Button is defined
        /// </summary>
        /// <returns>true = channel is defined</returns>
        public bool DoesBoxButtonExist()
        {
            return stationConfigure.GetChannelInstrument(FixtureDefintions.CH_OPERATOR_BUTTON) != null;
        }

        /// <summary>
        /// Returns the box button status
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <returns>true = button pushed</returns>
        public bool IsBoxButtonPushed()
        {
            double value;

            value = ReadOneMeasurement(FixtureDefintions.CH_OPERATOR_BUTTON);

            return (value < 1);
        }


        //--------------------------------------
        // ControlPIRSource
        /// <summary>
        /// Control the Test source for the PIR. Will delay ConfigPIRDetectorLightSource.onDelayMs
        /// when turned on and ConfigPIRDetectorLightSource.OffDelayMs when turned off.
        /// </summary>
        /// <param name="control"></param>
        /// <returns>Last error status</returns>
        public int ControlPIRSource(FixtureDefintions.PowerControl control)
        {
            LastFunctionStatus = 0;
            LastFunctionErrorMessage = string.Empty;

            SetOutput(FixtureDefintions.CH_PIR_SOURCE_CNTL, (double)control);

            return LastFunctionStatus;
        }


        /// <summary>
        /// Will turn power on and off to the dut. Will delay after set.
        /// </summary>
        /// <remarks>
        /// <param name="control 1=on 0=off"></param>
        /// <param name="aftersetdelayms">ms to wait after control</param>
        /// <returns>
        /// last error code
        /// </returns>
        public int ControlDUTPower(FixtureDefintions.PowerControl control, int aftersetdelayms)
        {
            LastFunctionStatus = 0;
            LastFunctionErrorMessage = string.Empty;

            if (stationConfigure.GetChannelData(FixtureDefintions.CH_DUT_POWER_CNTL) != null)
                SetOutput(FixtureDefintions.CH_DUT_POWER_CNTL, (double)control);    // sensors 
            if (stationConfigure.GetChannelData(FixtureDefintions.CH_DUT_POWER_CNTL1) != null)
                SetOutput(FixtureDefintions.CH_DUT_POWER_CNTL1, (double)control);   // gateway
            if (stationConfigure.GetChannelData(FixtureDefintions.CH_DUT_POWER_CNTL2) != null)
                SetOutput(FixtureDefintions.CH_DUT_POWER_CNTL2, (double)control);   // gateway
            if (stationConfigure.GetChannelData(FixtureDefintions.CH_DUT_POWER_CNTL3) != null)
                SetOutput(FixtureDefintions.CH_DUT_POWER_CNTL3, (double)control);   // gateway
            if (stationConfigure.GetChannelData(FixtureDefintions.CH_DUT_POWER_CNTL4) != null)
                SetOutput(FixtureDefintions.CH_DUT_POWER_CNTL4, (double)control);   // gateway
            Thread.Sleep(aftersetdelayms);

            if (LastFunctionStatus != 0)
                LastFunctionStatus = FixtureErrorCodes.DAQBase - LastFunctionStatus;     // always a system error if there is a error
            return LastFunctionStatus;
        }


        //---------------------------------------
        // SetAmbientSensorLED
        /// <summary>
        /// Set the Ambient to the voltage.
        /// </summary>
        /// <returns></returns>
        public int SetAmbientSensorLED(double voltage)
        {

            LastFunctionStatus = 0;
            LastFunctionErrorMessage = string.Empty;

            SetOutput(FixtureDefintions.CH_AMBIENT_DETECTOR_LED, voltage);

            return LastFunctionStatus;
        }


        //----------------------------------------
        // ReadTemperature
        //
        //  Read the temperature probe in C
        public double ReadTemperatureProbe()
        {
            double value = 0;

            LastFunctionStatus = 0;
            LastFunctionErrorMessage = string.Empty;

            value = ReadOneMeasurement(FixtureDefintions.CH_TEMPERTURE_PROBE);
            if (stationConfigure.ConfigTempSensorOutput == "C")
                value *= 10;
            return value;
        }


        //----------------------------------
        // Reads the LED Detector.
        //  Exceptions
        //      LabJack exceptions
        //
        // The following properties are set:
        //      double value - frequency, average of 10 period readings
        public int ReadLEDDetector(ref double freq, ref double dutyCycle)
        {
            bool usesLabJack = false;
            bool usesOptomistic = false;

            LastFunctionStatus = 0;
            LastFunctionErrorMessage = string.Empty;

            // Variables to satisfy certain method signatures
            double[] valuearray = new double[10];
            double[] valuearraydc = new double[10];


            // see if it is a Optomistic
            if (optomisticLEDDetector != null)
            {
                usesOptomistic = true;
            }

            if (!usesOptomistic & !usesLabJack)
            {
                LastFunctionErrorMessage = "LED Detector does not exist.";
                LastFunctionStatus = FixtureErrorCodes.InvalidChannelData;
                return LastFunctionStatus;
            }


            //            Thread.Sleep(100);
            freq = 0;
            for (int i = 0; i < 10; i++)
            {
                if (usesOptomistic)
                {
                    int retrycount = 0;
                    do      // this loop is to try to catch when a 0 comes back from the read.
                            // assuming that for some reason no reads are being done, so give a some 
                            // time.
                    {
                        if (optomisticLEDDetector.wavelength == 0)
                            Thread.Sleep(100);
                        valuearray[i] = optomisticLEDDetector.wavelength;
                        valuearraydc[i] = optomisticLEDDetector.intensity;
                    } while ((freq == 0) & (retrycount++ < 5));
                }
                else
                {
                    valuearray[i] = labjackDAQ.ReadChannel("LED_DETECTOR");
                }
            }

            freq = valuearray.Average();
            dutyCycle = valuearraydc.Average();
            return 0;
        }

        /// <summary>
        /// Will read the LED Detector and see if it is in the color range. If returns false,
        /// check LastFunctionErrorMessage and LastFunctionStatus to see if there was a error.
        /// </summary>
        /// <param name="ledColor"></param>
        /// <param name="readFreq"></param>
        /// <param name="outmsg"></param>
        /// <returns></returns>
        public bool IsLEDColorInrange(string checkColor, string freqRange, ref int readFreq, ref int readIntensity, ref string outmsg)
        {
            bool status = true;
            double freq = 0;
            int checkFreqLow = 0;
            int checkFreqHigh = 0;
            double intensity = 0;

            outmsg = string.Empty;
            readFreq = 0;
            if (freqRange == string.Empty)
            {
                status = false;
                LastFunctionStatus = FixtureErrorCodes.MissingInstrumentOption;
                LastFunctionErrorMessage = "LED color range for " + checkColor + " can not be empty.";
                return status;
            }
            checkFreqLow = Convert.ToInt32(freqRange.Substring(0, freqRange.IndexOf(',')));
            checkFreqHigh = Convert.ToInt32(freqRange.Substring(freqRange.IndexOf(',') + 1));


            if (ReadLEDDetector(ref freq, ref intensity) == 0)
            {
                readFreq = (int)freq;
                readIntensity = (int)intensity;
                if (((int)freq < checkFreqLow) || ((int)freq > checkFreqHigh))
                {
                    status = false;
                    outmsg = string.Format("Reading({0:N0}) for color {1} is outside the limits({2}-{3})",
                                            freq, checkColor, checkFreqHigh, checkFreqLow);
                }
            }
            else
                status = false;     // LastFunctionStatus and LastFunctionErrorMessage set by ReadLEDDetector

            return status;
        }

        /// <summary>
        /// Will read the LED Detector and see if it is in the color/int range.
        /// </summary>
        /// <param name="checkFreqLow">low freq limit</param>
        /// <param name="checkFreqHigh">high freq limit</param>
        /// <param name="checkIntHigh">high intensity limit</param>
        /// <param name="checkIntLow">low intensity limit</param>
        /// <param name="freqStatus">0 = within limit or not tested</param>
        /// <param name="intensityStatus">0 = within limit or not tested</param>
        /// <param name="readFreq">frequency read</param>
        /// <param name="readIntensity">intensity read</param>
        /// <param name="outmsg">error message</param>
        /// <returns>true = freq and intensity good. check LastFunctionStatus to see if there was a system error</returns>
        public bool IsLEDValuesInrange(int checkFreqLow, int checkFreqHigh, int checkIntLow, int checkIntHigh,
                    ref int freqStatus, ref int intensityStatus, ref int readFreq, ref int readIntensity, ref string outmsg)
        {
            bool status = true;
            double freq = 0;
            double intensity = 0;

            outmsg = string.Empty;
            readFreq = 0;
            freqStatus = 0;
            intensityStatus = 0;


            if (ReadLEDDetector(ref freq, ref intensity) == 0)
            {
                readFreq = (int)freq;
                readIntensity = (int)intensity;
                if (checkFreqLow != -1)         // -1 indicates that it is not to be tested
                {
                    if (((int)freq < checkFreqLow) || ((int)freq > checkFreqHigh))
                    {
                        status = false;
                        freqStatus = 1;
                        outmsg = string.Format("Reading({0:N0}) is outside the limits({1}-{2})",
                                                freq, checkFreqHigh, checkFreqLow);
                    }
                }
                if (checkIntLow != -1)         // -1 indicates that it is not to be tested
                {
                    if (((int)intensity < checkIntLow) || ((int)intensity > checkIntHigh))
                    {
                        status = false;
                        intensityStatus = 1;
                        outmsg = string.Format("Reading({0:N0}) is outside the limits({1}-{2})",
                                                intensity, checkIntHigh, checkIntLow);
                    }
                }
            }
            return status;
        }


        //-------------------------------------------------------
        // Class utilities
        //-------------------------------------------------------

        //public FixtureDefintions.fixtureTypes GetFixtureData(string fixtureName)
        //{
        //    return fixtureTypeList.Find(x => x.fixtureType == fixtureName);
        //}


        /// <summary>
        /// Will look for fixtureTypeDefs.xml file. If found, will add the defs
        /// to the fixDefs list. Will replace predefined if type matches predefined.
        /// </summary>
        /// <param name="fixDefs">current list of fixture definitions</param>
        //public void GetFixtureDefFile (ref List<FixtureInstrument.fixtureTypes> fixDefs)
        //{
        //    var path = System.IO.Path.GetDirectoryName(
        //        System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
        //    path = Path.Combine(path, "FixtureTypeDefs.xml");
        //    path = path.Substring(6);   // get rid of the 'file://' at the begining
            //if (File.Exists(path))
            //{
            //    FixtureInstrument.fixtureTypes readTypes = new FixtureInstrument.fixtureTypes();
            //    XDocument xdoc;
            //    xdoc = XDocument.Load(path);

            //    var xx = xdoc.Elements();

            //    foreach (var typeDefs in xx.Descendants("Fixture"))
            //    {
            //        readTypes.desc = string.Empty;
            //        readTypes.hasEnlightedInterface = false;     // enlighted interface, CU and 0-10V output
            //        readTypes.hasBLE = false;                    // has bluetooth

            //        foreach (var item in typeDefs.Elements())
            //        {
            //            if (item.Name.ToString().ToUpper() == "DESC")
            //                readTypes.desc = item.Value.ToString();
            //            if (item.Name.ToString().ToUpper() == "TYPE")
            //                readTypes.fixtureType = item.Value.ToString();
            //            if (item.Name.ToString().ToUpper() == "HASENLIGHTEDINTERFACE")
            //                if (item.Value.ToUpper() == "YES")
            //                    readTypes.hasEnlightedInterface = true;
            //            if (item.Name.ToString().ToUpper() == "HASBLE")
            //                if (item.Value.ToUpper() == "YES")
            //                    readTypes.hasBLE = true;
            //        }

            //        // see if this def is in the list alread
            //        bool found = false;
            //        for (int i = 0; i < fixDefs.Count; i++)
            //        {
            //            if (fixDefs[i].fixtureType.ToUpper() == readTypes.fixtureType.ToUpper())
            //                fixDefs[i] = readTypes;         // replace with what is in the file
            //        }
            //        if (!found)     // if not found in the list, it is new
            //            fixDefs.Add(readTypes);
            //    }
            //}
        //}

        //public bool IsInstrumentUsed(FixtureDefintions.InstrumentID id)
        //{
        //    if (stationConfigure.FixtureEquipment.channelDefs == null)
        //        return false;
        //    return stationConfigure.FixtureEquipment.channelDefs.Exists(x => x.instrumentID == id);
        //}

        //------------------------
        // events
        //  Use UpdateStatusWindow(msg) to display a message on the
        //  MainWindow status window.
        //------------------------
        public event EventHandler<StatusUpdatedEventArgs>
            StatusUpdated;

        protected virtual void
            OnStatusUpdated(object sender, StatusUpdatedEventArgs e)
        {
            if (StatusUpdated != null)
                StatusUpdated(this, e);
        }

        // call these functions to update the main status window
        private void UpdateStatusWindow(string msg)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            OnStatusUpdated(this, nuea);
        }
        private void UpdateStatusWindow(string msg, StatusType status)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            nuea.statusType = status;
            OnStatusUpdated(this, nuea);
        }


    }
}

