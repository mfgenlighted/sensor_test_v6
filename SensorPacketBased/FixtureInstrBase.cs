﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace FixtureInstrumentBase
{
    public abstract class FixtureInstrument
    {

        public string LastFunctionErrorMessage = string.Empty;              // if a function had a error, contains error message
        public int LastFunctionStatus = 0;    // status of the last function called

        public const int InsturmentError_NoError = 0;                       // 0
        public const int InsturmentError_CouldNotConnect = 1;               // could not connect
        public const int InstrumentError_ChannelAddInstOpen = 2;            // can not add a channel if the instrument is connected
        public const int InstrumentError_ChannelAdd = 3;                    // error trying to add channels
        public const int InstrumentError_RequestedChannelNotFound = 4;      // the channel name was not found in the setup list
        public const int InstrumentError_ReadError = 5;                     // error reading the channel
        public const int InstrumentError_WrongChannelType = 6;              // requested channel does not support type assked for
        public const int InstrumentError_NoChannelList = 7;                 // must have a channel list before connecting to instrument
        public const int InstrumentError_InvalidFunction = 8;               // instrument does not support this function
        public const int InstrumentError_InvalidParameter = 9;              // a parameter passed is invalid for this function
        public const int InstrumentError_NotConnected = 10;                 // instrurment is not connected
        public const int InstrumentError_ContactStatusReadbackWrong = 11;  // the readback of a set contact was wrong value
        public const int InsturmentError_VisaOpenError = 12;                // problem opening a Visa sesson
        public const int InstrumentError_NotCorrectInstrument = 13;         // instrument is not what expected
        public const int InstrumentError_NotCorrectCard = 14;               // the fuction asked for can not be done on this card
        public const int InstrumentError_MissingInstrumentOption = 15;      // options asked for in not installed on instrument
        public const int InsturmentError_VisaSendError = 16;                // error sending a VISA command
        public const int InsturmentError_VisaError = 17;                    // error instrument error occured
        public const int InstrumentError_MissingConfigData = 18;            // config data has not been read

        public void GetLastError(ref string msg, ref int code)
        {
            msg = LastFunctionErrorMessage;
            code = LastFunctionStatus;
        }



    }
}
