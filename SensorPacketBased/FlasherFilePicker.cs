﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SensorManufSY2
{
    public partial class FlasherFilePicker : Form
    {
        public string PickedFile = string.Empty;
        public FlasherFilePicker(List<string> filelist)
        {
            InitializeComponent();
            foreach (var item in filelist)
            {
                cbPickAFile.Items.Add(item);
            }
        }

        private void bDone_Click(object sender, EventArgs e)
        {
            PickedFile = cbPickAFile.SelectedItem.ToString();
            DialogResult = DialogResult.OK;
        }
    }
}
