﻿namespace SensorManufSY2
{
    partial class GatewayFixtureControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bDone = new System.Windows.Forms.Button();
            this.cbConnectConsol = new System.Windows.Forms.CheckBox();
            this.cbConnectPOE = new System.Windows.Forms.CheckBox();
            this.cbTestIndicator = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lb3p3V = new System.Windows.Forms.Label();
            this.lbOperatorButton = new System.Windows.Forms.Label();
            this.lbLidStatus = new System.Windows.Forms.Label();
            this.tbMac = new System.Windows.Forms.TextBox();
            this.tbSerialNumber = new System.Windows.Forms.TextBox();
            this.tbDUTTestIP = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tbModel = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbPartNumber = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbTargetIP = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.lbLEDWaveLenght = new System.Windows.Forms.Label();
            this.lbLEDInisity = new System.Windows.Forms.Label();
            this.rtbMonitor = new System.Windows.Forms.RichTextBox();
            this.bSetManufData = new System.Windows.Forms.Button();
            this.bReadVersionInfo = new System.Windows.Forms.Button();
            this.bStartLEDTest = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.tbRefRadioMAC = new System.Windows.Forms.TextBox();
            this.bDiagTest = new System.Windows.Forms.Button();
            this.bStartManufMode = new System.Windows.Forms.Button();
            this.bExitManufMode = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tbLEDColor = new System.Windows.Forms.TextBox();
            this.bReadAnalogData = new System.Windows.Forms.Button();
            this.cbEnableConsoleDisplay = new System.Windows.Forms.CheckBox();
            this.bReeableManufactureMode = new System.Windows.Forms.Button();
            this.nudLowFreq = new System.Windows.Forms.NumericUpDown();
            this.nudHighInt = new System.Windows.Forms.NumericUpDown();
            this.nudLowInt = new System.Windows.Forms.NumericUpDown();
            this.nudHighFreq = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.nudTimeout = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.cbLEDOn = new System.Windows.Forms.CheckBox();
            this.nudLQI = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.cbConnectEthernetTX = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.nudLowFreq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHighInt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLowInt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHighFreq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTimeout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLQI)).BeginInit();
            this.SuspendLayout();
            // 
            // bDone
            // 
            this.bDone.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bDone.Location = new System.Drawing.Point(388, 743);
            this.bDone.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bDone.Name = "bDone";
            this.bDone.Size = new System.Drawing.Size(112, 35);
            this.bDone.TabIndex = 0;
            this.bDone.Text = "Done";
            this.bDone.UseVisualStyleBackColor = true;
            // 
            // cbConnectConsol
            // 
            this.cbConnectConsol.AutoSize = true;
            this.cbConnectConsol.Location = new System.Drawing.Point(96, 125);
            this.cbConnectConsol.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbConnectConsol.Name = "cbConnectConsol";
            this.cbConnectConsol.Size = new System.Drawing.Size(157, 24);
            this.cbConnectConsol.TabIndex = 1;
            this.cbConnectConsol.Text = "Connect Console";
            this.cbConnectConsol.UseVisualStyleBackColor = true;
            this.cbConnectConsol.CheckedChanged += new System.EventHandler(this.cbConnectConsol_CheckedChanged);
            // 
            // cbConnectPOE
            // 
            this.cbConnectPOE.AutoSize = true;
            this.cbConnectPOE.Location = new System.Drawing.Point(96, 162);
            this.cbConnectPOE.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbConnectPOE.Name = "cbConnectPOE";
            this.cbConnectPOE.Size = new System.Drawing.Size(132, 24);
            this.cbConnectPOE.TabIndex = 2;
            this.cbConnectPOE.Text = "Connect POE";
            this.cbConnectPOE.UseVisualStyleBackColor = true;
            this.cbConnectPOE.CheckedChanged += new System.EventHandler(this.cbConnectPOE_CheckedChanged);
            // 
            // cbTestIndicator
            // 
            this.cbTestIndicator.AutoSize = true;
            this.cbTestIndicator.Location = new System.Drawing.Point(96, 223);
            this.cbTestIndicator.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbTestIndicator.Name = "cbTestIndicator";
            this.cbTestIndicator.Size = new System.Drawing.Size(132, 24);
            this.cbTestIndicator.TabIndex = 3;
            this.cbTestIndicator.Text = "Test Indicator";
            this.cbTestIndicator.UseVisualStyleBackColor = true;
            this.cbTestIndicator.CheckedChanged += new System.EventHandler(this.cbTestIndicator_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(142, 294);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "3.3V";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(63, 315);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(124, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Operator Button";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(105, 338);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Lid Status";
            // 
            // lb3p3V
            // 
            this.lb3p3V.AutoSize = true;
            this.lb3p3V.Location = new System.Drawing.Point(212, 294);
            this.lb3p3V.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb3p3V.Name = "lb3p3V";
            this.lb3p3V.Size = new System.Drawing.Size(29, 20);
            this.lb3p3V.TabIndex = 7;
            this.lb3p3V.Text = "0V";
            // 
            // lbOperatorButton
            // 
            this.lbOperatorButton.AutoSize = true;
            this.lbOperatorButton.Location = new System.Drawing.Point(212, 314);
            this.lbOperatorButton.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbOperatorButton.Name = "lbOperatorButton";
            this.lbOperatorButton.Size = new System.Drawing.Size(92, 20);
            this.lbOperatorButton.TabIndex = 8;
            this.lbOperatorButton.Text = "Not Pushed";
            // 
            // lbLidStatus
            // 
            this.lbLidStatus.AutoSize = true;
            this.lbLidStatus.Location = new System.Drawing.Point(212, 337);
            this.lbLidStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbLidStatus.Name = "lbLidStatus";
            this.lbLidStatus.Size = new System.Drawing.Size(48, 20);
            this.lbLidStatus.TabIndex = 9;
            this.lbLidStatus.Text = "Open";
            // 
            // tbMac
            // 
            this.tbMac.Location = new System.Drawing.Point(420, 125);
            this.tbMac.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbMac.Name = "tbMac";
            this.tbMac.Size = new System.Drawing.Size(148, 26);
            this.tbMac.TabIndex = 10;
            this.tbMac.Text = "6854f50002d9";
            // 
            // tbSerialNumber
            // 
            this.tbSerialNumber.Location = new System.Drawing.Point(420, 191);
            this.tbSerialNumber.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbSerialNumber.Name = "tbSerialNumber";
            this.tbSerialNumber.Size = new System.Drawing.Size(148, 26);
            this.tbSerialNumber.TabIndex = 11;
            this.tbSerialNumber.Text = "XE2W01112211111";
            // 
            // tbDUTTestIP
            // 
            this.tbDUTTestIP.Location = new System.Drawing.Point(424, 463);
            this.tbDUTTestIP.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbDUTTestIP.Name = "tbDUTTestIP";
            this.tbDUTTestIP.Size = new System.Drawing.Size(148, 26);
            this.tbDUTTestIP.TabIndex = 12;
            this.tbDUTTestIP.Text = "192.168.0.10";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(420, 100);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 20);
            this.label4.TabIndex = 13;
            this.label4.Text = "MAC";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(424, 438);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 20);
            this.label5.TabIndex = 14;
            this.label5.Text = "DUT Test IP";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(420, 166);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(109, 20);
            this.label6.TabIndex = 15;
            this.label6.Text = "Serial Number";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(420, 229);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 20);
            this.label7.TabIndex = 16;
            this.label7.Text = "Model";
            // 
            // tbModel
            // 
            this.tbModel.Location = new System.Drawing.Point(420, 254);
            this.tbModel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbModel.Name = "tbModel";
            this.tbModel.Size = new System.Drawing.Size(148, 26);
            this.tbModel.TabIndex = 17;
            this.tbModel.Text = "GW-2-01";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(420, 297);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(98, 20);
            this.label8.TabIndex = 18;
            this.label8.Text = "Part Number";
            // 
            // tbPartNumber
            // 
            this.tbPartNumber.Location = new System.Drawing.Point(420, 322);
            this.tbPartNumber.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbPartNumber.Name = "tbPartNumber";
            this.tbPartNumber.Size = new System.Drawing.Size(148, 26);
            this.tbPartNumber.TabIndex = 19;
            this.tbPartNumber.Text = "01-00000-01";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(424, 508);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 20);
            this.label9.TabIndex = 20;
            this.label9.Text = "Target IP";
            // 
            // tbTargetIP
            // 
            this.tbTargetIP.Location = new System.Drawing.Point(424, 532);
            this.tbTargetIP.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbTargetIP.Name = "tbTargetIP";
            this.tbTargetIP.Size = new System.Drawing.Size(148, 26);
            this.tbTargetIP.TabIndex = 21;
            this.tbTargetIP.Text = "192.168.0.3";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(80, 458);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(107, 20);
            this.label10.TabIndex = 22;
            this.label10.Text = "LED Detector";
            // 
            // lbLEDWaveLenght
            // 
            this.lbLEDWaveLenght.AutoSize = true;
            this.lbLEDWaveLenght.Location = new System.Drawing.Point(222, 458);
            this.lbLEDWaveLenght.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbLEDWaveLenght.Name = "lbLEDWaveLenght";
            this.lbLEDWaveLenght.Size = new System.Drawing.Size(18, 20);
            this.lbLEDWaveLenght.TabIndex = 23;
            this.lbLEDWaveLenght.Text = "0";
            // 
            // lbLEDInisity
            // 
            this.lbLEDInisity.AutoSize = true;
            this.lbLEDInisity.Location = new System.Drawing.Point(222, 480);
            this.lbLEDInisity.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbLEDInisity.Name = "lbLEDInisity";
            this.lbLEDInisity.Size = new System.Drawing.Size(18, 20);
            this.lbLEDInisity.TabIndex = 24;
            this.lbLEDInisity.Text = "0";
            // 
            // rtbMonitor
            // 
            this.rtbMonitor.Location = new System.Drawing.Point(868, 58);
            this.rtbMonitor.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rtbMonitor.Name = "rtbMonitor";
            this.rtbMonitor.Size = new System.Drawing.Size(952, 750);
            this.rtbMonitor.TabIndex = 25;
            this.rtbMonitor.Text = "";
            // 
            // bSetManufData
            // 
            this.bSetManufData.Location = new System.Drawing.Point(666, 118);
            this.bSetManufData.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bSetManufData.Name = "bSetManufData";
            this.bSetManufData.Size = new System.Drawing.Size(180, 35);
            this.bSetManufData.TabIndex = 26;
            this.bSetManufData.Text = "Set Manufacture data";
            this.bSetManufData.UseVisualStyleBackColor = true;
            this.bSetManufData.Click += new System.EventHandler(this.bSetManufData_Click);
            // 
            // bReadVersionInfo
            // 
            this.bReadVersionInfo.Location = new System.Drawing.Point(666, 166);
            this.bReadVersionInfo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bReadVersionInfo.Name = "bReadVersionInfo";
            this.bReadVersionInfo.Size = new System.Drawing.Size(180, 35);
            this.bReadVersionInfo.TabIndex = 27;
            this.bReadVersionInfo.Text = "Read Version Info";
            this.bReadVersionInfo.UseVisualStyleBackColor = true;
            this.bReadVersionInfo.Click += new System.EventHandler(this.bReadVersionInfo_Click);
            // 
            // bStartLEDTest
            // 
            this.bStartLEDTest.Location = new System.Drawing.Point(666, 229);
            this.bStartLEDTest.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bStartLEDTest.Name = "bStartLEDTest";
            this.bStartLEDTest.Size = new System.Drawing.Size(180, 35);
            this.bStartLEDTest.TabIndex = 28;
            this.bStartLEDTest.Text = "Start LED Test";
            this.bStartLEDTest.UseVisualStyleBackColor = true;
            this.bStartLEDTest.Click += new System.EventHandler(this.bStartLEDTest_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(429, 574);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(120, 20);
            this.label11.TabIndex = 31;
            this.label11.Text = "Ref Radio MAC";
            // 
            // tbRefRadioMAC
            // 
            this.tbRefRadioMAC.Location = new System.Drawing.Point(424, 600);
            this.tbRefRadioMAC.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbRefRadioMAC.Name = "tbRefRadioMAC";
            this.tbRefRadioMAC.Size = new System.Drawing.Size(148, 26);
            this.tbRefRadioMAC.TabIndex = 32;
            this.tbRefRadioMAC.Text = "6854F5000000";
            // 
            // bDiagTest
            // 
            this.bDiagTest.Location = new System.Drawing.Point(666, 398);
            this.bDiagTest.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bDiagTest.Name = "bDiagTest";
            this.bDiagTest.Size = new System.Drawing.Size(180, 35);
            this.bDiagTest.TabIndex = 33;
            this.bDiagTest.Text = "Diag Test";
            this.bDiagTest.UseVisualStyleBackColor = true;
            this.bDiagTest.Click += new System.EventHandler(this.bDiagTest_Click);
            // 
            // bStartManufMode
            // 
            this.bStartManufMode.Location = new System.Drawing.Point(666, 45);
            this.bStartManufMode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bStartManufMode.Name = "bStartManufMode";
            this.bStartManufMode.Size = new System.Drawing.Size(180, 35);
            this.bStartManufMode.TabIndex = 34;
            this.bStartManufMode.Text = "Start Manuf mode";
            this.bStartManufMode.UseVisualStyleBackColor = true;
            this.bStartManufMode.Click += new System.EventHandler(this.bStartManufMode_Click);
            // 
            // bExitManufMode
            // 
            this.bExitManufMode.Location = new System.Drawing.Point(666, 595);
            this.bExitManufMode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bExitManufMode.Name = "bExitManufMode";
            this.bExitManufMode.Size = new System.Drawing.Size(180, 35);
            this.bExitManufMode.TabIndex = 35;
            this.bExitManufMode.Text = "Exit Manuf Mode";
            this.bExitManufMode.UseVisualStyleBackColor = true;
            this.bExitManufMode.Click += new System.EventHandler(this.bExitManufMode_Click);
            // 
            // tbLEDColor
            // 
            this.tbLEDColor.Location = new System.Drawing.Point(320, 458);
            this.tbLEDColor.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbLEDColor.Name = "tbLEDColor";
            this.tbLEDColor.ReadOnly = true;
            this.tbLEDColor.Size = new System.Drawing.Size(28, 26);
            this.tbLEDColor.TabIndex = 36;
            // 
            // bReadAnalogData
            // 
            this.bReadAnalogData.Location = new System.Drawing.Point(126, 363);
            this.bReadAnalogData.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bReadAnalogData.Name = "bReadAnalogData";
            this.bReadAnalogData.Size = new System.Drawing.Size(158, 35);
            this.bReadAnalogData.TabIndex = 37;
            this.bReadAnalogData.Text = "Start Analog Read";
            this.bReadAnalogData.UseVisualStyleBackColor = true;
            this.bReadAnalogData.Click += new System.EventHandler(this.bReadAnalogData_Click);
            // 
            // cbEnableConsoleDisplay
            // 
            this.cbEnableConsoleDisplay.AutoSize = true;
            this.cbEnableConsoleDisplay.Location = new System.Drawing.Point(886, 822);
            this.cbEnableConsoleDisplay.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbEnableConsoleDisplay.Name = "cbEnableConsoleDisplay";
            this.cbEnableConsoleDisplay.Size = new System.Drawing.Size(199, 24);
            this.cbEnableConsoleDisplay.TabIndex = 38;
            this.cbEnableConsoleDisplay.Text = "Enable Console display";
            this.cbEnableConsoleDisplay.UseVisualStyleBackColor = true;
            this.cbEnableConsoleDisplay.CheckedChanged += new System.EventHandler(this.cbEnableConsoleDisplay_CheckedChanged);
            // 
            // bReeableManufactureMode
            // 
            this.bReeableManufactureMode.Location = new System.Drawing.Point(626, 652);
            this.bReeableManufactureMode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bReeableManufactureMode.Name = "bReeableManufactureMode";
            this.bReeableManufactureMode.Size = new System.Drawing.Size(234, 35);
            this.bReeableManufactureMode.TabIndex = 39;
            this.bReeableManufactureMode.Text = "Reenable Manufacture Mode";
            this.bReeableManufactureMode.UseVisualStyleBackColor = true;
            this.bReeableManufactureMode.Click += new System.EventHandler(this.bReeableManufactureMode_Click);
            // 
            // nudLowFreq
            // 
            this.nudLowFreq.Location = new System.Drawing.Point(714, 268);
            this.nudLowFreq.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.nudLowFreq.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nudLowFreq.Name = "nudLowFreq";
            this.nudLowFreq.Size = new System.Drawing.Size(132, 26);
            this.nudLowFreq.TabIndex = 40;
            this.nudLowFreq.Value = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            // 
            // nudHighInt
            // 
            this.nudHighInt.Location = new System.Drawing.Point(714, 354);
            this.nudHighInt.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.nudHighInt.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nudHighInt.Name = "nudHighInt";
            this.nudHighInt.Size = new System.Drawing.Size(132, 26);
            this.nudHighInt.TabIndex = 41;
            this.nudHighInt.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // nudLowInt
            // 
            this.nudLowInt.Location = new System.Drawing.Point(714, 325);
            this.nudLowInt.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.nudLowInt.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nudLowInt.Name = "nudLowInt";
            this.nudLowInt.Size = new System.Drawing.Size(132, 26);
            this.nudLowInt.TabIndex = 42;
            this.nudLowInt.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // nudHighFreq
            // 
            this.nudHighFreq.Location = new System.Drawing.Point(714, 297);
            this.nudHighFreq.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.nudHighFreq.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nudHighFreq.Name = "nudHighFreq";
            this.nudHighFreq.Size = new System.Drawing.Size(132, 26);
            this.nudHighFreq.TabIndex = 43;
            this.nudHighFreq.Value = new decimal(new int[] {
            6000,
            0,
            0,
            0});
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(639, 278);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(64, 20);
            this.label12.TabIndex = 44;
            this.label12.Text = "low freq";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(612, 358);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(92, 20);
            this.label13.TabIndex = 45;
            this.label13.Text = "high intesity";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(618, 329);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(85, 20);
            this.label14.TabIndex = 46;
            this.label14.Text = "low intesity";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(633, 302);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(71, 20);
            this.label15.TabIndex = 47;
            this.label15.Text = "high freq";
            // 
            // nudTimeout
            // 
            this.nudTimeout.Location = new System.Drawing.Point(714, 78);
            this.nudTimeout.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.nudTimeout.Name = "nudTimeout";
            this.nudTimeout.Size = new System.Drawing.Size(132, 26);
            this.nudTimeout.TabIndex = 48;
            this.nudTimeout.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(602, 82);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(105, 20);
            this.label16.TabIndex = 49;
            this.label16.Text = "timeout(secs)";
            // 
            // cbLEDOn
            // 
            this.cbLEDOn.AutoSize = true;
            this.cbLEDOn.Location = new System.Drawing.Point(84, 500);
            this.cbLEDOn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbLEDOn.Name = "cbLEDOn";
            this.cbLEDOn.Size = new System.Drawing.Size(92, 24);
            this.cbLEDOn.TabIndex = 50;
            this.cbLEDOn.Text = "LED On";
            this.cbLEDOn.UseVisualStyleBackColor = true;
            this.cbLEDOn.CheckedChanged += new System.EventHandler(this.cbLEDOn_CheckedChanged);
            // 
            // nudLQI
            // 
            this.nudLQI.Location = new System.Drawing.Point(714, 448);
            this.nudLQI.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.nudLQI.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudLQI.Name = "nudLQI";
            this.nudLQI.Size = new System.Drawing.Size(132, 26);
            this.nudLQI.TabIndex = 51;
            this.nudLQI.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(621, 458);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(35, 20);
            this.label17.TabIndex = 52;
            this.label17.Text = "LQI";
            // 
            // cbConnectEthernetTX
            // 
            this.cbConnectEthernetTX.AutoSize = true;
            this.cbConnectEthernetTX.Location = new System.Drawing.Point(96, 188);
            this.cbConnectEthernetTX.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbConnectEthernetTX.Name = "cbConnectEthernetTX";
            this.cbConnectEthernetTX.Size = new System.Drawing.Size(185, 24);
            this.cbConnectEthernetTX.TabIndex = 53;
            this.cbConnectEthernetTX.Text = "Connect Ethernet TX";
            this.cbConnectEthernetTX.UseVisualStyleBackColor = true;
            this.cbConnectEthernetTX.CheckedChanged += new System.EventHandler(this.cbConnectEthernetTX_CheckedChanged);
            // 
            // GatewayFixtureControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1890, 866);
            this.ControlBox = false;
            this.Controls.Add(this.cbConnectEthernetTX);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.nudLQI);
            this.Controls.Add(this.cbLEDOn);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.nudTimeout);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.nudHighFreq);
            this.Controls.Add(this.nudLowInt);
            this.Controls.Add(this.nudHighInt);
            this.Controls.Add(this.nudLowFreq);
            this.Controls.Add(this.bReeableManufactureMode);
            this.Controls.Add(this.cbEnableConsoleDisplay);
            this.Controls.Add(this.bReadAnalogData);
            this.Controls.Add(this.tbLEDColor);
            this.Controls.Add(this.bExitManufMode);
            this.Controls.Add(this.bStartManufMode);
            this.Controls.Add(this.bDiagTest);
            this.Controls.Add(this.tbRefRadioMAC);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.bStartLEDTest);
            this.Controls.Add(this.bReadVersionInfo);
            this.Controls.Add(this.bSetManufData);
            this.Controls.Add(this.rtbMonitor);
            this.Controls.Add(this.lbLEDInisity);
            this.Controls.Add(this.lbLEDWaveLenght);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tbTargetIP);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tbPartNumber);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tbModel);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbDUTTestIP);
            this.Controls.Add(this.tbSerialNumber);
            this.Controls.Add(this.tbMac);
            this.Controls.Add(this.lbLidStatus);
            this.Controls.Add(this.lbOperatorButton);
            this.Controls.Add(this.lb3p3V);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbTestIndicator);
            this.Controls.Add(this.cbConnectPOE);
            this.Controls.Add(this.cbConnectConsol);
            this.Controls.Add(this.bDone);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "GatewayFixtureControl";
            this.Text = "Gateway Fixture Control";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.GatewayFixtureControl_FormClosed);
            this.Load += new System.EventHandler(this.GatewayFixtureControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudLowFreq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHighInt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLowInt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHighFreq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTimeout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLQI)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bDone;
        private System.Windows.Forms.CheckBox cbConnectConsol;
        private System.Windows.Forms.CheckBox cbConnectPOE;
        private System.Windows.Forms.CheckBox cbTestIndicator;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lb3p3V;
        private System.Windows.Forms.Label lbOperatorButton;
        private System.Windows.Forms.Label lbLidStatus;
        private System.Windows.Forms.TextBox tbMac;
        private System.Windows.Forms.TextBox tbSerialNumber;
        private System.Windows.Forms.TextBox tbDUTTestIP;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbModel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbPartNumber;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbTargetIP;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lbLEDWaveLenght;
        private System.Windows.Forms.Label lbLEDInisity;
        private System.Windows.Forms.RichTextBox rtbMonitor;
        private System.Windows.Forms.Button bSetManufData;
        private System.Windows.Forms.Button bReadVersionInfo;
        private System.Windows.Forms.Button bStartLEDTest;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbRefRadioMAC;
        private System.Windows.Forms.Button bDiagTest;
        private System.Windows.Forms.Button bStartManufMode;
        private System.Windows.Forms.Button bExitManufMode;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TextBox tbLEDColor;
        private System.Windows.Forms.Button bReadAnalogData;
        private System.Windows.Forms.CheckBox cbEnableConsoleDisplay;
        private System.Windows.Forms.Button bReeableManufactureMode;
        private System.Windows.Forms.NumericUpDown nudLowFreq;
        private System.Windows.Forms.NumericUpDown nudHighInt;
        private System.Windows.Forms.NumericUpDown nudLowInt;
        private System.Windows.Forms.NumericUpDown nudHighFreq;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown nudTimeout;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox cbLEDOn;
        private System.Windows.Forms.NumericUpDown nudLQI;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.CheckBox cbConnectEthernetTX;
    }
}