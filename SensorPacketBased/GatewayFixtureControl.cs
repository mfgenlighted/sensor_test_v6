﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

using GatewayProdTests;

namespace SensorManufSY2
{
    public partial class GatewayFixtureControl : Form
    {
        public delegate void SetTextCallback(string text);
        public delegate void SetTextTypeCallback(string text, StatusType type);
        public delegate void updateFixtureReadings(string lid, string pushButton, string temp);
        public delegate void updateLEDReadings(int intensity, int freq, Color backColor);

        ThreadStart updateScreenThreadStart;
        Thread updateScreenThread;
        ThreadStart updateLEDScreenThreadStart;
        Thread updateLEDScreenThread;
        bool runThreads = true;
        bool runAnalogThread = false;


        StationConfigure sconfig;
        FixtureFunctions fixture;

        public GatewayFixtureControl(StationConfigure config)
        {
            InitializeComponent();
            sconfig = config;
        }

        private void GatewayFixtureControl_Load(object sender, EventArgs e)
        {
            fixture = new FixtureFunctions(sconfig);
            fixture.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(sc_StatusUpdated);

            if (!fixture.InitFixture())
            {
                MessageBox.Show("Problem initializing fixture. " + fixture.LastFunctionErrorMessage);
                return;
            }

            fixture.StatusUpdated += sc_StatusUpdated;
            AddToMonitorWindow("Fixture initialize.\n");

            updateScreenThreadStart = new ThreadStart(StartUpdateScreen);
            updateScreenThread = new Thread(updateScreenThreadStart);
            updateScreenThread.Start();

            if (sconfig.GetChannelData(FixtureDefintions.CH_LED_DETECTOR) != null)
            {
                updateLEDScreenThreadStart = new ThreadStart(StartLEDUpdateScreen);
                updateLEDScreenThread = new Thread(updateLEDScreenThreadStart);
                updateLEDScreenThread.Start();
            }

            tbRefRadioMAC.Text = fixture.RefRadioMAC;
        }


        //-----------------------------------
        // fixture reading screen updating routines
        //
        private void StartUpdateScreen()
        {

            updateFixtureReadings UpdateFixtureScreenDel = new updateFixtureReadings(UpdateFixtureReadings);

            while (runThreads)
            {
                if (runAnalogThread)
                    this.lbLidStatus.BeginInvoke(UpdateFixtureScreenDel, fixture.IsCoverClosed() ? "CLOSED" : "OPEN",
                                                                    fixture.IsBoxButtonPushed() ? "Pushed" : "Not Pushed",
                                                                    fixture.ReadOneMeasurement(FixtureDefintions.CH_V3P3_VOLTAGE).ToString("N1"));
                Thread.Sleep(500);
            }
        }

        private void StartLEDUpdateScreen()
        {
            int intensity = 0;
            int freq = 0;
            string msg = string.Empty;
            Color backColor;

            updateLEDReadings UpdateLEDScreenDel = new updateLEDReadings(UpdateLEDReadings);
            while (runThreads)
            {
                if (fixture.IsLEDColorInrange("Red", sconfig.ConfigLedDetector.ColorRED, ref freq, ref intensity, ref msg))
                {
                    backColor = Color.Red;
                }
                else if (fixture.IsLEDColorInrange("Green", sconfig.ConfigLedDetector.ColorGREEN, ref freq, ref intensity, ref msg))
                {
                    backColor = Color.Green;
                }
                else if (fixture.IsLEDColorInrange("Blue", sconfig.ConfigLedDetector.ColorBLUE, ref freq, ref intensity, ref msg))
                {
                    backColor = Color.Blue;
                }
                else
                {
                    backColor = Color.Gray;
                }
                this.lbLEDWaveLenght.BeginInvoke(UpdateLEDScreenDel, intensity, freq, backColor);
                Thread.Sleep(500);
            }
        }

        private void UpdateFixtureReadings(string lid, string pushButton, string temp)
        {
            lbLidStatus.Text = lid;
            lbOperatorButton.Text = pushButton;
            lb3p3V.Text = temp;
        }

        private void UpdateLEDReadings(int intensity, int freq, Color backColor)
        {
            tbLEDColor.BackColor = backColor;
            lbLEDWaveLenght.Text = freq.ToString("N0");
            lbLEDInisity.Text = intensity.ToString("N0");
        }

        private void bReadAnalogData_Click(object sender, EventArgs e)
        {
            Button mybutton = sender as Button;
            if (mybutton.Text.Contains("Start"))
            {
                runAnalogThread = true;
                mybutton.Text = "Stop";
            }
            else
            {
                runAnalogThread = false;
                mybutton.Text = "Start";
            }
        }

        //----------------------------------
        // utils
        //

        public void sc_StatusUpdated(object sender, StatusUpdatedEventArgs e)
        {
            this.AddToMonitorWindow(e.statusText, e.statusType);
        }

        private void AddToMonitorWindow(string msg)
        {
            if (this.rtbMonitor.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(AddToMonitorWindow);
                this.Invoke(d, new object[] { msg });
            }
            else
            {
                this.rtbMonitor.AppendText(msg);
                this.rtbMonitor.ScrollToCaret();
                this.rtbMonitor.Update();
            }
        }
        /// <summary>
        /// Adds text to the status screen;
        /// </summary>
        /// <param name="msg"></param>
        public void AddToMonitorWindow(string msg, StatusType type)
        {
            if (this.rtbMonitor.InvokeRequired)
            {
                SetTextTypeCallback d = new SetTextTypeCallback(AddToMonitorWindow);
                this.Invoke(d, new object[] { msg, type });
            }
            else
            {
                switch (type)
                {
                    case StatusType.text:
                        this.rtbMonitor.SelectionColor = Color.Black;
                        break;
                    case StatusType.passed:
                        this.rtbMonitor.SelectionColor = Color.Green;
                        break;
                    case StatusType.failed:
                        this.rtbMonitor.SelectionColor = Color.Red;
                        break;
                    case StatusType.serialSend:
                        this.rtbMonitor.SelectionColor = Color.Blue;
                        break;
                    case StatusType.serialRcv:
                        this.rtbMonitor.SelectionColor = Color.Gray;
                        break;
                    case StatusType.prompt:
                        this.rtbMonitor.SelectionColor = Color.Purple;
                        break;
                    case StatusType.cuSend:
                        this.rtbMonitor.SelectionColor = Color.Aqua;
                        break;
                    case StatusType.cuRcv:
                        this.rtbMonitor.SelectionColor = Color.RosyBrown;
                        break;
                    case StatusType.snifferSend:
                        this.rtbMonitor.SelectionColor = Color.Aqua;
                        break;
                    case StatusType.snifferRcv:
                        this.rtbMonitor.SelectionColor = Color.RosyBrown;
                        break;
                    default:
                        this.rtbMonitor.SelectionColor = Color.Black;
                        break;
                }
                this.rtbMonitor.AppendText(msg);
                this.rtbMonitor.ScrollToCaret();
                this.rtbMonitor.Update();
            }
        }

        private void GatewayFixtureControl_FormClosed(object sender, FormClosedEventArgs e)
        {
            runThreads = false;
            if (updateScreenThread != null)
            {
                while (updateScreenThread.IsAlive)
                {
                    Thread.Sleep(0);
                }
                updateScreenThread.Join();
            }
            if (updateLEDScreenThread != null)
            {
                while (updateLEDScreenThread.IsAlive)
                {
                    Thread.Sleep(0);
                }
                updateLEDScreenThread.Join();
            }
            fixture.Dispose();

        }

        private void cbConnectConsol_CheckedChanged(object sender, EventArgs e)
        {
            if (cbConnectConsol.Checked)
            {
                fixture.SetOutput(FixtureDefintions.CH_DUT_CONSOLE_CONNECT, (double)FixtureDefintions.PowerControl.ON);
            }
            else
            {
                fixture.SetOutput(FixtureDefintions.CH_DUT_CONSOLE_CONNECT, (double)FixtureDefintions.PowerControl.OFF);
            }
        }

        private void cbConnectPOE_CheckedChanged(object sender, EventArgs e)
        {
            if (cbConnectPOE.Checked)
            {
                fixture.SetOutput(FixtureDefintions.CH_DUT_POWER_CNTL2, (double)FixtureDefintions.PowerControl.ON);
                fixture.SetOutput(FixtureDefintions.CH_DUT_POWER_CNTL4, (double)FixtureDefintions.PowerControl.ON);
            }
            else
            {
                fixture.SetOutput(FixtureDefintions.CH_DUT_POWER_CNTL2, (double)FixtureDefintions.PowerControl.OFF);
                fixture.SetOutput(FixtureDefintions.CH_DUT_POWER_CNTL4, (double)FixtureDefintions.PowerControl.OFF);
            }
        }
        private void cbConnectEthernetTX_CheckedChanged(object sender, EventArgs e)
        {
            if (cbConnectEthernetTX.Checked)
            {
                fixture.SetOutput(FixtureDefintions.CH_DUT_POWER_CNTL1, (double)FixtureDefintions.PowerControl.ON);
                fixture.SetOutput(FixtureDefintions.CH_DUT_POWER_CNTL3, (double)FixtureDefintions.PowerControl.ON);
            }
            else
            {
                fixture.SetOutput(FixtureDefintions.CH_DUT_POWER_CNTL1, (double)FixtureDefintions.PowerControl.OFF);
                fixture.SetOutput(FixtureDefintions.CH_DUT_POWER_CNTL3, (double)FixtureDefintions.PowerControl.OFF);
            }
        }

        private void cbTestIndicator_CheckedChanged(object sender, EventArgs e)
        {
            if (cbTestIndicator.Checked)
            {
                fixture.SetOutput(FixtureDefintions.CH_DUT_TESTING_LED, (double)FixtureDefintions.PowerControl.ON);
            }
            else
            {
                fixture.SetOutput(FixtureDefintions.CH_DUT_TESTING_LED, (double)FixtureDefintions.PowerControl.OFF);
            }
        }

        private void bStartManufMode_Click(object sender, EventArgs e)
        {
            string outmsg = string.Empty;
            string orgmesg = bStartManufMode.Text;
            LimitsDatabase.LimitsData.stepsItem testConfig = new LimitsDatabase.LimitsData.stepsItem();
            LimitsDatabase.LimitsData.parametersItem param = new LimitsDatabase.LimitsData.parametersItem();
            List<LimitsDatabase.LimitsData.parametersItem> paramList = new List<LimitsDatabase.LimitsData.parametersItem>();

            if (!cbConnectConsol.Checked)
            {
                MessageBox.Show("Need to connect the console before running this test.");
                return;
            }

            AddToMonitorWindow("Boot up and Start Manufacture Mode  --  Start\n");

            bStartManufMode.Text = "Looking";
            param.name = "TimeoutSecs";
            param.value = nudTimeout.Value.ToString();
            paramList.Add(param);
            param.name = "TurnOnPower";
            param.value = "YES";
            paramList.Add(param);
            testConfig.parameters = paramList;
            testConfig.name = "Boot up";

            // turn off power just in case it is on
            fixture.SetOutput(FixtureDefintions.CH_DUT_POWER_CNTL1, (double)FixtureDefintions.PowerControl.OFF);
            fixture.SetOutput(FixtureDefintions.CH_DUT_POWER_CNTL3, (double)FixtureDefintions.PowerControl.OFF);
            cbConnectEthernetTX.Checked = true;

            GatewayTests gwtests = new GatewayTests(fixture);
            gwtests.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(sc_StatusUpdated);

            fixture.TextDutConsole.ClearBuffers();
            if (gwtests.GWTest_BootUp(testConfig, null))
            {
                testConfig.name = "Start Manufacture Mode";
                gwtests.GWTest_StartMFGMode(testConfig, null);
            }
            bStartManufMode.Text = orgmesg;
            AddToMonitorWindow("Boot up and Start Manufacture Mode  --  Done\n");
        }

        private void cbEnableConsoleDisplay_CheckedChanged(object sender, EventArgs e)
        {
            if (cbEnableConsoleDisplay.Checked)
            {
                fixture.TextDutConsole.DisplayRcvChar = true;
                fixture.TextDutConsole.DisplaySendChar = true;
            }
            else
            {
                fixture.TextDutConsole.DisplayRcvChar = false;
                fixture.TextDutConsole.DisplaySendChar = false;
            }
        }

        private void bReadVersionInfo_Click(object sender, EventArgs e)
        {
            string aver = string.Empty;
            string gwver = string.Empty;
            string kver = string.Empty;

            if (!cbConnectConsol.Checked)
            {
                MessageBox.Show("Need to connect the console before running this test.");
                return;
            }

            GatewayTests gwtests = new GatewayTests(fixture);

            AddToMonitorWindow("Get Versions -- Start\n");
            gwtests.GetVersion(out gwver, out aver, out kver);

            AddToMonitorWindow("\tAtemel: " + aver + "  Gateway: " + gwver + "  Kernal: " + kver + "\nGet Versions -- Done\n");
        }

        private void bStartLEDTest_Click(object sender, EventArgs e)
        {
            string outmsg = string.Empty;
            string orgmesg = bStartLEDTest.Text;
            LimitsDatabase.LimitsData.stepsItem testConfig = new LimitsDatabase.LimitsData.stepsItem();
            LimitsDatabase.LimitsData.limitsItem limit = new LimitsDatabase.LimitsData.limitsItem();
            List<LimitsDatabase.LimitsData.limitsItem> limitList = new List<LimitsDatabase.LimitsData.limitsItem>();

            if (!cbConnectConsol.Checked)
            {
                MessageBox.Show("Need to connect the console before running this test.");
                return;
            }

            AddToMonitorWindow("LED Test -- Start\n");

            GatewayTests gwtests = new GatewayTests(fixture);

            limit.name = "Freq";
            limit.value1 = nudLowFreq.Value.ToString();
            limit.value2 = nudHighFreq.Value.ToString();
            limit.operation = "GELE";
            limit.type = "RANGE";
            limitList.Add(limit);
            limit.name = "Intesity";
            limit.value1 = nudLowInt.Value.ToString();
            limit.value2 = nudHighInt.Value.ToString();
            limitList.Add(limit);
            testConfig.limits = limitList;

            if (gwtests.GWTest_LEDTest(testConfig, null))
                AddToMonitorWindow("\tPASSED\n", StatusType.passed);
            else
                AddToMonitorWindow("\tFAILED\n", StatusType.failed);
            AddToMonitorWindow("\tData: " + gwtests.LastErrorMessage + "\nLED Test  -- Done\n");

        }


        private void cbLEDOn_CheckedChanged(object sender, EventArgs e)
        {
            if (!cbConnectConsol.Checked)
            {
                MessageBox.Show("Need to connect the console before running this test.");
                return;
            }

            if (cbLEDOn.Checked)
                fixture.TextDutConsole.WriteLine("LED-TEST-START", 1000);
            else
                fixture.TextDutConsole.WriteLine("PASS", 1000);
        }

        private void bSetManufData_Click(object sender, EventArgs e)
        {
            GatewayTests gwtests = new GatewayTests(fixture);
            LimitsDatabase.LimitsData.stepsItem testConfig = new LimitsDatabase.LimitsData.stepsItem();

            if (!cbConnectConsol.Checked)
            {
                MessageBox.Show("Need to connect the console before running this test.");
                return;
            }

            AddToMonitorWindow("Set Manufacture Data  --  Start\n");

            gwtests.GWMac = tbMac.Text;
            gwtests.GWPartNumber = tbPartNumber.Text;
            gwtests.GWSerialNumber = tbSerialNumber.Text;
            gwtests.GWTestIPAddress = tbDUTTestIP.Text;
            gwtests.GWModel = tbModel.Text;

            try
            {
                testConfig.name = "Set Manufacture Data";
                gwtests.GWTest_ProgramManufactureData(testConfig, null);
            }
            catch (Exception ex)
            {
                AddToMonitorWindow("\tFAIL: " + ex.Message + "\n", StatusType.failed);
            }
            AddToMonitorWindow("Set Manufacture Data  --  Done\n");
        }

        private void bDiagTest_Click(object sender, EventArgs e)
        {
            LimitsDatabase.LimitsData.stepsItem testConfig = new LimitsDatabase.LimitsData.stepsItem();
            LimitsDatabase.LimitsData.limitsItem limit = new LimitsDatabase.LimitsData.limitsItem();
            List<LimitsDatabase.LimitsData.limitsItem> limitList = new List<LimitsDatabase.LimitsData.limitsItem>();
            GatewayTests gwtests = new GatewayTests(fixture);

            if (!cbConnectConsol.Checked)
            {
                MessageBox.Show("Need to connect the console before running this test.");
                return;
            }

            AddToMonitorWindow("Diag Test  -- Start\n");

            limit.name = "LQI";
            limit.value1 = nudLowFreq.Value.ToString();
            limit.value2 = nudHighFreq.Value.ToString();
            limit.operation = "GE";
            limit.type = "VALUE";
            limitList.Add(limit);
            testConfig.limits = limitList;
            if (tbRefRadioMAC.Text.Contains(':'))
                tbRefRadioMAC.Text.Replace(":", "");
            gwtests.GWTargetMac = tbRefRadioMAC.Text;
            gwtests.GWTargetIPAddress = tbTargetIP.Text;

            try
            {
                if (gwtests.GWTest_DiagTest(testConfig, null))
                    AddToMonitorWindow("\tPASS\n", StatusType.passed);
                else
                    AddToMonitorWindow("\tFAIL: " + gwtests.LastErrorMessage + "\n", StatusType.failed);
            }
            catch (Exception ex)
            {
                AddToMonitorWindow("\tFAIL: " + "\n" + ex.Message, StatusType.failed);
            }

            AddToMonitorWindow("Diag Test  -- Done\n");
        }

        private void bReeableManufactureMode_Click(object sender, EventArgs e)
        {
            GatewayTests gwtests = new GatewayTests(fixture);
            if (!cbConnectConsol.Checked)
            {
                MessageBox.Show("Need to connect the console before running this test.");
                return;
            }

            AddToMonitorWindow("Restore Manufacturing mode  -- Start\n");
            if (gwtests.GWFunction_ResetToManufactureMode())
                AddToMonitorWindow("\tPass\n");
            else
                AddToMonitorWindow("\tFail - " + gwtests.LastErrorMessage + "\n");
            AddToMonitorWindow("Restore Manufacturing mode  -- Done\n");
        }

        private void bExitManufMode_Click(object sender, EventArgs e)
        {
            GatewayTests gwtests = new GatewayTests(fixture);
            if (!cbConnectConsol.Checked)
            {
                MessageBox.Show("Need to connect the console before running this test.");
                return;
            }

            AddToMonitorWindow("Exit Manufacturing Mode  --  Start\n");
            if (gwtests.GWTest_ExitManufactureMode(null, null))
                AddToMonitorWindow("\tPass\n");
            else
                AddToMonitorWindow("\tFail - " + gwtests.LastErrorMessage + "\n");
            AddToMonitorWindow("Exit Manufacturing Mode  --  Done\n");
        }

    }
}
