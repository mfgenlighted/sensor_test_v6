﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Threading;
using System.Text.RegularExpressions;

using SensorManufSY2;

namespace GatewayProdTests
{
    public partial class GatewayTests : IDisposable
    {
        //-----------------------
        // Properties
        //-----------------------
        public string LastErrorMessage = string.Empty;          // last error message issued
        public int LastErrorCode = 0;                           // error code of last error

            //------------------------------
            // GWMac
            //  Will read or set the current MAC. When setting it it will
            //  first test to verify that it is a valid MAC.
            //  Expects a format of xxxxxxxxxxxx ( no colons)
            //      length = 12 characters
            //      Hex digits only
            //  If the format is incorrect, a expection will be thrown.
        public string GWMac
        {
            get { return dutMac; }
            set
            {
                if (value == null) return;
                Regex r = new Regex(@"^[/0-9A-F\r\n]+$");
                value = value.ToUpper();
                if ((value.Length != 12) || !r.Match(value).Success)
                    throw new Exception("Invalid MAC address");
                dutMac = value;
            }
        }

        //------------------------------
        // GWTargetMac
        //  Will read or set the target MAC. When setting it it will
        //  first test to verify that it is a valid MAC.
        //  Expects a format of xxxxxxxxxxxx ( no colons)
        //      length = 12 characters
        //      Hex digits only
        //  If the format is incorrect, a expection will be thrown.
        public string GWTargetMac
        {
            get { return targetMac; }
            set
            {
                if (value == null) return;
                Regex r = new Regex(@"^[0-9A-F\r\n]+$");
                value = value.ToUpper();
                if ((value.Length != 12) || !r.Match(value).Success)
                    throw new Exception("Invalid MAC address");
                targetMac = value;
            }
        }

        //----------------------------
        // GWPartNumber
        //  Save the gateway part number.
        //  The format of the PN is:
        //      format:   xx-xxxxx-xx
        //                          x = digit number
        public string GWPartNumber
        {
            get { return dutAsmNumber; }
            set
            {
                if (value == null) return;
                Regex newfmt = new Regex(@"^\d\d-\d\d\d\d\d-\d\d$");        // new format
                value = value.ToUpper();                                    // make sure in upper case
                if (!newfmt.Match(value).Success)                           // if not the correct format
                    throw new Exception("Invalid Part Number format");
                dutAsmNumber = value;
            }
        }

            //------------------------------
            // GWSerialNumber
            //  Save the gateway serial number
            //  The format of the serial number can be one of two formats:
            //  New format:
            //      FPPPVVYYWWSSSSS
            //      The only thing checked is the lenght(=15)
        public string GWSerialNumber
        {
            get { return dutSerialNumber; }
            set
            {
                if (value == null) return;
                value = value.ToUpper();
                if (value.Length != 15)       // if failed
                    throw new Exception("Invalid Serial Number format");
                dutSerialNumber = value;
            }
        }

            //-----------------------------
            // GWTestIPAddress
            //  Save the gateway test IP adrress
            //  The format is:
            //      ddd.ddd.ddd.ddd
        public string GWTestIPAddress
        {
            get { return configGWIPAddress; }
            set { configGWIPAddress = value; }
        }

            //-----------------------------
            // GWTargetIPAddress
            //  Save the gateway target IP adrress
            //  The format is:
            //      ddd.ddd.ddd.ddd
        public string GWTargetIPAddress
        {
            get { return configTargetIPAddress; }
            set { configTargetIPAddress = value; }
        }

        //-------------------------------
        // GWModel
        //  Save the gateway Model Number
        //
        public string GWModel
        {
            get { return dutModel; }
            set { dutModel = value; }
        }


        public FixtureFunctions ptrfixture = null;                     // pointer to the fixture functions
        string configGWIPAddress = string.Empty;
        string configTargetIPAddress = string.Empty;
        string dutMac = string.Empty;
        string targetMac = string.Empty;
        string dutAsmNumber = string.Empty;
        string dutSerialNumber = string.Empty;
        string dutModel = string.Empty;

        //------------------------
        // constructor/dispose
        //------------------------

        public GatewayTests(FixtureFunctions fixture)
        {
            ptrfixture = fixture;

        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~GatewayTests() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion


        //------------------------
        // events
        //  Use UpdateStatusWindow(msg) to display a message on the
        //  MainWindow status window.
        //------------------------
        public event EventHandler<StatusUpdatedEventArgs>
            StatusUpdated;

        protected virtual void
            OnStatusUpdated(StatusUpdatedEventArgs e)
        {
            if (StatusUpdated != null)
                StatusUpdated(this, e);
        }

        // call these functions to update the main status window
        private void UpdateStatusWindow(string msg)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            OnStatusUpdated(nuea);
        }
        private void UpdateStatusWindow(string msg, StatusType status)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            nuea.statusType = status;
            OnStatusUpdated(nuea);
        }


        //----------------------------------------
        // Gateway tests - top level
        //---------------------------------------

        /// <summary>
        /// Connect the power and serial ports(if configured to) to the DUT and wait for the boot up message.
        /// </summary>
        /// <remarks>
        /// Parameters
        ///     TimeoutSecs - max number of seconds to wait for the boot up message. Default = 20 sec
        ///     TurnOnPower - YES=control the power and console connection. Default=YES
        /// </remarks>
        /// <param name="testConfig"></param>
        /// <param name="testDB"></param>
        /// <returns></returns>
        public bool GWTest_BootUp(LimitsDatabase.LimitsData.stepsItem testConfig, FixtureDatabase.ResultData testDB)
        {
            bool turnOnPower = true;                    // defines if the test will turn on power and connect console
            int maxBootUpTime = 20;                     // max number of seconds to wait for boot message
            string msg = string.Empty;

            LastErrorMessage = string.Empty;
            LastErrorCode = 0;

            if (testDB != null)
                testDB.CreateStepRecord(testConfig.name, testConfig.step_number);

            UpdateStatusWindow("Starting test '" + testConfig.name + "'\n");
            if ((msg = testConfig.GetParameterValue("TimeoutSecs")) != string.Empty)
            {
                if (!Int32.TryParse(msg, out maxBootUpTime))
                {
                    LastErrorCode = TestStationErrorCodes.ErrorCodes.System.ParameterFormatError;
                    LastErrorMessage = "TimeoutSecs can only be a interger. Found:" + msg;
                    if (testDB != null)
                        testDB.AddStepResult(FixtureDatabase.ResultData.TEST_FAILED, LastErrorMessage, LastErrorCode);
                    UpdateStatusWindow("\tFail: " + LastErrorMessage + "\n", StatusType.failed);
                    return false;
                }
            }
            if ((msg = testConfig.GetParameterValue("TurnOnPower")) != string.Empty)
            {
                if (msg.ToUpper().Contains("NO"))
                    turnOnPower = false;
            }

            // see if power control is enabled
            if (turnOnPower)
            {
                UpdateStatusWindow("\tTurning on power\n");
                ptrfixture.SetOutput(FixtureDefintions.CH_DUT_POWER_CNTL1, 1);
                if (ptrfixture.LastFunctionStatus != 0)     // just check for error the first time
                {
                    LastErrorCode = ptrfixture.LastFunctionStatus;
                    LastErrorMessage = "Error turning on DUT. " + ptrfixture.LastFunctionErrorMessage;
                    if (testDB != null)
                        testDB.AddStepResult(FixtureDatabase.ResultData.TEST_FAILED, LastErrorMessage, LastErrorCode);
                    UpdateStatusWindow("\tFail: " + LastErrorMessage + "\n", StatusType.failed);
                    return false;
                }
                ptrfixture.SetOutput(FixtureDefintions.CH_DUT_POWER_CNTL3, 1);
                ptrfixture.SetOutput(FixtureDefintions.CH_DUT_TESTING_LED, 1);

            }

            // now for the test
            UpdateStatusWindow("\tWaiting for bootup\n");
            WaitForBootUp(maxBootUpTime * 1000, ref msg);       // sets LastErrorxxx

            if (LastErrorCode != 0)
                UpdateStatusWindow("\tFail: " + LastErrorMessage + "\n", StatusType.failed);
            else
                UpdateStatusWindow("\tPass\n", StatusType.passed);
            if (testDB != null)
                testDB.AddStepResult((LastErrorCode == 0) ? FixtureDatabase.ResultData.TEST_PASSED : FixtureDatabase.ResultData.TEST_FAILED, LastErrorMessage, LastErrorCode);

            return (LastErrorCode == 0);
        }

        /// <summary>
        /// Read a voltage and test to the limits.
        /// </summary>
        /// <remarks>
        /// parameter 'VoltageToTest' has the Instrument channel to read
        /// limits 'VoltageLimits' has the limits to test to
        /// </remarks>
        /// <param name="testConfig"></param>
        /// <param name="testDB"></param>
        /// <returns></returns>
        public bool GWTest_CheckVoltage(LimitsDatabase.LimitsData.stepsItem testConfig, FixtureDatabase.ResultData testDB)
        {
            bool status = true;
            string voltageToTest = string.Empty;
            double readVoltage = 0;
            string outmsg = string.Empty;

            LastErrorCode = 0;
            LastErrorMessage = string.Empty;

            if (testDB != null)
                testDB.CreateStepRecord(testConfig.name, testConfig.step_number);

            //if ((voltageToTest = testConfig.GetParameterValue("VoltageToTest")) == string.Empty)
            //{
            //    LastErrorCode = TestStationErrorCodes.ErrorCodes.System.MissingParameter;
            //    LastErrorMessage = "VoltageToTest parameter is missing.";
            //    testDB.AddStepResult(FixtureDatabase.ResultData.TEST_FAILED, LastErrorMessage, LastErrorCode);
            //    return false;
            //}

            readVoltage = ptrfixture.ReadOneMeasurement(FixtureDefintions.CH_V3P3_VOLTAGE);
            if (ptrfixture.LastFunctionStatus != 0)
            {
                LastErrorCode = ptrfixture.LastFunctionStatus;
                LastErrorMessage = "Error reading voltage " + voltageToTest + ". " + ptrfixture.LastFunctionErrorMessage;
                testDB.AddStepResult(FixtureDatabase.ResultData.TEST_FAILED, LastErrorMessage, LastErrorCode);
                return false;
            }

            try
            {
                status = CheckResultValue(readVoltage, testConfig.GetLimitValues("DUT_3p3V"), ref outmsg);
                if (!status)
                {
                    LastErrorMessage = outmsg;
                    LastErrorCode = TestStationErrorCodes.ErrorCodes.DUT.ValueOutsideOfLimit;
                }
                LimitsDatabase.LimitsData.limitsItem limitdata = testConfig.GetLimitValues("DUT_3p3V");
                testDB.AddStepResult(voltageToTest, status ? FixtureDatabase.ResultData.TEST_PASSED : FixtureDatabase.ResultData.TEST_FAILED,
                                        limitdata.operation, Convert.ToDouble(limitdata.value1), Convert.ToDouble(limitdata.value2),
                                        readVoltage, "V", "N2", LastErrorMessage, LastErrorCode);
            }
            catch (Exception ex)
            {
                LastErrorCode = TestStationErrorCodes.ErrorCodes.System.InvalidLimitOperation;
                LastErrorMessage = "Error checking limits. " + ex.Message;
                testDB.AddStepResult(FixtureDatabase.ResultData.TEST_FAILED, LastErrorMessage, LastErrorCode);
                status = false;
            }

            return status;
        }

        /// <summary>
        /// Send the START command and wait for MFG READY.
        /// </summary>
        /// <param name="testConfig"></param>
        /// <param name="testDB"></param>
        /// <returns></returns>
        public bool GWTest_StartMFGMode(LimitsDatabase.LimitsData.stepsItem testConfig, FixtureDatabase.ResultData testDB)
        {
            bool status = true;
            int timeoutsec = 1;
            string msg = string.Empty;

            UpdateStatusWindow("Starting test: '" + testConfig.name + "'\n");

            if (testDB != null)
                testDB.CreateStepRecord(testConfig.name, testConfig.step_number);

            if ((msg = testConfig.GetParameterValue("TimeoutSecs")) != string.Empty)
            {
                if (!Int32.TryParse(msg, out timeoutsec))
                {
                    LastErrorCode = TestStationErrorCodes.ErrorCodes.System.ParameterFormatError;
                    LastErrorMessage = "TimeoutSecs can only be a interger. Found:" + msg;
                    if (testDB != null)
                        testDB.AddStepResult(FixtureDatabase.ResultData.TEST_FAILED, LastErrorMessage, LastErrorCode);
                    UpdateStatusWindow("Failed: " + LastErrorMessage + "\n", StatusType.failed);
                    return false;
                }
            }
            ptrfixture.TextDutConsole.ClearBuffers();
            UpdateStatusWindow("\tIssue 'START' command\n");
            if (!ptrfixture.TextDutConsole.WriteLine("START", 2000))
            {
                if (testDB != null)
                    testDB.AddStepResult(FixtureDatabase.ResultData.TEST_FAILED, LastErrorMessage, LastErrorCode);
                UpdateStatusWindow("Failed: " + LastErrorMessage + "\n", StatusType.failed);
                LastErrorCode = ptrfixture.LastFunctionStatus;
                LastErrorMessage = ptrfixture.LastFunctionErrorMessage;
                return false;
            }
            UpdateStatusWindow("\tWaiting for 'MFG READY'\n");
            if (!ptrfixture.TextDutConsole.WaitForPrompt(timeoutsec * 1000, "MFG READY", out msg))
            {
                if (testDB != null)
                    testDB.AddStepResult(FixtureDatabase.ResultData.TEST_FAILED, LastErrorMessage, LastErrorCode);
                UpdateStatusWindow("Failed: " + LastErrorMessage + "\n", StatusType.failed);
                LastErrorCode = ptrfixture.LastFunctionStatus;
                LastErrorMessage = ptrfixture.LastFunctionErrorMessage;
                return false;
            }

            if (testDB != null)
                testDB.AddStepResult(FixtureDatabase.ResultData.TEST_PASSED, "", 0);

            UpdateStatusWindow("\tPassed\n", StatusType.passed);

            return status;
        }

        /// <summary>
        /// Program the gateway manufacturing data. GWMAC, GWPartNumber, GWSerialNumber, and GWTestIPAddress
        /// must be set before calling.
        /// </summary>
        /// <param name="testConfig">step configure</param>
        /// <param name="testDB">test database pointer</param>
        /// <returns></returns>
        public bool GWTest_ProgramManufactureData(LimitsDatabase.LimitsData.stepsItem testConfig, FixtureDatabase.ResultData testDB)
        {
            bool status = true;

            LastErrorMessage = string.Empty;
            LastErrorCode = 0;

            UpdateStatusWindow("Start test: " + testConfig.name + "\n");
            if (testDB != null)
                testDB.CreateStepRecord(testConfig.name, testConfig.step_number);

            if ((GWMac == null) ||
                (GWPartNumber == null) ||
                (GWSerialNumber == null) ||
                (GWTestIPAddress == null))
            {
                LastErrorCode = TestStationErrorCodes.ErrorCodes.System.MissingParameter;
                LastErrorMessage = "Error setting manufacturing data. One of the following properties not set. GWMAC, GWPartNumber, GWSerialNumber, or GWTestIPAddress.";
                if (testDB != null)
                    testDB.AddStepResult(FixtureDatabase.ResultData.TEST_FAILED, LastErrorMessage, LastErrorCode);
                UpdateStatusWindow("\tFail: " + LastErrorMessage + "\n");
                return false;
            }

            status = SetManufactureData(GWMac, GWTestIPAddress, GWModel, GWPartNumber, GWSerialNumber);

            if (testDB != null)
                testDB.AddStepResult(status ? FixtureDatabase.ResultData.TEST_PASSED:FixtureDatabase.ResultData.TEST_FAILED, LastErrorMessage, LastErrorCode);

            if (status)
                UpdateStatusWindow("\tPass\n");
            else
            {
                UpdateStatusWindow("\tFail: " + LastErrorMessage + "\n");
            }

            return status;
        }

        /// <summary>
        /// Verify the firmware versions. At least one of the AtmelVer, GWVer, or Kernal must be defined.
        /// </summary>
        /// <param name="testConfig"></param>
        /// <param name="testDB"></param>
        /// <returns></returns>
        public bool GWTest_VerifyFirmware(LimitsDatabase.LimitsData.stepsItem testConfig, FixtureDatabase.ResultData testDB)
        {
            bool status = true;
            string atmelVer = string.Empty;
            string readAtmelVer = string.Empty;
            string gwVer = string.Empty;
            string readGwVer = string.Empty;
            string kernalVer = string.Empty;
            string readKernalVer = string.Empty;

            LastErrorCode = 0;
            LastErrorMessage = string.Empty;
            if (testDB != null)
                testDB.CreateStepRecord(testConfig.name, testConfig.step_number);

            atmelVer = testConfig.GetLimitValues("AtmelVer").value1;
            gwVer = testConfig.GetLimitValues("GWVer").value1;
            kernalVer = testConfig.GetLimitValues("Kernel").value1;
            if ((atmelVer == string.Empty) & (gwVer == string.Empty) & (kernalVer == string.Empty))
            {
                LastErrorCode = TestStationErrorCodes.ErrorCodes.System.MissingParameter;
                LastErrorMessage = "At least one of the limits(AtmeVer, GWVer, or Kernal) must be defined";
                if (testDB != null)
                    testDB.AddStepResult(FixtureDatabase.ResultData.TEST_FAILED, LastErrorMessage, LastErrorCode);
                return false;
            }

            status = GetVersion(out readGwVer, out readAtmelVer, out readKernalVer);
            if (!status)
            {
                if (testDB != null)
                    testDB.AddStepResult(FixtureDatabase.ResultData.TEST_FAILED, LastErrorMessage, LastErrorCode);
                return false;
            }

            if (atmelVer != string.Empty)
            {
                if (!readAtmelVer.Contains(atmelVer))        // do a Contains just in case there are extra spaces
                {
                    status = false;
                    LastErrorCode = TestStationErrorCodes.ErrorCodes.DUT.ValueOutsideOfLimit;
                    LastErrorMessage = "Atmel version fail. Read " + readAtmelVer + ". ";
                }
            }
            if (gwVer != string.Empty)
            {
                if ( !readGwVer.Contains(gwVer))        // do a Contains just in case there are extra spaces
                {
                    status = false;
                    LastErrorCode = TestStationErrorCodes.ErrorCodes.DUT.ValueOutsideOfLimit;
                    LastErrorMessage = "GW version fail. Read " + readGwVer + ". ";
                }
            }
            if (kernalVer != string.Empty)        // do a Contains just in case there are extra spaces
            {
                if (!readKernalVer.Contains(kernalVer))
                {
                    status = false;
                    LastErrorCode = TestStationErrorCodes.ErrorCodes.DUT.ValueOutsideOfLimit;
                    LastErrorMessage = "Kernal version fail. Read " + readKernalVer + ". ";
                }
            }

            if (testDB != null)
                testDB.AddStepResult(status ? FixtureDatabase.ResultData.TEST_PASSED : FixtureDatabase.ResultData.TEST_FAILED, LastErrorMessage, LastErrorCode);
            return status;
        }


        public bool GWTest_LEDTest(LimitsDatabase.LimitsData.stepsItem testConfig, FixtureDatabase.ResultData testDB)
        {
            bool status = true;
            LimitsDatabase.LimitsData.limitsItem freq = new LimitsDatabase.LimitsData.limitsItem();
            LimitsDatabase.LimitsData.limitsItem intensity = new LimitsDatabase.LimitsData.limitsItem();
            int readFreq = 0, readIntensity = 0;
            int freqStatus = 0, intensityStatus = 0;

            LastErrorCode = 0;
            LastErrorMessage = string.Empty;

            if (testDB != null)
                testDB.CreateStepRecord(testConfig.name, testConfig.step_number);

            if (testConfig.DoesLimitExist("Freq"))
                freq = testConfig.GetLimitValues("Freq");
            else
            {
                freq.value1 = "-1";           // indicates not to test
                freq.value2 = "-1";
            }
            if (testConfig.DoesLimitExist("Intesity"))
                intensity = testConfig.GetLimitValues("Intesity");
            else
            {
                intensity.value1 = "-1";
                intensity.value2 = "-1";
            }

            if ((freq.value1 == "-1") & (intensity.value1 == "-1"))
            {
                LastErrorCode = TestStationErrorCodes.ErrorCodes.System.MissingParameter;
                LastErrorMessage = "Must have at least one test defined. Freq or/and Intensity.";
                if (testDB != null)
                    testDB.AddStepResult(FixtureDatabase.ResultData.TEST_FAILED, LastErrorMessage, LastErrorCode);
                return false;
            }
            status = DoLEDTest(out readFreq, out freqStatus, out readIntensity, out intensityStatus, Convert.ToInt32(freq.value2), Convert.ToInt32(freq.value1), Convert.ToInt32(intensity.value2), Convert.ToInt32(intensity.value1));

            if (testDB != null)
            {
                if (freq.value1 != "-1")
                    testDB.AddStepResult("LEDFreq", (freqStatus == 0) ? FixtureDatabase.ResultData.TEST_PASSED : FixtureDatabase.ResultData.TEST_FAILED,
                                        freq.operation, Convert.ToDouble(freq.value1), Convert.ToDouble(freq.value2),
                                        readFreq, "hz", "N0", LastErrorMessage, LastErrorCode);
                if (intensity.value1 != "-1")
                    testDB.AddStepResult("LEDIntensity", (intensityStatus == 0) ? FixtureDatabase.ResultData.TEST_PASSED : FixtureDatabase.ResultData.TEST_FAILED,
                                        intensity.operation, Convert.ToDouble(intensity.value1), Convert.ToDouble(intensity.value2),
                                        readIntensity, "", "N0", LastErrorMessage, LastErrorCode);
            }
            else            // if no database, return the results in LastErrorMessage
            {
                LastErrorMessage = string.Format("Freq: {0}  Intesity: {1}", readFreq, readIntensity);
            }
            return status;
        }

        public bool GWTest_DiagTest(LimitsDatabase.LimitsData.stepsItem testConfig, FixtureDatabase.ResultData testDB)
        {
            bool status = true;
            LimitsDatabase.LimitsData.limitsItem lqiLimit = new LimitsDatabase.LimitsData.limitsItem();
            bool radioResult = false;
            bool netResult = false;
            bool cfcardResult = false;
            bool spidiagResult = false;
            bool radioLQIResult = false;
            int radioLQIValue = 0;
            LastErrorCode = 0;
            LastErrorMessage = string.Empty;

            if (testDB != null)
                testDB.CreateStepRecord(testConfig.name, testConfig.step_number);

            if (GWTargetIPAddress == string.Empty)
            {
                LastErrorCode = TestStationErrorCodes.ErrorCodes.System.MissingParameter;
                LastErrorMessage = "Target IP address is not set. Property GWTargetIPAddress must be set.";
                if (testDB != null)
                    testDB.AddStepResult(FixtureDatabase.ResultData.TEST_FAILED, LastErrorMessage, LastErrorCode);
                return false;
            }
            if (GWTargetMac == string.Empty)
            {
                LastErrorCode = TestStationErrorCodes.ErrorCodes.System.MissingParameter;
                LastErrorMessage = "Target MAC is not set. Property GWTargetMac must be set.";
                if (testDB != null)
                    testDB.AddStepResult(FixtureDatabase.ResultData.TEST_FAILED, LastErrorMessage, LastErrorCode);
                return false;
            }
            if (!testConfig.DoesLimitExist("LQI"))
            {
                LastErrorCode = TestStationErrorCodes.ErrorCodes.System.MissingParameter;
                LastErrorMessage = "Missing the LQI limit";
                if (testDB != null)
                    testDB.AddStepResult(FixtureDatabase.ResultData.TEST_FAILED, LastErrorMessage, LastErrorCode);
                return false;
            }
            else
            {
                lqiLimit = testConfig.GetLimitValues("LQI");
            }

            status = DoDiagTest(GWTargetIPAddress, GWTargetMac, lqiLimit, out radioResult, out radioLQIResult, out radioLQIValue, out netResult, out cfcardResult, out spidiagResult);
            if (!status)
            {
                    // LastErrorxxx set by DoDiagTest
                if (testDB != null)
                    testDB.AddStepResult(FixtureDatabase.ResultData.TEST_FAILED, LastErrorMessage, LastErrorCode);
                return false;
            }
            if (testDB != null)
            {
                testDB.AddStepResult("RadioLQI", radioLQIResult ? FixtureDatabase.ResultData.TEST_PASSED : FixtureDatabase.ResultData.TEST_FAILED, lqiLimit.operation,
                                            Convert.ToDouble(lqiLimit.value1), (double)radioLQIValue, "", "", "", 0);
                testDB.AddStepResult("RadioTest", radioResult ? FixtureDatabase.ResultData.TEST_PASSED : FixtureDatabase.ResultData.TEST_FAILED,  "", 0);
                testDB.AddStepResult("NetTest", netResult ? FixtureDatabase.ResultData.TEST_PASSED : FixtureDatabase.ResultData.TEST_FAILED, "", 0);
                testDB.AddStepResult("CFCardTest", cfcardResult ? FixtureDatabase.ResultData.TEST_PASSED : FixtureDatabase.ResultData.TEST_FAILED, "", 0);
                testDB.AddStepResult("SPITest", spidiagResult ? FixtureDatabase.ResultData.TEST_PASSED : FixtureDatabase.ResultData.TEST_FAILED, "", 0);
            }
            else
            {
                LastErrorMessage = "LQI = " + radioLQIValue + "Radio diags = " + (radioResult ? "PASS" : "FAIL") + "  NetTest = " + (netResult ? "PASS" : "FAIL") + "  CFCardTest = " + (cfcardResult ? "PASS" : "FAIL") + "  SPITest = " + (spidiagResult ? "PASS" : "FAIL");
            }
            return (radioResult & netResult & cfcardResult & spidiagResult & radioLQIResult);
        }

        public bool GWTest_ExitManufactureMode(LimitsDatabase.LimitsData.stepsItem testConfig, FixtureDatabase.ResultData testDB)
        {
            bool status = true;

            status = StartApp();

            if (testDB != null)
            {
                testDB.CreateStepRecord(testConfig.name, testConfig.step_number);
                testDB.AddStepResult(status ? FixtureDatabase.ResultData.TEST_PASSED : FixtureDatabase.ResultData.TEST_FAILED, LastErrorMessage, LastErrorCode);
            }

            return status;
        }

        /// <summary>
        /// Function to restore to manufacture mode. Just returns after the reboot command.
        /// </summary>
        /// <returns></returns>
        public bool GWFunction_ResetToManufactureMode()
        {
            string rcvline = string.Empty;

            LastErrorCode = 0;
            LastErrorMessage = string.Empty;

            if (ptrfixture == null)
            {
                LastErrorCode = -1;
                LastErrorMessage = "Fixture not defined.";
                return false;
            }

            ptrfixture.TextDutConsole.WriteLine("root", 2000);
            ptrfixture.TextDutConsole.WaitForPrompt(2000, "$ ", out rcvline);
            ptrfixture.TextDutConsole.WriteLine("mount /dev/sda3 /mnt/rwfs", 2000);
            ptrfixture.TextDutConsole.WaitForPrompt(2000, "$ ", out rcvline);
            ptrfixture.TextDutConsole.WriteLine("echo >/mnt/rwfs/etc/MFGMODE", 2000);
            ptrfixture.TextDutConsole.WaitForPrompt(2000, "$ ", out rcvline);
            ptrfixture.TextDutConsole.WriteLine("sync", 2000);
            ptrfixture.TextDutConsole.WaitForPrompt(2000, "$ ", out rcvline);
            ptrfixture.TextDutConsole.WriteLine("reboot", 2000);

            return true;
        }

        public void GWEthernetConnect(LimitsDatabase.LimitsData.stepsItem testConfig, FixtureDatabase.ResultData testDB)
        {
            bool poweron = false;
            bool connectethernet = false;

            LastErrorCode = 0;
            LastErrorMessage = string.Empty;

            if (testConfig != null)
                testDB.CreateStepRecord(testConfig.name, testConfig.step_number);

            if (ptrfixture == null)
            {
                LastErrorCode = -1;
                LastErrorMessage = "Fixture not defined.";
            }
            if (testConfig.DoesParameterExist("POWER"))
            {
                poweron = testConfig.GetParameterValue("POWER").ToUpper().Equals("ON");
                if (poweron)
                {
                    ptrfixture.SetOutput(FixtureDefintions.CH_DUT_POWER_CNTL2, (double)FixtureDefintions.PowerControl.ON);
                    ptrfixture.SetOutput(FixtureDefintions.CH_DUT_POWER_CNTL4, (double)FixtureDefintions.PowerControl.ON);
                    ptrfixture.SetOutput(FixtureDefintions.CH_DUT_TESTING_LED, (double)FixtureDefintions.PowerControl.ON);
                }
                else
                {
                    ptrfixture.SetOutput(FixtureDefintions.CH_DUT_POWER_CNTL2, (double)FixtureDefintions.PowerControl.OFF);
                    ptrfixture.SetOutput(FixtureDefintions.CH_DUT_POWER_CNTL4, (double)FixtureDefintions.PowerControl.OFF);
                    ptrfixture.SetOutput(FixtureDefintions.CH_DUT_TESTING_LED, (double)FixtureDefintions.PowerControl.OFF);
                }
            }
            if (testConfig.DoesParameterExist("ETHERNET"))
            {
                connectethernet = testConfig.GetParameterValue("ETHERNET").ToUpper().Equals("CONNECT");
                if (connectethernet)
                {
                    ptrfixture.SetOutput(FixtureDefintions.CH_DUT_POWER_CNTL1, (double)FixtureDefintions.PowerControl.ON);
                    ptrfixture.SetOutput(FixtureDefintions.CH_DUT_POWER_CNTL3, (double)FixtureDefintions.PowerControl.ON);
                    ptrfixture.SetOutput(FixtureDefintions.CH_DUT_TESTING_LED, (double)FixtureDefintions.PowerControl.ON);
                }
                else
                {
                    ptrfixture.SetOutput(FixtureDefintions.CH_DUT_POWER_CNTL1, (double)FixtureDefintions.PowerControl.OFF);
                    ptrfixture.SetOutput(FixtureDefintions.CH_DUT_POWER_CNTL3, (double)FixtureDefintions.PowerControl.OFF);
                    ptrfixture.SetOutput(FixtureDefintions.CH_DUT_TESTING_LED, (double)FixtureDefintions.PowerControl.OFF);
                }
            }
            if (testConfig.DoesParameterExist("CONSOLE"))
            {
                connectethernet = testConfig.GetParameterValue("CONSOLE").ToUpper().Equals("CONNECT");
                if (connectethernet)
                {
                    ptrfixture.SetOutput(FixtureDefintions.CH_DUT_CONSOLE_CONNECT, (double)FixtureDefintions.PowerControl.ON);
                }
                else
                {
                    ptrfixture.SetOutput(FixtureDefintions.CH_DUT_CONSOLE_CONNECT, (double)FixtureDefintions.PowerControl.OFF);
                }
            }

            if (testConfig != null)
                    testDB.AddStepResult((LastErrorCode == 0) ? FixtureDatabase.ResultData.TEST_PASSED : FixtureDatabase.ResultData.TEST_FAILED, LastErrorMessage, LastErrorCode);

        }


        //----------------------------------------
        // local Gateway tests
        //----------------------------------------

        /// <summary>
        /// Wait for the 'MFG: Starting - Waiting for START' message. 
        /// </summary>
        /// <param name="timeoutMs">max time in ms to wait for message</param>
        /// <param name="outputData">read line after the message found</param>
        /// <returns></returns>
        private bool WaitForBootUp(int timeoutMs, ref string outputData)
        {
            LastErrorCode = 0;
            LastErrorMessage = string.Empty;

            if (!ptrfixture.TextDutConsole.WaitForPrompt(timeoutMs, "MFG: Starting - Waiting for START", out outputData))
            {
                LastErrorMessage = ptrfixture.TextDutConsole.LastErrorMessage;
                LastErrorCode = ptrfixture.TextDutConsole.LastErrorCode;
                return false;
            }
            ptrfixture.TextDutConsole.ReadLine(ref outputData, 3000);          // read the prompt line
            return true;
        }


        /// <summary>
        /// Will issue the START command and wait for the MFG READY response. If there was a error
        /// LastErrorCode and LastErrorMessage will be set. If no error LastErrorCode = 0.
        /// </summary>
        /// <returns>true = MFG READY found</returns>
        private bool StartManufactureMode()
        {
            bool status = true;
            string rtnstring = string.Empty;

            ptrfixture.TextDutConsole.WriteLine("START", 1000);
            ptrfixture.TextDutConsole.ReadLine(ref rtnstring, 1000);
            if (!rtnstring.Contains("MFG READY"))           // if did not get the START response
            {
                LastErrorMessage = "Did not receive MFG READY.\n" + rtnstring.Substring(((rtnstring.Length - 900) < 0) ? 0 : (rtnstring.Length - 900));
                LastErrorCode = TestStationErrorCodes.ErrorCodes.DUT.NotInManufactureMode;
                status = false;
            }

            return status;
        }

        /// <summary>
        /// Sets the manufacturing data.  If there was a error
        /// LastErrorCode and LastErrorMessage will be set. If no error LastErrorCode = 0.
        /// </summary>
        /// <remarks>
        /// MAC=[last 3 bytes of mac] MFG_IP=[test ip for gw] MFG_DATE=[ddmmyyyy] MFG_MODEL_NO=[model] MFG_ASM=[hla PN] MFG_SER=[hla SN]
        /// </remarks>
        /// <returns>true = NW READY found</returns>
        private bool SetManufactureData(string mac, string ip, string model, string pn, string sn)
        {
            bool status = true;
            string curDatestr = DateTime.Now.ToString("ddMMyyyy");
            string rcvbuf = string.Empty;
            string rcvline = string.Empty;

            LastErrorCode = 0;
            LastErrorMessage = string.Empty;

            string cmd = "MAC=" + mac + " MFG_IP=" + ip + " MFG_DATE=" + curDatestr
                    + " MFG_MODEL_NO=" + model + " MFG_ASM=" + pn + " MFG_SER=" + sn.Substring(7);

            UpdateStatusWindow("\tWriting manufacture data\n");
            if (!ptrfixture.TextDutConsole.WriteLine(cmd, 2000))        // if error sending the command
            {
                LastErrorMessage = "Error sending comand string. " + ptrfixture.TextDutConsole.LastErrorMessage;
                LastErrorCode = ptrfixture.TextDutConsole.LastErrorCode;
                return false;
            }

            // wait for 'NW READY'
            UpdateStatusWindow("\tWaiting for 'NW READY'\n");
            if (!ptrfixture.TextDutConsole.WaitForPrompt(10000, "NW READY", out rcvbuf))
            {
                LastErrorMessage = "Error waiting for 'NW READ'. " +ptrfixture.TextDutConsole.LastErrorMessage;
                LastErrorCode = ptrfixture.TextDutConsole.LastErrorCode;
            }
            UpdateStatusWindow("\tChecking if command Passed\n");
            ptrfixture.TextDutConsole.ReadLine(ref rcvline, 2000);
            if (!rcvbuf.Contains("PASSED"))
            {
                LastErrorMessage = "'ATMEL-EEPROM-TEST: PASSED' not found after setting manufacture data.";
                LastErrorCode = TestStationErrorCodes.ErrorCodes.DUT.DutUnexpectedCmdReturn;
                return false;

            }
            UpdateStatusWindow("\tWaiting for `PHY: 0:00 - Link is Up - 100/Full' message\n");
            // wait for the 'PHY: 0:00 - Link is Up - 100/Full' response
            if (!rcvbuf.Contains("PHY: 0:00 - Link is Up - 100/Full"))
            {
                status = ptrfixture.TextDutConsole.WaitForPrompt(10000, "PHY: 0:00 - Link is Up - 100/Full", out rcvbuf);
                ptrfixture.TextDutConsole.ReadLine(ref rcvline, 1000);
                if (!status)    // if prompt is not found
                {
                    LastErrorMessage = "'PHY: 0:00 - Link is Up - 100/Full' not found.";
                    LastErrorCode = TestStationErrorCodes.ErrorCodes.DUT.DutUnexpectedCmdReturn;
                }
            }

            UpdateStatusWindow("\tSetting ethernet speed\n");
            ptrfixture.TextDutConsole.WriteLine("SHELL", 2000);
            ptrfixture.TextDutConsole.WaitForPrompt(2000, "/ #", out rcvline);
            ptrfixture.TextDutConsole.WriteLine("ethtool -s eth0 speed 10 duplex full autoneg on", 2000);
            ptrfixture.TextDutConsole.WaitForPrompt(2000, "/ #", out rcvline);
            ptrfixture.TextDutConsole.WriteLine("exit", 2000);
            ptrfixture.TextDutConsole.WaitForPrompt(5000, "PHY: 0:00 - Link is Up - 10/Full", out rcvline);

            if (status)
                UpdateStatusWindow("\tPASS\n", StatusType.passed);
            else
                UpdateStatusWindow("\tFail: " + LastErrorMessage + "\n", StatusType.failed);
            return status;
        }

        /// <summary>
        /// Get the versions. SetManufactureData must be called before this function. If there was a error
        /// LastErrorCode and LastErrorMessage will be set. If no error LastErrorCode = 0.
        /// </summary>
        /// <remarks>
        /// 
        /// 2.6.0 response:
        /// 
        /// VERSION
        /// GW Linux Kernel version: 2.6.35.3-670-g914558e-ltib-new-enl_svnr2633
        /// ---GWApp stats shared memory created---
        /// ---GW wmod stats shared memory created---
        /// GW:MAC=68:54:f5-fc:00:01
        /// GW:Opened TTY /dev/ttySP1 at 115200 baud
        /// SSL:Init..
        /// SSL:Init Done
        /// GW:Init..
        /// Couldn't open /etc/ENL
        /// GW:Param init-param [using defaults]
        /// GW:Init Done OK
        /// WMOD: Init@77003
        /// WMOD: Init-Send Ver
        /// Sent GWII_COMM_PROTO_VER to wmod
        /// Sent GWII_VER_REQ to wmod
        /// GW Version: 2.6.0.2610 Built by: root [on Apr  2 2014 @ 10:43:11]
        /// GW Version: 2.6.0.2610 Built by: root [on Apr  2 2014 @ 10:43:11]
        /// Atmel Version: 2.6.0.2610 Boot Version: 2.3
        /// </remarks>
        /// <returns>true = all versions found</returns>
        public bool GetVersion(out string gwVersion, out string atmelVersion, out string kernelVersion)
        {
            bool status = true;
            int start = 0;
            int end = 0;

            gwVersion = string.Empty;
            atmelVersion = string.Empty;
            kernelVersion = string.Empty;
            LastErrorCode = 0;
            LastErrorMessage = string.Empty;

            string outstring = string.Empty, outstring2 = string.Empty;

            ptrfixture.TextDutConsole.ClearBuffers();
            if (!ptrfixture.TextDutConsole.WriteLine("VERSION", 3000))  // sets LastErrorMessage and LastErrorCode
            {       // problem with send
                status = false;
            }
            else
                status = ptrfixture.TextDutConsole.WaitForPrompt(7000, "Atmel Version:", out outstring);

            if (status)
            {
                ptrfixture.TextDutConsole.ReadLine(ref outstring2, 1000);
                if (outstring2 == string.Empty)
                {
                    LastErrorMessage = "Error getting data for versions";
                    LastErrorCode = TestStationErrorCodes.ErrorCodes.DUT.DutUnexpectedCmdReturn;
                    return false;
                }
                outstring = outstring + outstring2;
                // have a good send and found the correct prompt

                // get Atmel version
                start = outstring.IndexOf("Atmel Version: ");
                if (start != -1)
                {
                    start += "Atmel Version: ".Length;
                    end = outstring.IndexOf("Boot");
                    if (end != -1)
                        atmelVersion = outstring.Substring(start, end - start);
                }

                // get GW version
                start = outstring.IndexOf("GW Version: ");
                if (start != -1)
                {
                    start += "GW Version: ".Length;
                    end = outstring.IndexOf("Built");
                    if (end != -1)
                        gwVersion = outstring.Substring(start, end - start);
                }

                // get Kernel version
                start = outstring.IndexOf("GW Linux Kernel version: ");
                if (start != -1)
                {
                    start += "GW Linux Kernel version: ".Length;
                    end = outstring.IndexOf("-ltib");
                    if (end != -1)
                        kernelVersion = outstring.Substring(start, end - start);
                }
            }
            return status;
        }



        /// <summary>
        /// Will do the diag function and return the results. Sends the command
        ///  DIAGS NW_PING=[pingIPAddress] RADIO_PING=[mac starting at position 9]
        /// </summary>
        /// <param name="pingIPAddress">ip to ping</param>
        /// <param name="pingMAC">full mac with colons</param>
        /// <param name="radioResult">RADIO-DIAGS: result</param>
        /// <param name="radioLQI">RADIO-STRENGH: value</param>
        /// <param name="netResult">NETWORK: result</param>
        /// <param name="cfcardResult">CF-CARD TEST: result</param>
        /// <param name="spidiagResult">SPI-FLASH-DIAGS: result</param>
        /// <returns>false = some error was found. LastErrorxxx has the error info</returns>
        private bool DoDiagTest(string pingIPAddress, string pingMAC, LimitsDatabase.LimitsData.limitsItem lqiLimit,
                                out bool radioResult, out bool radioLQIResult, out int radioLQI, out bool netResult,
                                out bool cfcardResult, out bool spidiagResult)
        {
            bool status = true;

            string rcvbuf = string.Empty;
            string[] nums;
            string cmd = string.Empty;
            string outmsg = string.Empty;

            radioLQI = 0;
            radioResult = false;        // default all to failed
            radioLQIResult = false;
            netResult = false;
            cfcardResult = false;
            spidiagResult = false;

            LastErrorMessage = string.Empty;
            LastErrorCode = 0;

            if (pingMAC.Length > 12)    // if > 12 then assume that it has ':'s
                cmd = "DIAGS NW_PING=" + pingIPAddress + " RADIO_PING=" + pingMAC.Substring(9);
            else
                cmd = "DIAGS NW_PING=" + pingIPAddress + " RADIO_PING=" + pingMAC.Substring(6, 2) + ":" + pingMAC.Substring(8, 2) + ":" + pingMAC.Substring(10, 2);
            if (!ptrfixture.TextDutConsole.WriteLine(cmd, 1000))
            {
                LastErrorMessage = ptrfixture.TextDutConsole.LastErrorMessage;
                LastErrorCode = ptrfixture.TextDutConsole.LastErrorCode;
                return false;
            }

            // The radio results are the first to appear. If it can not find the radio, it will not
            // return the 'RADIO-STENGHT: xxx - xxx' message.
            // It will always return the 'RADIO-DIAGS: <PASSED|FAILED>'
            if (!ptrfixture.TextDutConsole.WaitForPrompt(20000, "RADIO-STRENGTH: ", out rcvbuf))       // wait for the LQI value
            {
                LastErrorCode = TestStationErrorCodes.ErrorCodes.DUT.ValueOutsideOfLimit;
                LastErrorMessage = "Error: Did not get RADIO-STRENGTH message";
                return false;
            }

            if (!ptrfixture.TextDutConsole.ReadLine(ref rcvbuf, 1000))   // get the strenght numbers
            {
                LastErrorCode = TestStationErrorCodes.ErrorCodes.DUT.ValueOutsideOfLimit;
                LastErrorMessage = "Error: Did not get values for RADIO-STRENGTH";
                return false;
            }
            else                                                                // found prompt
            {
                nums = Regex.Split(rcvbuf, @"\D+");
                radioLQI = int.Parse(nums[1]);
                try
                {
                    if (CheckResultValue(radioLQI, lqiLimit, ref outmsg))
                    {
                        radioLQIResult = true;
                    }
                    else
                    {
                        radioLQIResult = false;
                        LastErrorCode = TestStationErrorCodes.ErrorCodes.DUT.ValueOutsideOfLimit;
                        LastErrorMessage = outmsg;
                    }
                }
                catch (Exception ex)
                {
                    LastErrorCode = TestStationErrorCodes.ErrorCodes.System.InvalidLimitOperation;
                    LastErrorMessage = ex.Message;
                }
            }

            if (!ptrfixture.TextDutConsole.WaitForPrompt(5000, "RADIO-DIAGS:", out rcvbuf))
            {
                LastErrorCode = TestStationErrorCodes.ErrorCodes.DUT.ValueOutsideOfLimit;
                if (LastErrorMessage != string.Empty)
                    LastErrorMessage += ", did not get RADIO-DIAGS message";
                else
                    LastErrorMessage = "Error: Did not get RADIO-DIAGS message";
                return false;
            }

            ptrfixture.TextDutConsole.ReadLine(ref rcvbuf, 1000);
            if (rcvbuf.Contains("PASS"))
            {
                radioResult = true;
            }
            else
            {
                LastErrorCode = TestStationErrorCodes.ErrorCodes.DUT.ValueOutsideOfLimit;
                if (LastErrorMessage != string.Empty)
                    LastErrorMessage += ", RADIO-DIAGS failed";
                else
                    LastErrorMessage = "Error: RADIO-DIAGS failed";
                radioResult = false;
            }

            if (!ptrfixture.TextDutConsole.WaitForPrompt(30000, "NETWORK:", out rcvbuf))
            {
                LastErrorCode = TestStationErrorCodes.ErrorCodes.DUT.ValueOutsideOfLimit;
                if (LastErrorMessage != string.Empty)
                    LastErrorMessage += ", did not get NETWORK message";
                else
                    LastErrorMessage = "Error: Did not get NETWORK message";
                return false;
            }

            ptrfixture.TextDutConsole.ReadLine(ref rcvbuf, 1000);
            if (rcvbuf.Contains("PASS"))
            {
                netResult = true;
            }
            else
            {
                LastErrorCode = TestStationErrorCodes.ErrorCodes.DUT.ValueOutsideOfLimit;
                if (LastErrorMessage != string.Empty)
                    LastErrorMessage += ", NETWORK failed";
                else
                    LastErrorMessage = "Error: NETWORK failed";
                netResult = false;
            }


            if (!ptrfixture.TextDutConsole.WaitForPrompt(10000, "CF-CARD TEST:", out rcvbuf))
            {
                LastErrorCode = TestStationErrorCodes.ErrorCodes.DUT.ValueOutsideOfLimit;
                if (LastErrorMessage != string.Empty)
                    LastErrorMessage += ", did not get CF-CARD TEST message";
                else
                    LastErrorMessage = "Error: Did not get CF-CARD TEST message";
                return false;
            }

            ptrfixture.TextDutConsole.ReadLine(ref rcvbuf, 1000);
            if (rcvbuf.Contains("PASS"))
            {
                cfcardResult = true;
            }
            else
            {
                LastErrorCode = TestStationErrorCodes.ErrorCodes.DUT.ValueOutsideOfLimit;
                if (LastErrorMessage != string.Empty)
                    LastErrorMessage += ", CF-CARD TEST failed";
                else
                    LastErrorMessage = "Error: CF-CARD TEST failed";
                cfcardResult = false;
            }

            if (!ptrfixture.TextDutConsole.WaitForPrompt(10000, "SPI-FLASH-DIAGS:", out rcvbuf))
            {
                LastErrorCode = TestStationErrorCodes.ErrorCodes.DUT.ValueOutsideOfLimit;
                if (LastErrorMessage != string.Empty)
                    LastErrorMessage += ", did not get SPI-FLASH-DIAGS message";
                else
                    LastErrorMessage = "Error: Did not get SPI-FLASH-DIAGS message";
                return false;
            }

            ptrfixture.TextDutConsole.ReadLine(ref rcvbuf, 1000);
            if (rcvbuf.Contains("PASS"))
            {
                spidiagResult = true;
            }
            else
            {
                LastErrorCode = TestStationErrorCodes.ErrorCodes.DUT.ValueOutsideOfLimit;
                if (LastErrorMessage != string.Empty)
                    LastErrorMessage += ", SPI-FLASH-DIAGS failed";
                else
                    LastErrorMessage = "Error: SPI-FLASH-DIAGS failed";
                spidiagResult = false;
            }

            if (LastErrorMessage != string.Empty)
                LastErrorCode = TestStationErrorCodes.ErrorCodes.DUT.ValueOutsideOfLimit;
            return status;
        }

        /// <summary>
        /// Will issue the LED-TEST-START command and read the LED sensor. Assumes that the
        /// LED is already on. Will issue PASS/FAIL response based freq passed.
        /// </summary>
        /// <param name="freq">freqency read</param>
        /// <param name="lowerLEDfreq">lower freq test value</param>
        /// <param name="upperLEDfreq">upper freq test value</param>
        /// <returns>true = no errors. LastErrorMessage and LastErrorCode set if errors</returns>
        private bool DoLEDTest(out int readFreq, out int freqStatus, out int readIntensity, out int intStatus, int lowerLEDfreq, int upperLEDfreq, int lowerIntesity, int upperInstesity)
        {
            bool status = true;
            string rtnLine = string.Empty;
            string outmsg = string.Empty;
            int x = 0;

            LastErrorCode = 0;
            LastErrorMessage = string.Empty;

            readFreq = 0;
            readIntensity = 0;
            freqStatus = 0;
            intStatus = 0;

            // Send the command to the dut.
            // First clear out the input buffer.
            ptrfixture.TextDutConsole.ClearBuffers();

            // send the command to start the test
            if (!ptrfixture.TextDutConsole.WriteLine("LED-TEST-START", 1000))                     // problem sending command
                return false;
            if (!ptrfixture.TextDutConsole.WaitForPrompt(2000, "Enter PASS/FAIL", out rtnLine))     // wait for prompt
                return false;
            ptrfixture.TextDutConsole.ReadLine(ref rtnLine, 1000);                         // get prompt line
            Thread.Sleep(500);

            // make sure limit is correct order
            if (lowerIntesity > upperInstesity)
            {
                x = lowerIntesity;
                lowerIntesity = upperInstesity;
                upperInstesity = x;
            }
            if (lowerLEDfreq > upperLEDfreq)
            {
                x = lowerLEDfreq;
                lowerLEDfreq = upperLEDfreq;
                upperLEDfreq = x;
            }
            // get freq and verify it is in range
            ptrfixture.IsLEDValuesInrange(lowerLEDfreq, upperLEDfreq, lowerIntesity, upperInstesity, ref freqStatus, ref intStatus,
                                            ref readFreq, ref readIntensity, ref outmsg);

            if ((freqStatus == 0) & (intStatus == 0)) // if within range
            {
                ptrfixture.TextDutConsole.WriteLine("PASS", 1000);
                ptrfixture.TextDutConsole.ReadLine(ref rtnLine, 1000);     // get response
                status = true;
            }
            else
            {
                ptrfixture.TextDutConsole.WriteLine("FAIL", 1000);
                ptrfixture.TextDutConsole.ReadLine(ref rtnLine, 1000);     // get response
                status = false;
                LastErrorCode = TestStationErrorCodes.ErrorCodes.DUT.ValueOutsideOfLimit;
                LastErrorMessage = "LED freq or intensity out of limit. Read Freq:" + readFreq + " Read Intensity:" + readIntensity + "\n";
            }


            return status;
        }

        private bool StartApp()
        {
            bool status = true;
            string rcvstring = string.Empty;

            status = ptrfixture.TextDutConsole.WriteLine("FINISH", 1000);
            if (status)
                status = ptrfixture.TextDutConsole.WaitForPrompt(20000, "MFG DONE!", out rcvstring);
            if (!status)
            {
                LastErrorCode = ptrfixture.TextDutConsole.LastErrorCode;
                LastErrorMessage = ptrfixture.TextDutConsole.LastErrorMessage;
            }
            return status;
        }



        //------------------------
        // utils
        //-----------------------------

        //-------------------------------------------------
        /// <summary>
        /// This function will check a value as defined in a List type parameters. Will throw Exception for invalid
        /// stuff.
        /// </summary>
        /// <param name="testvalue"></param>
        /// <param name="item"></param>
        /// <returns>bool - true = meets the conditions</returns>
        private static bool CheckResultValue(double testvalue, LimitsDatabase.LimitsData.limitsItem item, ref string resultmessage)
        {
            bool status = true;

            resultmessage = string.Empty;
            switch (item.type.ToUpper())
            {
                case "RANGE":
                    switch (item.operation)
                    {
                        case "GTLT":
                            if ((testvalue <= Convert.ToDouble(item.value1)) || (testvalue >= Convert.ToDouble(item.value2)))
                                status = false;
                            resultmessage = string.Format("{0}   Expected: {1} < x < {2}  Found: {3:N2}\n", item.name, item.value1, item.value2, testvalue);
                            break;
                        case "GELE":
                            if ((testvalue < Convert.ToDouble(item.value1)) || (testvalue > Convert.ToDouble(item.value2)))
                                status = false;
                            resultmessage = string.Format("{0}   Expected: {1} <= x <= {2}  Found: {3:N2}\n", item.name, item.value1, item.value2, testvalue);
                            break;
                        default:
                            throw new Exception("Invalid operation: " + item.name.ToString() + ":" + item.operation.ToString());
                    }
                    break;

                case "VALUE":
                    switch (item.operation)
                    {
                        case "GE":      // greater than or equal
                            if (!(testvalue >= Convert.ToDouble(item.value1)))
                                status = false;
                            resultmessage = string.Format("{0}   Expected: x >= {1}  Found: {2:N2}\n", item.name, item.value1, testvalue);
                            break;

                        case "EQ":      // equal
                            if (!(testvalue == Convert.ToDouble(item.value1)))
                                status = false;
                            resultmessage = string.Format("{0}   Expected: x = {1}  Found: {2:N2}\n", item.name, item.value1, testvalue);
                            break;

                        case "LE":      // less than or equal
                            if (!(testvalue <= Convert.ToDouble(item.value1)))
                                status = false;
                            resultmessage = string.Format("{0}   Expected: x <= {1}  Found: {2:N2}\n", item.name, item.value1, testvalue);
                            break;

                        case "LT":      // less than
                            if (!(testvalue < Convert.ToDouble(item.value1)))
                                status = false;
                            resultmessage = string.Format("{0}   Expected: x < {1}  Found: {2:N2}\n", item.name, item.value1, testvalue);
                            break;

                        default:
                            throw new Exception("Invalid operation: " + item.name.ToString() + ":" + item.operation.ToString());
                    }
                    break;

                default:
                    throw new Exception("Invalid type: " + item.name.ToString() + ":" + item.operation.ToString());
            }

            return status;
        }



        //-------------------------------------

    }
}
