﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GatewayProdTests
{
    public partial class GatewayTests
    {
        public bool CheckAndRunGatewayTests(LimitsDatabase.LimitsData.stepsItem testConfig, FixtureDatabase.ResultData testDB)
        {
            bool foundTest = false;

            // LastErrorxxx is set by the called routine

            switch (testConfig.function_call.ToUpper())
            {
                case "GW_BOOTUP":
                    foundTest = true;
                    GWTest_BootUp(testConfig, testDB);
                    break;

                case "GW_ETHERNETCONNECT":
                    foundTest = true;
                    GWEthernetConnect(testConfig, testDB);
                    break;

                case "CHECK3P3VSUPPLY":
                    foundTest = true;
                    GWTest_CheckVoltage(testConfig, testDB);
                    break;

                case "WAITFORSTART":
                    foundTest = true;
                    GWTest_StartMFGMode(testConfig, testDB);
                    break;

                case "PROGMANUFDATA":
                    foundTest = true;
                    GWTest_ProgramManufactureData(testConfig, testDB);
                    break;

                case "VERIFYCODE":
                    foundTest = true;
                    GWTest_VerifyFirmware(testConfig, testDB);
                    break;

                case "LEDTEST":
                    foundTest = true;
                    GWTest_LEDTest(testConfig, testDB);
                    break;

                case "NETWORKTEST":
                    foundTest = true;
                    GWTest_DiagTest(testConfig, testDB);
                    break;

                case "EXITMANUFMODE":
                    foundTest = true;
                    GWTest_ExitManufactureMode(testConfig, testDB);
                    break;


                default:
                    break;
            }
            return foundTest;
        }

    }
}
