﻿namespace SensorManufSY2
{
    partial class GetAllManData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbPCBASN = new System.Windows.Forms.TextBox();
            this.tbPCBAPN = new System.Windows.Forms.TextBox();
            this.tbHLASN = new System.Windows.Forms.TextBox();
            this.tbHLAPN = new System.Windows.Forms.TextBox();
            this.tbProductCode = new System.Windows.Forms.TextBox();
            this.tbMAC1 = new System.Windows.Forms.TextBox();
            this.tbMAC2 = new System.Windows.Forms.TextBox();
            this.tbHardwareConfig = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.bDone = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbPCBASN
            // 
            this.tbPCBASN.Location = new System.Drawing.Point(32, 46);
            this.tbPCBASN.Name = "tbPCBASN";
            this.tbPCBASN.Size = new System.Drawing.Size(100, 20);
            this.tbPCBASN.TabIndex = 0;
            // 
            // tbPCBAPN
            // 
            this.tbPCBAPN.Location = new System.Drawing.Point(32, 86);
            this.tbPCBAPN.Name = "tbPCBAPN";
            this.tbPCBAPN.Size = new System.Drawing.Size(100, 20);
            this.tbPCBAPN.TabIndex = 1;
            // 
            // tbHLASN
            // 
            this.tbHLASN.Location = new System.Drawing.Point(32, 125);
            this.tbHLASN.Name = "tbHLASN";
            this.tbHLASN.Size = new System.Drawing.Size(100, 20);
            this.tbHLASN.TabIndex = 2;
            // 
            // tbHLAPN
            // 
            this.tbHLAPN.Location = new System.Drawing.Point(32, 166);
            this.tbHLAPN.Name = "tbHLAPN";
            this.tbHLAPN.Size = new System.Drawing.Size(100, 20);
            this.tbHLAPN.TabIndex = 3;
            // 
            // tbProductCode
            // 
            this.tbProductCode.Location = new System.Drawing.Point(32, 212);
            this.tbProductCode.Name = "tbProductCode";
            this.tbProductCode.Size = new System.Drawing.Size(100, 20);
            this.tbProductCode.TabIndex = 4;
            // 
            // tbMAC1
            // 
            this.tbMAC1.Location = new System.Drawing.Point(178, 46);
            this.tbMAC1.Name = "tbMAC1";
            this.tbMAC1.Size = new System.Drawing.Size(100, 20);
            this.tbMAC1.TabIndex = 5;
            // 
            // tbMAC2
            // 
            this.tbMAC2.Location = new System.Drawing.Point(178, 86);
            this.tbMAC2.Name = "tbMAC2";
            this.tbMAC2.Size = new System.Drawing.Size(100, 20);
            this.tbMAC2.TabIndex = 6;
            // 
            // tbHardwareConfig
            // 
            this.tbHardwareConfig.Location = new System.Drawing.Point(178, 134);
            this.tbHardwareConfig.Name = "tbHardwareConfig";
            this.tbHardwareConfig.Size = new System.Drawing.Size(100, 20);
            this.tbHardwareConfig.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "PCBA SN";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "PCBA PN";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(32, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "HLA SN";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(32, 150);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "HLA PN";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 196);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Product Code";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(183, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "MAC1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(183, 69);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "MAC2";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(183, 118);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(86, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Hardware Config";
            // 
            // bDone
            // 
            this.bDone.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bDone.Location = new System.Drawing.Point(178, 212);
            this.bDone.Name = "bDone";
            this.bDone.Size = new System.Drawing.Size(75, 23);
            this.bDone.TabIndex = 16;
            this.bDone.Text = "Save";
            this.bDone.UseVisualStyleBackColor = true;
            this.bDone.Click += new System.EventHandler(this.bDone_Click);
            // 
            // GetAllManData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(305, 267);
            this.Controls.Add(this.bDone);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbHardwareConfig);
            this.Controls.Add(this.tbMAC2);
            this.Controls.Add(this.tbMAC1);
            this.Controls.Add(this.tbProductCode);
            this.Controls.Add(this.tbHLAPN);
            this.Controls.Add(this.tbHLASN);
            this.Controls.Add(this.tbPCBAPN);
            this.Controls.Add(this.tbPCBASN);
            this.Name = "GetAllManData";
            this.Text = "GetAllManData";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbPCBASN;
        private System.Windows.Forms.TextBox tbPCBAPN;
        private System.Windows.Forms.TextBox tbHLASN;
        private System.Windows.Forms.TextBox tbHLAPN;
        private System.Windows.Forms.TextBox tbProductCode;
        private System.Windows.Forms.TextBox tbMAC1;
        private System.Windows.Forms.TextBox tbMAC2;
        private System.Windows.Forms.TextBox tbHardwareConfig;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button bDone;
    }
}