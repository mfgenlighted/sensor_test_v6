﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace SensorManufSY2
{
    public partial class GetAllManData : Form
    {
        public string PCBASN = string.Empty;
        public string PCBAPN = string.Empty;
        public string HLASN = string.Empty;
        public string HLAPN = string.Empty;
        public string ProductCode = string.Empty;
        public string MAC1 = string.Empty;
        public string MAC2 = string.Empty;
        public string HardwareCode = string.Empty;

        public GetAllManData()
        {
            InitializeComponent();
        }

        private void bDone_Click(object sender, EventArgs e)
        {
            Regex fmt = new Regex(@"^....\d\d\d\d\d\d\d\d\d\d\d$");          // new SN format
            tbPCBASN.Text = tbPCBASN.Text.ToUpper();
            if (!fmt.Match(tbPCBASN.Text).Success)       // if failed
            {
                MessageBox.Show("Invalid Serial Number format");
                tbPCBASN.Focus();
                this.DialogResult = DialogResult.None;
                return;
            }
            PCBASN = tbPCBASN.Text;
            PCBAPN = tbPCBAPN.Text;

            tbHLASN.Text = tbHLASN.Text.ToUpper();
            if (!fmt.Match(tbHLASN.Text).Success)       // if failed
            {
                MessageBox.Show("Invalid Serial Number format");
                tbHLASN.Focus();
                this.DialogResult = DialogResult.None;
                return;
            }
            HLASN = tbHLASN.Text;
            HLAPN = tbHLAPN.Text;

            if (tbMAC1.Text.Length != 12)
            {
                MessageBox.Show("Invalid format. The MAC1 must be 12 characters long.");
                tbMAC1.Clear();
                tbMAC1.Focus();
                this.DialogResult = DialogResult.None;
                return;
            }
            if (tbMAC2.Text.Length != 12)
            {
                MessageBox.Show("Invalid format. The MAC2 must be 12 characters long.");
                tbMAC2.Clear();
                tbMAC2.Focus();
                this.DialogResult = DialogResult.None;
                return;
            }
            MAC1 = tbMAC1.Text;
            MAC2 = tbMAC2.Text;

            ProductCode = tbProductCode.Text;
            HardwareCode = tbHardwareConfig.Text;
            DialogResult = System.Windows.Forms.DialogResult.OK;

        }
    }
}
