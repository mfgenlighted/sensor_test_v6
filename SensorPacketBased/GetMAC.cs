﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SensorManufSY2
{
    public partial class GetMAC : Form
    {

        public string MAC1 { get { return tbMAC1.Text; } }
        public string MAC2 { get { return tbMAC2.Text; } }

        public GetMAC()
        {
            InitializeComponent();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if (tbMAC1.Text.Length != 12)
            {
                MessageBox.Show("Invalid format. The MAC1 must be 12 characters long.");
                tbMAC1.Clear();
                tbMAC1.Focus();
                return;
            }
            if (tbMAC2.Text.Length != 12)
            {
                MessageBox.Show("Invalid format. The MAC2 must be 12 characters long.");
                tbMAC2.Clear();
                tbMAC2.Focus();
                return;
            }

            DialogResult = System.Windows.Forms.DialogResult.OK;
            return;

        }
    }
}
