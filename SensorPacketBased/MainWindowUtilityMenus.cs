﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;

using Seagull.BarTender.Print;

using FixtureDatabase;

namespace SensorManufSY2
{
    public partial class MainWindow
    {
        //private System.ComponentModel.IContainer components;
        private FixtureFunctions fixture = null;

        private void ReprintLabel(object sender, EventArgs e)
        {
            char[] delimiterChars = { '\n', '\r' };
//            uutdata readuutData = new uutdata();
//            LimitFileFunctions.LimitFileFunctions limitfile = new LimitFileFunctions.LimitFileFunctions();
//            ProductDatabase.Station_ProductCodeData productCodeData = new ProductDatabase.Station_ProductCodeData();
            string outmsg = string.Empty;
//            string appPrompt;
//            int limitIndex;
            string pcbaLabelFileName = string.Empty;
            string productLabelFileName = string.Empty;
//            System.Windows.Forms.DialogResult answer;
            string fullLimitFileName = string.Empty;


            ReprintWindow rpl = new ReprintWindow();
            //    if ((answer = rpl.ShowDialog()) == System.Windows.Forms.DialogResult.Yes) // yes = reprint last barcode
            //    {
            if ((lastPCBABarcodeFileName == string.Empty) & (lastProductBarcodeFileName == string.Empty))
            {
                MessageBox.Show("There is no barcode to print.");
                return;
            }
            try
            {
                if (lastPCBABarcodeFileName != string.Empty)
                {
                    var btengine = new Engine(true);

                    if (stationConfig.ConfigLabelFileDirectory == string.Empty)     // if no directory is defined
                    {
                        String strAppDir = Path.GetDirectoryName(Application.ExecutablePath);
                        fullLimitFileName = Path.Combine(strAppDir, lastPCBABarcodeFileName);
                    }
                    else                                                            // directory is defined
                    {
                        fullLimitFileName = Path.Combine(stationConfig.ConfigLabelFileDirectory, lastPCBABarcodeFileName);
                    }


                    //string appPath = Path.GetDirectoryName(Application.ExecutablePath);
                    //appPath = "\"" + appPath + "\\" + lastPCBABarcodeFileName + "\"";
                    //var btformat = btengine.Documents.Open(appPath);
                    var btformat = btengine.Documents.Open(fullLimitFileName);
                    btformat.PrintSetup.PrinterName = lastPCBALabelPrinter;
                    btformat.Save();
                    Result result = btformat.Print();
                    btformat.Close(SaveOptions.DoNotSaveChanges);
                    btengine.Dispose();
                    MessageBox.Show("Label printed");
                }
                if (lastProductBarcodeFileName != string.Empty)
                {
                    var btengine = new Engine(true);
                    if (stationConfig.ConfigLabelFileDirectory == string.Empty)     // if no directory is defined
                    {
                        String strAppDir = Path.GetDirectoryName(Application.ExecutablePath);
                        fullLimitFileName = Path.Combine(strAppDir, lastProductBarcodeFileName);
                    }
                    else                                                            // directory is defined
                    {
                        fullLimitFileName = Path.Combine(stationConfig.ConfigLabelFileDirectory, lastProductBarcodeFileName);
                    }
                    //string appPath = Path.GetDirectoryName(Application.ExecutablePath);
                    //appPath = "\"" + appPath + "\\" + lastProductBarcodeFileName + "\"";
                    //var btformat = btengine.Documents.Open(appPath);
                    var btformat = btengine.Documents.Open(fullLimitFileName);
                    btformat.PrintSetup.PrinterName = lastProductLabelPrinter;
                    btformat.Save();
                    Result result = btformat.Print();
                    btformat.Close(SaveOptions.DoNotSaveChanges);
                    btengine.Dispose();
                    MessageBox.Show("Label printed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("There was a problem printing the label.\n" + ex.Message);
            }

        }

        /// <summary>
        /// This will clear the Manufacturing data from the unit. Must be in DVTMAN.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClearManufacutureData(object sender, EventArgs e)
        {
            string outmsg = string.Empty;
            int msgVersion = 0;

            //using (SensorConsole rrc = new SensorConsole())
            {
                addTextToStatusWindow("Starting DUT Manufacture Data clear.....", true);

                fixture = new FixtureFunctions(stationConfig);
                if (!fixture.InitFixture())
                {
                    MessageBox.Show("Error initializing fixture. " + fixture.LastFunctionErrorMessage);
                    return;
                }
                fixture.ControlDUTPower(FixtureDefintions.PowerControl.ON, 0);     // turn dut power on
                Thread.Sleep(1000);
                // Verify it is in DVTMAN.
                fixture.PacketDutConsole.GetMsgVersion(ref msgVersion);

                if (!fixture.PacketDutConsole.GetMsgVersion(ref msgVersion))
                {
                    addTextToStatusWindow("Dut Com error: Getting msg version.\n", false);
                    fixture.ControlDUTPower(FixtureDefintions.PowerControl.OFF, 0);     // turn dut power off
                    fixture.Dispose();
                    return;
                }

                if (!fixture.PacketDutConsole.ClearManData())
                {
                    addTextToStatusWindow(fixture.PacketDutConsole.LastErrorMessage);
                }

                Thread.Sleep(1000);                 // let everything catch up
                fixture.ControlDUTPower(FixtureDefintions.PowerControl.OFF, 0);     // turn dut power off
                fixture.Dispose();

                addTextToStatusWindow("DUT Manufacture Data cleared\n", true);
            }
        }
    }
}
