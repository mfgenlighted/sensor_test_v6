﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SensorManufSY2
{
    public partial class SelectStation : Form
    {
        public SelectStation(List<string> stations)
        {
            InitializeComponent();
            foreach (var item in stations)
            {
                this.comboBoxStations.Items.Add(item);
            }
            this.comboBoxStations.SelectedIndex = 0;
        }

        public string Station { get { return this.comboBoxStations.Text; } }
    }
}
