﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

using FixtureDatabase;
using LimitsDatabase;
using TestStationErrorCodes;
using SensorManufSY2;

//namespace SensorManufSY2
namespace SensorProdTests
{
    class SensorAPPTests : IDisposable
    {
        //------------------------
        // events
        //  Use UpdateStatusWindow(msg) to display a message on the
        //  MainWindow status window.
        //------------------------
        public event EventHandler<StatusUpdatedEventArgs>
            StatusUpdated;

        protected virtual void
            OnStatusUpdated(StatusUpdatedEventArgs e)
        {
            if (StatusUpdated != null)
                StatusUpdated(this, e);
        }

        // call these functions to update the main status window
        private void UpdateStatusWindow(string msg)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            OnStatusUpdated(nuea);
        }
        private void UpdateStatusWindow(string msg, StatusType status)
        {
            string newStatus = msg;
            StatusUpdatedEventArgs nuea = new StatusUpdatedEventArgs();
            nuea.statusText = msg;
            nuea.statusType = status;
            OnStatusUpdated(nuea);
        }


        //--------------------
        // Properties
        //--------------------
        public StationConfigure stationConfig = null;
        public PacketBasedConsole sensorConsole = null;

        //CUConsole cuConsole;
        //TestRadioConsole testRadioConsole;
        //FixtureFunctions fixture;

        public SensorAPPTests()
        {
        }


        public int VersionTest(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, ref string resultmsg)
        {
            int status = 0;

            return status;
        }

        /// <summary>
        /// Test not implemented
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="testDB"></param>
        /// <param name="resultmsg"></param>
        /// <returns></returns>
        public int VerifyLabel(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, MainWindow.uutdata uutData, out string resultmsg)
        {
            int status = (int)ErrorCodes.System.TestNotImplemented;
            resultmsg = "Test Not Implemented";
            return status;
        }


        /// <summary>
        /// Will verify that the output from the d profile command does not contain any
        /// 255 or 65535
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="testDB"></param>
        /// <param name="resultmsg"></param>
        /// <returns></returns>
        public int CheckProfile(LimitsData.stepsItem parameters, FixtureDatabase.ResultData testDB, string prompt, ref string resultmsg)
        {
            //int status = 0;
            //string outmsg = string.Empty;

            //testDB.CreateStepRecord(parameters.Name, parameters.TestNumber);

            //sensorConsole.ClearBuffers();
            //if (!sensorConsole.WriteLine("d profile", 1000))
            //{
            //    resultmsg = "Error sending 'd profile' command";
            //    status = (int)ErrorCodes.Program.DUTComError + (parameters.TestNumber * 100);
            //}
            //if (!sensorConsole.WaitForPrompt(5000, prompt, out outmsg))
            //{
            //    resultmsg = "Error waiting for prompt after 'd profile' command";
            //    status = (int)ErrorCodes.Program.DUTComError + (parameters.TestNumber * 100);
            //}

            //// should now have the response in outmsg
            //// make sure there are no '255' or '65535' in the text
            //if ((outmsg.Contains("255")) | outmsg.Contains("65535"))
            //{
            //    resultmsg = "Profile has a 255 or 65535 in the text.\n" + outmsg;
            //    status = (int)ErrorCodes.Program.DUTComError + (parameters.TestNumber * 100);

            //}

            //testDB.AddStepResult((status == 0) ? "PASS" : "FAIL", resultmsg, status); 
            //return status;
            return (int)ErrorCodes.System.TestNotImplemented;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~SensorAPPTests() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
