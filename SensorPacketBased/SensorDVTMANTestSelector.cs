﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SensorManufSY2;

namespace SensorProdTests
{
    public partial class SensorDVTMANTests
    {
        public bool CheckAndRunTests(LimitsDatabase.LimitsData.stepsItem testConfig, FixtureDatabase.ResultData testDB, MainWindow.uutdata uutData)
        {
            bool foundTest = false;
            string resultmsg = string.Empty;
            //int LastErrorCode = 0;
            //string LastErrorMessage = string.Empty;
            ;

            switch (testConfig.function_call.ToUpper())
            {
                //------ turn on dut, verify voltage, flash code
                case "TURNONDUT":
                    foundTest = true;
                    LastErrorCode = TurnOnDut(testConfig, testDB, uutData, ref LastErrorMessage);
                    break;

                //------- Check PIR, AMB, and Temp sensors
                case "CHECKSENSORS":
                    foundTest = true;
                    LastErrorCode = AllSensorTest(testConfig, testDB, uutData, ref LastErrorMessage);
                    break;

                //------ Radios
                case "BLERADIOTEST":
                    foundTest = true;
                    LastErrorCode = TestBLE(testConfig, testDB, uutData, ref LastErrorMessage);
                    break;

                case "RADIOTEST":
                    foundTest = true;
                    LastErrorCode = TestNetworks(testConfig, testDB, uutData, ref LastErrorMessage);
                    break;
                default:
                    break;
            }
            return foundTest;
        }

    }
}
